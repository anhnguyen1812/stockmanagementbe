package com.creasia.stock.util;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {
//	 @SuppressWarnings("unused")
	public void sendmail(String subject, String content, List<String> recipient, String receiptTO) throws AddressException, MessagingException, IOException {
		   Properties props = new Properties();
		   props.put("mail.smtp.auth", "true");
		   props.put("mail.smtp.starttls.enable", "true");
		   props.put("mail.smtp.host", "pro08.emailserver.vn");
		   props.put("mail.smtp.port", "587");
		   
		   Session session = Session.getInstance(props, new javax.mail.Authenticator() {
		      protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
		    	  //send
		         return new javax.mail.PasswordAuthentication("tech@creasia.com.vn", "creasia2014@");
		      }
		   });
		   System.out.println("session" + session);
		   Message msg = new MimeMessage(session);
		   msg.setFrom(new InternetAddress("tech@creasia.com.vn", false));
		   //"anhnt@creasia.com.vn"
		   //"Tutorials point email"
		   //"Tutorials point email"
		   msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(receiptTO));
		   for (String rc : recipient) {
			   msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(rc));
		   }		   
		   msg.setSubject(subject);
		   msg.setContent(content, "text/html");
		   msg.setSentDate(new Date());
		   String mail_body = "<html><head><link href=\"https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap\" rel=\"stylesheet\" /></head><body>";
		   String mail_body_end = "</body></html>";
//		   msg.setContent(mail_body + content + mail_body_end, "text/html");
//		   msg.setHeader("Content-Type", encodingOptions);

		   MimeBodyPart messageBodyPart = new MimeBodyPart();
		   messageBodyPart.setContent(content, "text/html");
		   Multipart multipart = new MimeMultipart();
		   multipart.addBodyPart(messageBodyPart);
		   MimeBodyPart attachPart = new MimeBodyPart();

//		   attachPart.attachFile("/var/tmp/image19.png");
//		   multipart.addBodyPart(attachPart);
		   msg.setContent(multipart);
		   javax.mail.Transport.send(msg);   
		}
}
