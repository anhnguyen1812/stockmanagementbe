package com.creasia.stock.importer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.creasia.stock.entity.Report;

public class ExcelImporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet, sheet1;
	
	   
   private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else if (value instanceof String){
            cell.setCellValue((String) value); 
        }else {
        	 cell.setCellValue((Long) value); 
        }
        cell.setCellStyle(style);
    }
	    
   // Report by project
   public void exportProject(List<Report> resultList,List<Report> resultItemLegdeEntry, HttpServletResponse response) throws IOException{
        writeHeaderLineProject();
        writeDataLinesProject(resultList,resultItemLegdeEntry);
		ServletOutputStream outputStream = response.getOutputStream();
		
		workbook.write(outputStream);
		workbook.close();
		outputStream.flush();
		outputStream.close();
		return;
    }
   
   private void writeHeaderLineProject() {
		workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Sumary");
         
        Row row = sheet.createRow(0);
         
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
         
        createCell(row, 0, "Starting Date", style);      
        createCell(row, 1, "Ending Date", style);       
        createCell(row, 2, "Project Code ", style);    
        createCell(row, 3, "Project Name", style);
        createCell(row, 4, "Item name", style);
        createCell(row, 5, "Unit of Measure", style);
        createCell(row, 6, "Input Quantity", style);
        createCell(row, 7, "Output Quantity", style);
        createCell(row, 8, "Balance Quantity", style);  
        

        sheet1 = workbook.createSheet("Detail");
   
        row = sheet1.createRow(0);
        
        style = workbook.createCellStyle();
        font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
         
        createCell(row, 0, "Posted Date", style);      
        createCell(row, 1, "Document No", style);       
        createCell(row, 2, "Document Type", style);    
        createCell(row, 3, "Document Group", style);
        createCell(row, 4, "Company", style);
        createCell(row, 5, "Project", style);
        createCell(row, 6, "Stock Name", style);
        createCell(row, 7, "Item", style);
        createCell(row, 8, "Unit of Measure", style);  
        createCell(row, 9, "Quanity", style);  
//        
         
	  }
	  
   private void writeDataLinesProject(List<Report> reportList, List<Report> resultItemLegdeEntry) {
        int rowCount = 1;
 
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (int i = 0; i< reportList.size(); i++) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
           
            createCell(row, columnCount++, reportList.get(i).getProject().getStartDate().substring(0,10), style);
            createCell(row, columnCount++, reportList.get(i).getProject().getFinishDate().substring(0,10), style);
            createCell(row, columnCount++, reportList.get(i).getProject().getCode(), style);
            createCell(row, columnCount++, reportList.get(i).getProject().getName(), style);
            createCell(row, columnCount++,  reportList.get(i).getItem().getName(), style);
            createCell(row, columnCount++,  reportList.get(i).getUoM().getName(), style);
            createCell(row, columnCount++, reportList.get(i).getQuantityIn() , style);
            createCell(row, columnCount++, reportList.get(i).getQuantityOut(), style);
            createCell(row, columnCount++,  reportList.get(i).getQuantity(), style);
        }
        rowCount = 1;
        for (int i = 0; i< resultItemLegdeEntry.size(); i++) {
        	if(resultItemLegdeEntry.get(i).getTransaction().getPostedDate() != null && resultItemLegdeEntry.get(i).getTransactionLine().getPostedQuantity() != 0) {
            Row row = sheet1.createRow(rowCount++);
            int columnCount = 0;
            System.out.println("result item: " + resultItemLegdeEntry.get(i).getTransactionLine().getPostedQuantity());
            
     
            
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getPostedDate() , style);
         
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getNoteCode() , style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getName() , style);
            if(resultItemLegdeEntry.get(i).getTransaction().getDocumentGroup()== 1) {
            	createCell(row, columnCount++,"Positive" , style);
            }else {
            	createCell(row, columnCount++,"Negative" , style);
            }
            
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getProject().getCompany().getName() , style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getProject().getName() , style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getStock().getName() , style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getItem().getName(), style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getItem().getUoM().getName(), style);
            createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getPostedQuantity() , style);
       
            }
        }
    }
     
   // Report by stock
 
   public void exportStock(List<Report> resultList,List<Report>  reportItemLegdeEntry,HttpServletResponse response) throws IOException{
        writeHeaderLineStock();
        writeDataLinesStock(resultList,reportItemLegdeEntry);
		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();
		outputStream.flush();
		outputStream.close();
		return;
    }
   private void writeHeaderLineStock() {
		workbook = new XSSFWorkbook();
       sheet = workbook.createSheet("Sumary");
        
       Row row = sheet.createRow(0);
        
       CellStyle style = workbook.createCellStyle();
       XSSFFont font = workbook.createFont();
       font.setBold(true);
       font.setFontHeight(16);
       style.setFont(font);
        
       createCell(row, 0, "Stock Code", style);      
       createCell(row, 1, "Stock Name", style);  
       createCell(row, 2, "Address", style); 
       createCell(row, 3, "Item Name", style);    
       createCell(row, 4, "Unit of Measure", style);
       createCell(row, 5, "Balance Quantity", style);
       
       
       sheet1 = workbook.createSheet("Detail");
       
       row = sheet1.createRow(0);
       
       style = workbook.createCellStyle();
       font = workbook.createFont();
       font.setBold(true);
       font.setFontHeight(16);
       style.setFont(font);
        
       createCell(row, 0, "Posted Date", style);      
       createCell(row, 1, "Document No", style);       
       createCell(row, 2, "Document Type", style);    
       createCell(row, 3, "Document Group", style);
       createCell(row, 4, "Company", style);
       createCell(row, 5, "Project", style);
       createCell(row, 6, "Stock Name", style);
       createCell(row, 7, "Item", style);
       createCell(row, 8, "Unit of Measure", style);  
       createCell(row, 9, "Quanity", style); 
      
	  }
	  
  private void writeDataLinesStock(List<Report> reportList,List<Report> resultItemLegdeEntry) {
	  int rowCount = 1;
	  
      CellStyle style = workbook.createCellStyle();
      XSSFFont font = workbook.createFont();
      font.setFontHeight(14);
      style.setFont(font);

      for (int i = 0; i< reportList.size(); i++) {
          Row row = sheet.createRow(rowCount++);
          int columnCount = 0;
          createCell(row, columnCount++, reportList.get(i).getStock().getCode(), style);
          createCell(row, columnCount++, reportList.get(i).getStock().getName(), style);
          createCell(row, columnCount++, reportList.get(i).getStock().getAddress()+"-" + reportList.get(i).getStock().getDistrict().getName(), style);
          createCell(row, columnCount++, reportList.get(i).getItem().getName(), style);
          createCell(row, columnCount++, reportList.get(i).getUoM().getName(), style);
          createCell(row, columnCount++, reportList.get(i).getQuantity(), style);
      }
      
      rowCount = 1;
      for (int i = 0; i< resultItemLegdeEntry.size(); i++) {
      	
          Row row = sheet1.createRow(rowCount++);
          int columnCount = 0;
          System.out.println("result item: " + resultItemLegdeEntry.get(i).getTransactionLine().getPostedQuantity());
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getPostedDate() , style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getNoteCode() , style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getName() , style);
          if(resultItemLegdeEntry.get(i).getTransaction().getDocumentGroup()== 1) {
          	createCell(row, columnCount++,"Positive" , style);
          }else {
          	createCell(row, columnCount++,"Negative" , style);
          }
          
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getProject().getCompany().getName() , style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getProject().getName() , style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransaction().getStock().getName() , style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getItem().getName(), style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getItem().getUoM().getName(), style);
          createCell(row, columnCount++,resultItemLegdeEntry.get(i).getTransactionLine().getPostedQuantity() , style);
      }
  }
   

}
