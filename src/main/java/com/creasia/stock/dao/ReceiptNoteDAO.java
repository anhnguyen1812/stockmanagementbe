package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReceiptNote;

public interface ReceiptNoteDAO {

	public List<ReceiptNote> findAll();
	
	public long countReceiptNotes();

	public ReceiptNote findById(int theId);

	public void save(ReceiptNote theReceiptNote);

	public void update(ReceiptNote theReceiptNote);

	public void delete(int theId);

	public int findNewCode();
	
	public String findLastCode() ;
}
