package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Item;

public interface ItemDAO {

	public List<Item> findAll();
	
	public long countItem();
	
	public Item findById(int theId);
	
	public int findNewCode();
	
	public void save(Item theItem);
	
	public void delete(int theId);
	
	public List<Item>  findByNameAndUoM(String nameItem, int uomId);
}
