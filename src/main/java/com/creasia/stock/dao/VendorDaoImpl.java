package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Vendor;

@Repository
public class VendorDaoImpl implements VendorDAO {

	//define field for entitymanager
	private EntityManager entityManager;
		
		//setup constructor injection
	@Autowired
	public VendorDaoImpl(EntityManager theEntityManager) {
			entityManager = theEntityManager;
	}
			
	@Override
	public List<Vendor> findAll() {

		Session currentSession = entityManager.unwrap(Session.class);
		Query<Vendor> theQuery =
				currentSession.createQuery("from Vendor  Order by id DESC", Vendor.class);
		
		List<Vendor> vendors = theQuery.getResultList();
		
		return vendors;
	}

	@Override
	public Vendor findById(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Vendor theVendor = currentSession.get(Vendor.class, theId);
		return theVendor;
	}

	@Override
	public void save(Vendor theVendor) {
		try {
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.save(theVendor);
		currentSession.flush();
		}catch (Exception e) {
			
		}
	}
	
	@Override
	public void update(Vendor theVendor) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theVendor);
		currentSession.flush();
	}
	
	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Vendor theVendor = findById(theId);
		theVendor.setBlocked(true);
		
		currentSession.saveOrUpdate(theVendor);
		
	}
	
	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(v.id) FROM Vendor v", Integer.class);
			
			 maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
