package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Customer;
import com.creasia.stock.entity.Vendor;

public interface VendorDAO {

	public List<Vendor> findAll();
	
	public Vendor findById(int theId);
	
	public void save(Vendor theVendor);
	
	public void update(Vendor theVendor);
	
	public void delete(int theId);
	
	public int findNewCode();
}
