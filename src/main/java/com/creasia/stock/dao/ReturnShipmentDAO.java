package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReturnShipment;

public interface ReturnShipmentDAO {

	public List<ReturnShipment> findAll();

	public ReturnShipment findById(int theId);
	
	public List<ReturnShipment> findByRequestShipmentId(int theRequestId);

	public void save(ReturnShipment theReturnShipment);

	public void update(ReturnShipment theReturnShipment);

	public void delete(int theId);

	public int findNewCode();
	
	public String findLastCode();
}
