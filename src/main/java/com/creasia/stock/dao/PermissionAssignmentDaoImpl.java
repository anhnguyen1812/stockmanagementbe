package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ApprovedShipment;
import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;

@Repository
public class PermissionAssignmentDaoImpl implements PermissionAssignmentDAO {

	private EntityManager entityManager;
	
	@Autowired
	public PermissionAssignmentDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<PermissionAssignment> findAll(){
		
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<PermissionAssignment> theQuery = currentSession.createQuery("select pa from PermissionAssignment pa JOIN pa.users WHERE pa.isBlock = false Order by pa.id DESC", PermissionAssignment.class);
			
			List<PermissionAssignment> userAssignmentStock = theQuery.getResultList();
			System.out.println("userAssignmentStock:: " + userAssignmentStock);
			return userAssignmentStock;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error:: " + e.toString());
			return null;
		}
		
	}

	@Override
	public PermissionAssignment findById(int theId) {
		
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			PermissionAssignment theUserAssignmentStock = currentSession.get(PermissionAssignment.class, theId);
			return theUserAssignmentStock;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<PermissionAssignment> getPermissionByUser(long userId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("userId= " + userId);
			Query<PermissionAssignment> theQuery = currentSession.createQuery(
					"from PermissionAssignment pa Where (pa.users.id = :userId) and (pa.endingDate = NULL OR current_date BETWEEN pa.startingDate AND pa.endingDate )  Order by pa.id DESC",
					PermissionAssignment.class);
			theQuery.setParameter("userId", userId);
			System.out.println("userId= " + userId);
			List<PermissionAssignment> permission = theQuery.getResultList();
			System.out.println("stocks= " + permission);
			return permission;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error = " + e.toString());
			return null;
		}
	}
	@Override
	public List<User> getUserByStockId(int stockId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("userId= " + stockId);
			Query<User> theQuery = currentSession.createQuery(
					"select pa.users from PermissionAssignment pa Where  ((pa.stock.id = :stockId OR (pa.roles.id = 3 and pa.stock = null)) and (pa.endingDate = NULL OR current_date BETWEEN pa.startingDate AND pa.endingDate ))  Order by pa.id DESC",
					User.class);
			theQuery.setParameter("stockId", stockId);
			System.out.println("stockId= " + stockId);
			List<User> users = theQuery.getResultList();
			System.out.println("stocks= " + users);
			return users;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error = " + e.toString());
			return null;
		}
	}
	
	@Override
	public List<Employee> getUserAdmin() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Employee> theQuery = currentSession.createQuery(
					"select pa.users.employee from PermissionAssignment pa Where (pa.roles.id = 1) and (pa.endingDate = NULL OR current_date BETWEEN pa.startingDate AND pa.endingDate )  Order by pa.id DESC",
					Employee.class);
			List<Employee> users = theQuery.getResultList();
			return users;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error1 = " + e.toString());
			return null;
		}
	}
	
	@Override
	public Employee getUserManager(int userId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Employee> theQuery = currentSession.createQuery(
					"select pa.users.employee.manager from PermissionAssignment pa Where pa.users.employee.id = :userId and (pa.endingDate = NULL OR current_date BETWEEN pa.startingDate AND pa.endingDate )  Order by pa.id DESC",
					Employee.class);
			theQuery.setParameter("userId", userId);
			Employee users = theQuery.getSingleResult();
			return users;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error = " + e.toString());
			return null;
		}
	}

	@Override
	public void save(PermissionAssignment thePermissionAssignment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + thePermissionAssignment);
			
			currentSession.clear();
			currentSession.save(thePermissionAssignment);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void update(PermissionAssignment per) {
		// TODO Auto-generated method stub
		try {
			System.out.println("theper DAO1: " + per);
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(per);
			System.out.println("theper DAO2: " + per);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Exception DAO: " + e.toString());
		}
	}
}
