package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.RequestShipmentLine;

public interface ProjectDAO {

	public List<Project> findAll();
	
	public Project findById(int theId);
	
	public long findByCode(String theCode);
	
	public long countProject();
	
	public void save(Project theProject);
	
	public void update(Project theProject);
	
	public void delete(int theId);
	
	public int findNewCode();
	
	public List<Project>  findProjectByPmId(int pmId);
}
