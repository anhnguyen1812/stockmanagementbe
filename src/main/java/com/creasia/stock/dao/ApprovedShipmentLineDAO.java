package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ApprovedShipmentLine;


public interface ApprovedShipmentLineDAO {
	
	public List<ApprovedShipmentLine> findAll();

	public ApprovedShipmentLine findById(int theId);

	public void save(ApprovedShipmentLine theApprovedShipmentLine);

	public void update(ApprovedShipmentLine theApprovedShipmentLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<ApprovedShipmentLine> findByApprovedShipmentId(int theApprovedShipmentId);
}
