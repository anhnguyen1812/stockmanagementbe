package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Customer;

public interface CustomerDAO {

	public List<Customer> findAll();
	
	public Customer findById(int theId);
	
	public void save(Customer theCustomer);
	
	public void update(Customer theCustomer);
	
	public void delete(int theId);
	
	public int findNewCode();
}
