package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Role;

@Repository
public class RoleDaoImpl implements RoleDAO{
	
	private EntityManager entityManager;

	// setup constructor injection
	@Autowired
	public RoleDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Role> findAll(){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Role> theQuery =
				currentSession.createQuery("from Role", Role.class);
		
		List<Role> Roles = theQuery.getResultList();
		
		return Roles;
	}
}
