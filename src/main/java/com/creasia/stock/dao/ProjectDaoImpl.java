package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.Project;

@Repository
public class ProjectDaoImpl implements ProjectDAO {

	// define field for entitymanager
	private EntityManager entityManager;

	// setup constructor injection
	@Autowired
	public ProjectDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Project> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		// create a query
		Query<Project> theQuery = currentSession.createQuery("from Project  Order by id DESC", Project.class);

		List<Project> Projects = theQuery.getResultList();
		return Projects;
	}
	
	@Override
	public long findByCode(String theCode) {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM Project as i where i.code = :theCode", Long.class);
			theQuery2.setParameter("theShipmentNoteId", theCode);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public long countProject() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM Project as i where i.isBlocked = false", Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public List<Project>  findProjectByPmId(int pmId){
		Session currentSession = entityManager.unwrap(Session.class);
		// create a query
		Query<Project> theQuery = currentSession.createQuery("from Project p where p.employee.id = :pmId ", Project.class);
		theQuery.setParameter("pmId", pmId);
		List<Project> Projects = theQuery.getResultList();
		return Projects;
	}
	@Override
	public Project findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Project theQuery = currentSession.get(Project.class, theId);
		return theQuery;
	}

	@Override
	public void save(Project theProject) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.save(theProject);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void update(Project theProject) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theProject);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void delete(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Project theProject = findById(theId);
		currentSession.saveOrUpdate(theProject);

	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery = currentSession.createQuery("SELECT MAX(p.id) FROM Project p", Integer.class);

			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
