package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Province;

@Repository
public class ProvinceDaoImpl implements ProvinceDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ProvinceDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<Province> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Province> theQuery =
				currentSession.createQuery("from Province", Province.class);
		List<Province> provinces = theQuery.getResultList();
		return provinces;
	}

	@Override
	public Province findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Province theProvince =
				currentSession.get(Province.class, theId);
		return theProvince;
	}

	@Override
	public void save(Province theProvince) {

		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theProvince);
		currentSession.flush();

	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Province theProvince = findById(theId);
		//theProvince.setBlocked(true);

		currentSession.saveOrUpdate(theProvince);
	}

}
