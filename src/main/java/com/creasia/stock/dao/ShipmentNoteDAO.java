package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ShipmentNote;

public interface ShipmentNoteDAO {

	public List<ShipmentNote> findAll();
	
	public long countShipmentNotes();
	
	public List<ShipmentNote> findApprovedShipment();
	
	public List<ShipmentNote> findPostedShipment();

	public ShipmentNote findById(int theId);
	
	public List<ShipmentNote> findByRequestShipmentId(int theRequestId);

	public void save(ShipmentNote theShipmentNote);

	public void update(ShipmentNote theShipmentNote);

	public void delete(int theId);

	public int findNewCode();
	
	public String findLastCode();
}
