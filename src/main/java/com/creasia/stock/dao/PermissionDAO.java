package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Permission;

public interface PermissionDAO {

	public List<Permission> findAll();
	
	public Permission findById(int theId);
	
	public void save(Permission thePermission);
	
	public void update(Permission thePermission);
	
	public void delete(int theId);
	
	public int findNewCode();
}
