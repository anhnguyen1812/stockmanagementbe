package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ReturnReceipt;

@Repository
public class ReturnReceiptDaoImpl implements ReturnReceiptDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReturnReceiptDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReturnReceipt> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnReceipt> theQuery =
				currentSession.createQuery("from ReturnReceipt Order by  returned ASC, id DESC", ReturnReceipt.class);
		List<ReturnReceipt> returnReceipts = theQuery.getResultList();
		return returnReceipts;
	}

	@Override
	public List<ReturnReceipt> findByRequestShipmentId(int theRequestId){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnReceipt> theQuery =
				currentSession.createQuery("from ReturnReceipt sn Where sn.requestShipment.id = :theRequestId Order by sn.id DESC", ReturnReceipt.class);
		theQuery.setParameter("theRequestId", theRequestId);
		List<ReturnReceipt> returnReceipts = theQuery.getResultList();
		return returnReceipts;
	}
	@Override
	public ReturnReceipt findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReturnReceipt theReturnReceipt =
				currentSession.get(ReturnReceipt.class, theId);
		return theReturnReceipt;
	}

	@Override
	public void save(ReturnReceipt theReturnReceipt) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReturnReceipt);
			
			currentSession.clear();
			currentSession.save(theReturnReceipt);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReturnReceipt theReturnReceipt) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReturnReceipt);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReturnReceipt theReturnReceipt = findById(theId);
		currentSession.saveOrUpdate(theReturnReceipt);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReturnReceipt c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery =
					currentSession.createQuery("SELECT code FROM ReturnReceipt ORDER BY id DESC", String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
