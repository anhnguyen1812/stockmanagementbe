package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.ItemCategory;

@Repository
public class ItemCategoryDaoImpl implements ItemCategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	//define field for entitymanager
	private EntityManager entityManager;
	
	//setup constructor injection
	@Autowired
	public ItemCategoryDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ItemCategory> findAll() {
		//get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		//create a query 
		Query<ItemCategory> theQuery = 
				currentSession.createQuery("from ItemCategory  Order by id DESC", ItemCategory.class);
		
		//excute query
		List<ItemCategory> itemCategories = theQuery.getResultList();
		
		//return result
		return itemCategories;
	}
	
	@Override
	public ItemCategory findById(int theId) {
		//get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
				
		//create a query 
		ItemCategory theQuery = 
			currentSession.get(ItemCategory.class, theId);
				
		//return result
		return theQuery;
	}
	
	@Override
	public List<ItemCategory> findByName(String nameItemCategory) {
		List<ItemCategory> results = new ArrayList<ItemCategory>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ItemCategory> theQuery2 = currentSession.createQuery("FROM ItemCategory WHERE name = :name", ItemCategory.class);
			theQuery2.setParameter("name", nameItemCategory);
			results = theQuery2.getResultList();
		}catch (Exception e) {
			results = null;
		}

		return results;
	}
	
	
	@Override
	public void save(ItemCategory theItemCategory) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.save(theItemCategory);
		currentSession.flush();
		//currentSession.update(currentSession);
	}
	
	@Override
	public void update(ItemCategory theItemCategory) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theItemCategory);
		currentSession.flush();

	}
	
	@Override
	public void delete(int theId) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		ItemCategory theItemCategory = findById(theId);
		theItemCategory.setBlocked(true);
		currentSession.saveOrUpdate(theItemCategory);
		
		//Query theQuery = 
		//		currentSession.createQuery("delete from ItemCategory where id =:itemCategoryId");
		//theQuery.setParameter("itemCategoryId", theId);
	}
	

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery2 = currentSession
					.createQuery("SELECT MAX(id) FROM ItemCategory", Integer.class);
			maxId = theQuery2.getSingleResult();
		}catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
