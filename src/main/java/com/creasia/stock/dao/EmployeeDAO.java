package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Vendor;

public interface EmployeeDAO {

	public List<Employee> findAll();

	public Employee findById(int theId);

	public void save(Employee theEmployee);

	public void update(Employee Employee);

	public void delete(int theId);

	public int findNewCode();
}