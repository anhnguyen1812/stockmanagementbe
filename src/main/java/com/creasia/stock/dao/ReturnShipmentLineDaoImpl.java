package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ReturnShipmentLine;

@Repository
public class ReturnShipmentLineDaoImpl implements ReturnShipmentLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReturnShipmentLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReturnShipmentLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnShipmentLine> theQuery =
				currentSession.createQuery("from ReturnShipmentLine  Order by id DESC", ReturnShipmentLine.class);
		List<ReturnShipmentLine> shipmentNoteLines = theQuery.getResultList();
		return shipmentNoteLines;
	}

	@Override
	public ReturnShipmentLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReturnShipmentLine theReturnShipmentLine =
				currentSession.get(ReturnShipmentLine.class, theId);
		return theReturnShipmentLine;
	}

	@Override
	public void save(ReturnShipmentLine theReturnShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReturnShipmentLine);
			
			currentSession.clear();
			currentSession.save(theReturnShipmentLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReturnShipmentLine theReturnShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReturnShipmentLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReturnShipmentLine theReturnShipmentLine = findById(theId);
		currentSession.saveOrUpdate(theReturnShipmentLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReturnShipmentLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<ReturnShipmentLine> findByReturnShipmentId(int theReturnShipmentId) {
		
		List<ReturnShipmentLine> results = new ArrayList<ReturnShipmentLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ReturnShipmentLine> theQuery2 = currentSession
					.createQuery("FROM ReturnShipmentLine r WHERE r.ReturnShipment.id = :theReturnShipmentId", ReturnShipmentLine.class);
			theQuery2.setParameter("theReturnShipmentId", theReturnShipmentId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
