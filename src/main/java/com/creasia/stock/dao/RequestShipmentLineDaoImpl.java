package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.RequestShipmentLine;

@Repository
public class RequestShipmentLineDaoImpl implements RequestShipmentLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public RequestShipmentLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<RequestShipmentLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<RequestShipmentLine> theQuery =
				currentSession.createQuery("from RequestShipmentLine  Order by id DESC", RequestShipmentLine.class);
		List<RequestShipmentLine> requestShipmentLines = theQuery.getResultList();
		return requestShipmentLines;
	}

	@Override
	public RequestShipmentLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		RequestShipmentLine theRequestShipmentLine =
				currentSession.get(RequestShipmentLine.class, theId);
		return theRequestShipmentLine;
	}

	@Override
	public void save(RequestShipmentLine theRequestShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theRequestShipmentLine);
			
			currentSession.clear();
			currentSession.save(theRequestShipmentLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(RequestShipmentLine theRequestShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theRequestShipmentLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
//		RequestShipment theRequestShipment = findById(theId);
//		currentSession.saveOrUpdate(theRequestShipment);
		currentSession.clear();
		Query theQuery =
				currentSession.createQuery("delete from RequestShipmentLine rs where id =: theId");
		theQuery.setParameter("theId", theId);
		currentSession.flush();
		theQuery.executeUpdate();
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM RequestShipmentLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<RequestShipmentLine> findByRequestShipmentId(int theRequestShipmentId) {
		
		List<RequestShipmentLine> results = new ArrayList<RequestShipmentLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<RequestShipmentLine> theQuery2 = currentSession
					.createQuery("FROM RequestShipmentLine r WHERE r.RequestShipment.id = :theRequestShipmentId  Order by id DESC", RequestShipmentLine.class);
			theQuery2.setParameter("theRequestShipmentId", theRequestShipmentId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
