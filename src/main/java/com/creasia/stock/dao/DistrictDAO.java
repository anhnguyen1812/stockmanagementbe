package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Province;

public interface DistrictDAO {

public List<District> findAll();
	
	public District findById(int theId);
	
	public void save(District theDistrict);
	
	public void delete(int theId);
	
	public List<District> findDistrictByProvinceId(int theProvinceId);
}