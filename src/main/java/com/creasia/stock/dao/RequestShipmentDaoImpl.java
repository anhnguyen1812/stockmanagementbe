package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.RequestShipment;

@Repository
public class RequestShipmentDaoImpl implements RequestShipmentDAO {

	private EntityManager entityManager;
	
	@Autowired
	public RequestShipmentDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<RequestShipment> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<RequestShipment> theQuery =
				currentSession.createQuery("from RequestShipment rs where isDelete = false AND approved < 1 Order by approved ASC, id DESC", RequestShipment.class);
		List<RequestShipment> RequestShipments = theQuery.getResultList();
		return RequestShipments;
	}
	
	@Override
	public List<RequestShipment> findRequestVerified(int stage) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<RequestShipment> theQuery =
					currentSession.createQuery("from RequestShipment rs where approved >= :stage and isDelete = false Order by approved ASC, id DESC ", RequestShipment.class);
			theQuery.setParameter("stage", stage);
			List<RequestShipment> RequestShipments = theQuery.getResultList();
			return RequestShipments;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}

	@Override
	public RequestShipment findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		RequestShipment theRequestShipment =
				currentSession.get(RequestShipment.class, theId);
		return theRequestShipment;
	}

	@Override
	public void save(RequestShipment theRequestShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theRequestShipment);
			
			currentSession.clear();
			currentSession.save(theRequestShipment);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(RequestShipment theRequestShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theRequestShipment);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
//		RequestShipment theRequestShipment = findById(theId);
//		currentSession.saveOrUpdate(theRequestShipment);
		currentSession.clear();
		Query theQuery =
				currentSession.createQuery("delete from RequestShipment rs where id =: theId");
		theQuery.setParameter("theId", theId);
		currentSession.flush();
		theQuery.executeUpdate();
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM RequestShipment c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery =
					currentSession.createQuery("SELECT code FROM RequestShipment ORDER BY code DESC", String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
