package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.UoM;

public interface UomDAO {

	public List<UoM> findAll();
	
	public UoM findById(int theId);
	
	public List<UoM> findByName(String name);
	
	public void save(UoM theUoM);
	
	public void delete(int theId);
}
