package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.Customer;
import com.creasia.stock.entity.Vendor;

@Repository
public class CustomerDaoImpl implements CustomerDAO {

	private EntityManager entityManager;
	
	@Autowired
	public CustomerDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<Customer> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Customer> theQuery =
				currentSession.createQuery("from Customer  Order by id DESC", Customer.class);
		List<Customer> customers = theQuery.getResultList();
		return customers;
	}

	@Override
	public Customer findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Customer theCustomer =
				currentSession.get(Customer.class, theId);
		return theCustomer;
	}

	@Override
	public void save(Customer theCustomer) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theCustomer);
			
			currentSession.clear();
			currentSession.save(theCustomer);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(Customer theCustomer) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theCustomer);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Customer theCustomer = findById(theId);
		theCustomer.setBlocked(true);

		currentSession.saveOrUpdate(theCustomer);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM Customer c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
