package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Stock;

@Repository
public class StockDaoImpl implements StockDAO {

	private EntityManager entityManager;

	@Autowired
	public StockDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Stock> findAll() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Stock> theQuery = currentSession.createQuery("from Stock  Order by id DESC", Stock.class);
		List<Stock> stocks = theQuery.getResultList();
		return stocks;
	}

	@Override
	public long countStock() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM Stock as i where i.isBlocked = false", Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}
	@Override
	public Stock findById(int theId) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Stock stock = currentSession.get(Stock.class, theId);
		return stock;
	}

	@Override
	public void save(Stock theStock) {
		// TODO Auto-generated method stub
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.save(theStock);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Exception DAO: " + e.toString());
		}
	}

	@Override
	public void update(Stock theStock) {
		// TODO Auto-generated method stub
		try {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.clear();
		currentSession.saveOrUpdate(theStock);
		currentSession.flush();
		} catch (Exception e) {
			System.out.println("Exception DAO: " + e.toString());
		}
	}

	@Override
	public void delete(int theId) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Stock stock = findById(theId);
		stock.setBlocked(true);
		currentSession.saveOrUpdate(stock);
	}

	public int findNewCode(String theHeaderCode) {
		int countCode;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Stock> theQuery = currentSession
					.createQuery("SELECT s FROM Stock s where s.code like CONCAT(:headerCode,'%')", Stock.class);
			theQuery.setParameter("headerCode", theHeaderCode);

			List<Stock> stockList = theQuery.getResultList();
			countCode = stockList.size();
			System.out.println("countCode: " + countCode);
			// countCode = (int) theQuery.getSingleResult();
			// System.out.println("count Code: " + countCode);
		} catch (Exception e) {
			countCode = 0;
		}
		return countCode;
	}

}
