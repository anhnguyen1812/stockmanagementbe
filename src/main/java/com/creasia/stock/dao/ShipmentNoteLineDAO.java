package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ShipmentNoteLine;

public interface ShipmentNoteLineDAO {
	
	public List<ShipmentNoteLine> findAll();

	public ShipmentNoteLine findById(int theId);

	public void save(ShipmentNoteLine theShipmentNoteLine);

	public void update(ShipmentNoteLine theShipmentNoteLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<ShipmentNoteLine> findByShipmentNoteId(int theProvinceId);
}
