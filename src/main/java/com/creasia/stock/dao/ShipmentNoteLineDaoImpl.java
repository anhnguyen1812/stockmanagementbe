package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.ShipmentNoteLine;

@Repository
public class ShipmentNoteLineDaoImpl implements ShipmentNoteLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ShipmentNoteLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ShipmentNoteLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ShipmentNoteLine> theQuery =
				currentSession.createQuery("from ShipmentNoteLine  Order by id DESC", ShipmentNoteLine.class);
		List<ShipmentNoteLine> receiptNoteLines = theQuery.getResultList();
		return receiptNoteLines;
	}

	@Override
	public ShipmentNoteLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ShipmentNoteLine theShipmentNoteLine =
				currentSession.get(ShipmentNoteLine.class, theId);
		return theShipmentNoteLine;
	}

	@Override
	public void save(ShipmentNoteLine theShipmentNoteLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theShipmentNoteLine);
			
			currentSession.clear();
			currentSession.save(theShipmentNoteLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ShipmentNoteLine theShipmentNoteLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theShipmentNoteLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ShipmentNoteLine theShipmentNoteLine = findById(theId);
		currentSession.saveOrUpdate(theShipmentNoteLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ShipmentNoteLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<ShipmentNoteLine> findByShipmentNoteId(int theShipmentNoteId) {
		
		List<ShipmentNoteLine> results = new ArrayList<ShipmentNoteLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ShipmentNoteLine> theQuery2 = currentSession
					.createQuery("FROM ShipmentNoteLine r WHERE r.ShipmentNote.id = :theShipmentNoteId", ShipmentNoteLine.class);
			theQuery2.setParameter("theShipmentNoteId", theShipmentNoteId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
