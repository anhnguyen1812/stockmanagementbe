package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ReturnShipment;

@Repository
public class ReturnShipmentDaoImpl implements ReturnShipmentDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReturnShipmentDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReturnShipment> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnShipment> theQuery =
				currentSession.createQuery("from ReturnShipment Order by id DESC", ReturnShipment.class);
		List<ReturnShipment> returnShipments = theQuery.getResultList();
		return returnShipments;
	}

	@Override
	public List<ReturnShipment> findByRequestShipmentId(int theRequestId){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnShipment> theQuery =
				currentSession.createQuery("from ReturnShipment sn Where sn.requestShipment.id = :theRequestId Order by sn.id DESC", ReturnShipment.class);
		theQuery.setParameter("theRequestId", theRequestId);
		List<ReturnShipment> returnShipments = theQuery.getResultList();
		return returnShipments;
	}
	@Override
	public ReturnShipment findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReturnShipment theReturnShipment =
				currentSession.get(ReturnShipment.class, theId);
		return theReturnShipment;
	}

	@Override
	public void save(ReturnShipment theReturnShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReturnShipment);
			
			currentSession.clear();
			currentSession.save(theReturnShipment);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReturnShipment theReturnShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReturnShipment);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReturnShipment theReturnShipment = findById(theId);
		currentSession.saveOrUpdate(theReturnShipment);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReturnShipment c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery =
					currentSession.createQuery("SELECT code FROM ReturnShipment ORDER BY id DESC", String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
