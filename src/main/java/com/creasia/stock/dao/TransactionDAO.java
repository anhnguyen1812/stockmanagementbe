package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Report;
import com.creasia.stock.entity.Transaction;

public interface TransactionDAO {

	public List<Transaction> findAll();

	public Transaction findById(int theId);
	
	public long countTransactionInToday();
	
	public Transaction findByNoteCode(String theNoteCode);

	public void save(Transaction theTransaction);

	public void update(Transaction theTransaction);

	public void delete(int theId);

	public int findNewCode();
	
	public List<Object[]> getReportInMonth();
	
	public long findInventoryByStockAndProject(int stockId, int projectId, int itemId, int uomId);
	
	public long findInventoryByItemAndExpDate(int projectId, int stockId, int itemId, int uomId, String expDate);
	
	public List<Object[]> getReportByProjectIdStockIdExpDate(int stockId, int projectId, int itemId, int uomId);
	
	public long findInventory(int stockId, int itemId, int uomId);
	
	public List<Object[]> getExpirationSoon();
	
	public List<Object[]> getReportAll();
	
	public List<Object[]> getReportInStock();
	
	public List<Object[]> getReportByProject();
	
	public List<Object[]> getReportByStockId(int stockId);
	
	public List<Object[]> getItemLedgeEntry();
	
	public List<Object[]> getItemLedgeEntryByProject(int projectId);
	
	public List<Object[]> getReportStockTop();
	
	public List<Object[]> getReportProjectTop();
		
	public List<Object[]> getItemLedgeEntryByStock(int stockId);
	
	public List<Object[]> getAllReportByProject();
	
	public List<Object[]> getReportByProjectId(int projectId);
	
	public List<Object[]> getReportdetailByProjectIdItemId(int projectId, int itemId, int uoMId, String expDate);

	List<Object[]> getReportByItem();
}
