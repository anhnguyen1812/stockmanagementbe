package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.ItemCategory;

public interface CompanyDAO {

	public List<Company> findAll();
	
	public Company findById(int theId);
	
	public void save(Company theCompany);
	
	public void update(Company theCompany);
	
	public void delete(int theId);
	
	public int findNewCode();
}
