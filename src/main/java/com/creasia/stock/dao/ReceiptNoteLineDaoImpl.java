package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.ReceiptNoteLine;

@Repository
public class ReceiptNoteLineDaoImpl implements ReceiptNoteLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReceiptNoteLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReceiptNoteLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReceiptNoteLine> theQuery =
				currentSession.createQuery("from ReceiptNoteLine  Order by id DESC", ReceiptNoteLine.class);
		List<ReceiptNoteLine> receiptNoteLines = theQuery.getResultList();
		return receiptNoteLines;
	}

	@Override
	public ReceiptNoteLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReceiptNoteLine theReceiptNoteLine =
				currentSession.get(ReceiptNoteLine.class, theId);
		return theReceiptNoteLine;
	}

	@Override
	public void save(ReceiptNoteLine theReceiptNoteLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReceiptNoteLine);
			
			currentSession.clear();
			currentSession.save(theReceiptNoteLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReceiptNoteLine theReceiptNoteLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReceiptNoteLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReceiptNoteLine theReceiptNoteLine = findById(theId);
		currentSession.saveOrUpdate(theReceiptNoteLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReceiptNoteLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<ReceiptNoteLine> findByReceiptNoteId(int theReceiptNoteId) {
		
		List<ReceiptNoteLine> results = new ArrayList<ReceiptNoteLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ReceiptNoteLine> theQuery2 = currentSession
					.createQuery("FROM ReceiptNoteLine r WHERE r.ReceiptNote.id = :theReceiptNoteId  Order by id DESC", ReceiptNoteLine.class);
			theQuery2.setParameter("theReceiptNoteId", theReceiptNoteId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
