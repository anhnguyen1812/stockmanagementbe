package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Permission;

@Repository
public class PermissionDaoImpl implements PermissionDAO {

	//define field for entitymanager
	private EntityManager entityManager;
		
		//setup constructor injection
	@Autowired
	public PermissionDaoImpl(EntityManager theEntityManager) {
			entityManager = theEntityManager;
	}
			
	@Override
	public List<Permission> findAll() {

		Session currentSession = entityManager.unwrap(Session.class);
		Query<Permission> theQuery =
				currentSession.createQuery("from Permission  Order by id DESC", Permission.class);
		
		List<Permission> permissions = theQuery.getResultList();
		
		return permissions;
	}

	@Override
	public Permission findById(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Permission thePermission = currentSession.get(Permission.class, theId);
		return thePermission;
	}

	@Override
	public void save(Permission thePermission) {

		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.save(thePermission);
		currentSession.flush();
	}
	
	@Override
	public void update(Permission thePermission) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(thePermission);
		currentSession.flush();
	}
	
	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Permission thePermission = findById(theId);
		thePermission.setBlocked(true);
		
		currentSession.saveOrUpdate(thePermission);
		
	}
	
	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(v.id) FROM Permission v", Integer.class);
			
			 maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
