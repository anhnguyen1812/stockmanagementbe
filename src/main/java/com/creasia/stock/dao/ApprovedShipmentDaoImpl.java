package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ApprovedShipment;

@Repository
public class ApprovedShipmentDaoImpl implements ApprovedShipmentDAO{

	private EntityManager entityManager;

	@Autowired
	public ApprovedShipmentDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<ApprovedShipment> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ApprovedShipment> theQuery = currentSession.createQuery("from ApprovedShipment Order by isPosted, id DESC",
				ApprovedShipment.class);
		List<ApprovedShipment> approvedShipments = theQuery.getResultList();
		return approvedShipments;
	}

	@Override
	public List<ApprovedShipment> findApprovedShipment() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ApprovedShipment> theQuery = currentSession.createQuery(
				"from ApprovedShipment WHERE postedBy = null and postedDate = null Order by id DESC", ApprovedShipment.class);
		List<ApprovedShipment> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}

//	@Override
//	public List<ApprovedShipment> findPostedShipment() {
//		Session currentSession = entityManager.unwrap(Session.class);
//		Query<ApprovedShipment> theQuery = currentSession.createQuery(
//				"from ShipmentNote WHERE postedBy <> null and postedDate <> null Order by postedDate DESC",
//				ApprovedShipment.class);
//		List<ApprovedShipment> shipmentNotes = theQuery.getResultList();
//		return shipmentNotes;
//	}

	@Override
	public List<ApprovedShipment> findByRequestShipmentId(int theRequestId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ApprovedShipment> theQuery = currentSession.createQuery(
				"from ApprovedShipment sn Where sn.requestShipment.id = :theRequestId Order by sn.id DESC",
				ApprovedShipment.class);
		theQuery.setParameter("theRequestId", theRequestId);
		List<ApprovedShipment> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}

	@Override
	public ApprovedShipment findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ApprovedShipment theApprovedShipment = currentSession.get(ApprovedShipment.class, theId);
		return theApprovedShipment;
	}

	@Override
	public void save(ApprovedShipment theApprovedShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theApprovedShipment);

			currentSession.clear();
			currentSession.save(theApprovedShipment);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void update(ApprovedShipment theApprovedShipment) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theApprovedShipment);
			currentSession.flush();

		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}

	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ApprovedShipment theApprovedShipment = findById(theId);
		currentSession.saveOrUpdate(theApprovedShipment);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery = currentSession.createQuery("SELECT MAX(c.id) FROM ApprovedShipment c", Integer.class);

			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}

	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery = currentSession.createQuery("SELECT code FROM ApprovedShipment ORDER BY code DESC",
					String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
