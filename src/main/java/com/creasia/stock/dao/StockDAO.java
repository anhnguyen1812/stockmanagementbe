package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Vendor;

public interface StockDAO {
	
	public List<Stock> findAll();
	
	public Stock findById(int theId);
	
	public long countStock();
	
	public void save(Stock theStock);
	
	public void update(Stock theStock);
	
	public void delete(int theId);
	
	public int findNewCode(String theHeaderCode);
}
