package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReturnReceipt;

public interface ReturnReceiptDAO {

	public List<ReturnReceipt> findAll();

	public ReturnReceipt findById(int theId);
	
	public List<ReturnReceipt> findByRequestShipmentId(int theRequestId);

	public void save(ReturnReceipt theReturnReceipt);

	public void update(ReturnReceipt theReturnReceipt);

	public void delete(int theId);

	public int findNewCode();
	
	public String findLastCode();
}
