package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Role;

public interface RoleDAO {
	public List<Role> findAll();
}
