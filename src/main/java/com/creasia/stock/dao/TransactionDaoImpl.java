package com.creasia.stock.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Transaction;

@Repository
public class TransactionDaoImpl implements TransactionDAO {

	private EntityManager entityManager;

	@Autowired
	public TransactionDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Transaction> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Transaction> theQuery = currentSession.createQuery("from Transaction Order by id DESC",
				Transaction.class);
		List<Transaction> transactions = theQuery.getResultList();
		return transactions;
	}

	@Override
	public long countTransactionInToday() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession.createQuery(
					"SELECT count(i.id) FROM Transaction as i where i.postedDate = current_date and documentGroup <>0",
					Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public Transaction findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Transaction theTransaction = currentSession.get(Transaction.class, theId);
		return theTransaction;
	}

	@Override
	public void save(Transaction theTransaction) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theTransaction);

			currentSession.clear();
			currentSession.save(theTransaction);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void update(Transaction theTransaction) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theTransaction);
			currentSession.flush();

		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}

	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Transaction theTransaction = findById(theId);
		currentSession.saveOrUpdate(theTransaction);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery = currentSession.createQuery("SELECT MAX(c.id) FROM Transaction c", Integer.class);

			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}

	@Override
	public Transaction findByNoteCode(String theNoteCode) {

		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();

			Query<Transaction> theQuery = currentSession
					.createQuery("FROM Transaction c  WHERE c.noteCode like :theNoteCode", Transaction.class);
			theQuery.setParameter("theNoteCode", theNoteCode);
			Transaction result = theQuery.getSingleResult();
//			System.out.println("result notecode " + result);
			return result;
		} catch (Exception e) {
//			System.out.println("result Exception " +e.toString());
			return null;
		}
	}

	@Override
	public long findInventoryByStockAndProject(int stockId, int projectId, int itemId, int uomId) {
		long sumQuantity;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();

			Query<Long> theQuery = currentSession.createQuery(
					"SELECT sum(cl.postedQuantity) FROM Transaction c JOIN c.transactionLines cl  WHERE c.stock.id = :stockId AND  cl.item.id = :itemId AND  cl.uoM.id = :uomId and c.project.id = :projectId",
					Long.class);
			theQuery.setParameter("projectId", projectId);
			theQuery.setParameter("stockId", stockId);
			theQuery.setParameter("itemId", itemId);
			theQuery.setParameter("uomId", uomId);
			sumQuantity = theQuery.getSingleResult();
			currentSession.flush();
			return sumQuantity;
		} catch (NullPointerException e) {
			// System.out.println("NullPointerException= " + e.toString());
			return 0;
		} catch (Exception e) {
			System.out.println("Exception= " + e.toString());
			return -1;
		}
	}

	@Override
	public long findInventory(int stockId, int itemId, int uomId) {
		long sumQuantity;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();

			Query<Long> theQuery = currentSession.createQuery(
					"SELECT sum(cl.postedQuantity) FROM Transaction c JOIN c.transactionLines cl  WHERE c.stock.id = :stockId AND  cl.item.id = :itemId AND  cl.uoM.id = :uomId",
					Long.class);
			theQuery.setParameter("stockId", stockId);
			theQuery.setParameter("itemId", itemId);
			theQuery.setParameter("uomId", uomId);
			sumQuantity = theQuery.getSingleResult();
			currentSession.flush();
			return sumQuantity;
		} catch (NullPointerException e) {
			// System.out.println("NullPointerException= " + e.toString());
			return 0;
		} catch (Exception e) {
			System.out.println("Exception= " + e.toString());
			return -1;
		}

	}

	@Override
	public long findInventoryByItemAndExpDate(int projectId, int stockId, int itemId, int uomId, String expDate) {
		long sumQuantity;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();

			Query<Long> theQuery = currentSession.createQuery(
					"SELECT sum(cl.postedQuantity) FROM Transaction c JOIN c.transactionLines cl  WHERE c.project.id = :projectId and c.stock.id = :stockId AND  cl.item.id = :itemId AND  cl.uoM.id = :uomId AND  cl.expDate = :expDate",
					Long.class);
			theQuery.setParameter("projectId", projectId);
			theQuery.setParameter("stockId", stockId);
			theQuery.setParameter("itemId", itemId);
			theQuery.setParameter("uomId", uomId);
			theQuery.setParameter("expDate", expDate);
			sumQuantity = theQuery.getSingleResult();
			currentSession.flush();
			return sumQuantity;
		} catch (NullPointerException e) {
			// System.out.println("NullPointerException= " + e.toString());
			return 0;
		} catch (Exception e) {
			System.out.println("Exception= " + e.toString());
			return -1;
		}

	}

	@Override
	public List<Object[]> getReportAll() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.stock, tl.item, tl.uoM, sum(tl.postedQuantity) FROM Transaction as t JOIN t.transactionLines as tl WHERE tl.postedQuantity <>0 GROUP by t.stock, tl.item, tl.uoM  Order by t.stock.id DESC");

			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportInStock() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession
					.createQuery("SELECT t.stock, sum(t.totalPostedQuantity) FROM Transaction as t GROUP by t.stock");

			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportByProject() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.project, sum(t.totalPostedQuantity) FROM Transaction as t GROUP by t.project");

			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportByStockId(int stockId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.stock, tl.item, tl.uoM, sum(tl.postedQuantity) FROM Transaction as t JOIN t.transactionLines as tl  WHERE t.stock.id = :stockId AND tl.postedQuantity <>0 GROUP by t.stock, tl.item, tl.uoM  Order by t.stock.id DESC");
			theQuery.setParameter("stockId", stockId);
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportByItem() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			System.out.println("resultList:::: ");
			Query theQuery = currentSession.createQuery(
					"SELECT tl.item, sum(tl.postedQuantity) as qty FROM TransactionLine as tl  GROUP by tl.item order by qty desc");
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			System.out.println("resultList:::: " + resultList);
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getItemLedgeEntry() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"Select t, tl FROM Transaction as t JOIN t.transactionLines as tl  Order by t.id DESC");
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getItemLedgeEntryByProject(int projectId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"Select t, tl FROM Transaction as t JOIN t.transactionLines as tl WHERE t.project.id  = :projectId and  tl.postedQuantity <>0  Order by t.id DESC");
			theQuery.setParameter("projectId", projectId);
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getItemLedgeEntryByStock(int stockId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"Select t, tl FROM Transaction as t JOIN t.transactionLines as tl WHERE t.stock.id = :stockId and  tl.postedQuantity <>0  Order by t.id DESC");
			theQuery.setParameter("stockId", stockId);
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getAllReportByProject() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT  t.project, tl.item, tl.uoM, tl.expDate , t.documentGroup, sum(tl.postedQuantity) FROM Transaction as t JOIN t.transactionLines as tl WHERE tl.postedQuantity <>0 GROUP by t.project, tl.item, tl.uoM, tl.expDate, t.documentGroup HAVING sum(tl.postedQuantity)<>0 Order by t.project.id DESC");
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error: "+ e.toString());
			return null;
		}
	}

	@Override
	public List<Object[]> getReportStockTop() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT  t.stock, t.documentGroup, sum(tl.postedQuantity) as qty FROM Transaction as t JOIN t.transactionLines as tl GROUP by t.stock, t.documentGroup order by qty desc ");
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportProjectTop() {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT  t.project, t.documentGroup, sum(tl.postedQuantity) as qty FROM Transaction as t JOIN t.transactionLines as tl where t.project.isBlocked = false GROUP by t.project, t.documentGroup order by qty desc");
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportInMonth() {
		try {
			LocalDate now = LocalDate.now();
			int month = now.getMonth().getValue();
			int year = now.getYear();
			if (month == 1) {
				year = year -1;
				month = 12;
			} else if (month > 1) {
				month = month -1;
			}
			LocalDate lastMonth = now.plusDays ( -30 );
			
			System.out.println("lastMonth lastMonth : " + lastMonth.toString().getClass());
			System.out.println("now now : " + now.getClass());
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.documentDate, DAY(t.documentDate), MONTH(t.documentDate) , t.documentGroup, sum(tl.postedQuantity) as qty FROM Transaction as t JOIN t.transactionLines as tl WHERE t.postedDate >= :lastMonth AND t.postedDate <= :now GROUP by t.documentDate, DAY(t.documentDate), MONTH(t.documentDate) , t.documentGroup order by t.documentDate");
			theQuery.setParameter("now", now.toString());
			theQuery.setParameter("lastMonth", lastMonth.toString());
			List<Object[]> resultList = theQuery.getResultList();
			System.out.println("$$^^^^ day : " + ((Number) resultList.get(0)[1]).intValue());
			System.out.println("$$^^^^ month : " + ((Number) resultList.get(0)[2]).intValue());
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportByProjectId(int projectId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.project, tl.item, tl.uoM, tl.expDate , t.documentGroup, sum(tl.postedQuantity) FROM Transaction as t JOIN t.transactionLines as tl  WHERE t.project.id = :projectId AND tl.postedQuantity <>0 GROUP by t.project, tl.item, tl.uoM, tl.expDate, t.documentGroup");
			theQuery.setParameter("projectId", projectId);
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportdetailByProjectIdItemId(int projectId, int itemId, int uoMId, String expDate) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			String sql = "";
			Query theQuery;
//			System.out.println("type of :: " + expDate.getClass().getName() );
			if (expDate.equals("null")  || expDate == null) {
				System.out.println("exp === null");
				theQuery = currentSession.createQuery("SELECT t, tl.item, tl.uoM, tl.postedQuantity FROM Transaction as t JOIN t.transactionLines as tl  WHERE t.project.id = :projectId AND tl.item.id = :itemId AND tl.uoM.id = :uoMId AND tl.expDate is NULL AND tl.postedQuantity <>0");
				theQuery.setParameter("projectId", projectId);
				theQuery.setParameter("itemId", itemId);
				theQuery.setParameter("uoMId", uoMId);
			} else {
				System.out.println("exp !== null");
				theQuery = currentSession.createQuery("SELECT t, tl.item, tl.uoM, tl.postedQuantity FROM Transaction as t JOIN t.transactionLines as tl  WHERE t.project.id = :projectId AND tl.item.id = :itemId AND tl.uoM.id = :uoMId AND tl.expDate = :expDate AND tl.postedQuantity <>0");
				theQuery.setParameter("projectId", projectId);
				theQuery.setParameter("itemId", itemId);
				theQuery.setParameter("uoMId", uoMId); // expDate
				theQuery.setParameter("expDate", expDate);
			}
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public List<Object[]> getReportByProjectIdStockIdExpDate(int stockId, int projectId, int itemId, int uomId) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			Query theQuery = currentSession.createQuery(
					"SELECT t.stock, t.project, tl.item, tl.uoM, tl.expDate , sum(tl.postedQuantity) FROM Transaction as t JOIN t.transactionLines as tl  WHERE t.stock.id = :stockId AND  tl.item.id = :itemId AND  tl.uoM.id = :uomId and t.project.id = :projectId AND tl.postedQuantity <>0 GROUP by t.stock, t.project, tl.item, tl.uoM, tl.expDate");
			theQuery.setParameter("projectId", projectId);
			theQuery.setParameter("stockId", stockId);
			theQuery.setParameter("itemId", itemId);
			theQuery.setParameter("uomId", uomId);
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	@Override
	public List<Object[]> getExpirationSoon() {
		
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			LocalDate now = LocalDate.now();
			LocalDate nextMonth = now.plusDays (30);
			Query theQuery = currentSession.createQuery(
					"SELECT t.stock, tl.item, tl.expDate, sum(tl.postedQuantity) FROM Transaction t JOIN t.transactionLines tl  WHERE (tl.expDate >= :now AND tl.expDate <= :nextMonth) group by t.stock, tl.item, tl.expDate  HAVING sum(tl.postedQuantity)>0 ");
			theQuery.setParameter("now", now.toString());
			theQuery.setParameter("nextMonth", nextMonth.toString());
			List<Object[]> resultList = theQuery.getResultList();
			currentSession.flush();
			return resultList;
		} catch (Exception e) {
			System.out.println("Exception= " + e.toString());
			return null;
		}
		
	}
}
