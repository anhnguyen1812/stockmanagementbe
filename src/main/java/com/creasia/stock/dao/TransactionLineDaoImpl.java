package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.TransactionLine;

@Repository
public class TransactionLineDaoImpl implements TransactionLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public TransactionLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<TransactionLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<TransactionLine> theQuery =
				currentSession.createQuery("from TransactionLine ", TransactionLine.class);
		List<TransactionLine> transactionLines = theQuery.getResultList();
		return transactionLines;
	}

	@Override
	public TransactionLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		TransactionLine theTransactionLine =
				currentSession.get(TransactionLine.class, theId);
		return theTransactionLine;
	}

	@Override
	public void save(TransactionLine theTransactionLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theTransactionLine);
			
			currentSession.clear();
			currentSession.save(theTransactionLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(TransactionLine theTransactionLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theTransactionLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		TransactionLine theTransactionLine = findById(theId);
		currentSession.saveOrUpdate(theTransactionLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM TransactionLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<TransactionLine> findByTransactionId(int theTransactionId) {
		
		List<TransactionLine> results = new ArrayList<TransactionLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<TransactionLine> theQuery2 = currentSession
					.createQuery("FROM TransactionLine r WHERE r.Transaction.id = :theTransactionId", TransactionLine.class);
			theQuery2.setParameter("theTransactionId", theTransactionId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
