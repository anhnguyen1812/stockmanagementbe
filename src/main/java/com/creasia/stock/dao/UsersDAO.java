package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.User;

public interface UsersDAO {
	
	public List<User> findAll();

	public User findByUsenamePassword(int theId);
	
	public User findById(long theId);
	
	public List<User> findByEmployeeId(int theEmployeeId);
	
	public void save(User theUser);
	
	public void update(User theUser);
	
	public List<User> findUserPost();
}
