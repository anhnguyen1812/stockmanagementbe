package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Company;

@Repository
public class CompanyDaoImpl implements CompanyDAO {

	//define field for entitymanager
	private EntityManager entityManager;
				
	//setup constructor injection
	@Autowired
	public CompanyDaoImpl(EntityManager theEntityManager) {
			entityManager = theEntityManager;
	}
	
	@Override
	public List<Company> findAll() {

		Session currentSession = entityManager.unwrap(Session.class);
		
		//create a query 
		Query<Company> theQuery = 
			currentSession.createQuery("from Company  Order by id DESC", Company.class);
		
		List<Company> Companys = theQuery.getResultList();
		return Companys;
	}

	@Override
	public Company findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Company theQuery = currentSession.get(Company.class, theId);
		return theQuery;
	}

	@Override
	public void save(Company theCompany) {
		
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.save(theCompany);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(Company theCompany) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theCompany);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void delete(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Company theCompany = findById(theId);
		theCompany.setBlocked(true);
		currentSession.saveOrUpdate(theCompany);

	}
	
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM Company c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
