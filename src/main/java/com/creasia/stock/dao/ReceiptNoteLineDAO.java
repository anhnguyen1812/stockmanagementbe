package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReceiptNoteLine;

public interface ReceiptNoteLineDAO {
	
	public List<ReceiptNoteLine> findAll();

	public ReceiptNoteLine findById(int theId);

	public void save(ReceiptNoteLine theReceiptNoteLine);

	public void update(ReceiptNoteLine theReceiptNoteLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<ReceiptNoteLine> findByReceiptNoteId(int theProvinceId);
}
