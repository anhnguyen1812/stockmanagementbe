package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ReturnReceiptLine;

@Repository
public class ReturnReceiptLineDaoImpl implements ReturnReceiptLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReturnReceiptLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReturnReceiptLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReturnReceiptLine> theQuery =
				currentSession.createQuery("from ReturnReceiptLine  Order by id DESC", ReturnReceiptLine.class);
		List<ReturnReceiptLine> receiptNoteLines = theQuery.getResultList();
		return receiptNoteLines;
	}

	@Override
	public ReturnReceiptLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReturnReceiptLine theReturnReceiptLine =
				currentSession.get(ReturnReceiptLine.class, theId);
		return theReturnReceiptLine;
	}

	@Override
	public void save(ReturnReceiptLine theReturnReceiptLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReturnReceiptLine);
			
			currentSession.clear();
			currentSession.save(theReturnReceiptLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReturnReceiptLine theReturnReceiptLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReturnReceiptLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReturnReceiptLine theReturnReceiptLine = findById(theId);
		currentSession.saveOrUpdate(theReturnReceiptLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReturnReceiptLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<ReturnReceiptLine> findByReturnReceiptId(int theReturnReceiptId) {
		
		List<ReturnReceiptLine> results = new ArrayList<ReturnReceiptLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ReturnReceiptLine> theQuery2 = currentSession
					.createQuery("FROM ReturnReceiptLine r WHERE r.ReturnReceipt.id = :theReturnReceiptId", ReturnReceiptLine.class);
			theQuery2.setParameter("theReturnReceiptId", theReturnReceiptId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
