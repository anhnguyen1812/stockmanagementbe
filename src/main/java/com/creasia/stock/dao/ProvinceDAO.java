package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Province;

public interface ProvinceDAO {

public List<Province> findAll();
	
	public Province findById(int theId);
	
	public void save(Province theProvince);
	
	public void delete(int theId);
}
