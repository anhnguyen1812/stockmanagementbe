package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.creasia.stock.entity.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDAO {

	// define field for entitymanager
	private EntityManager entityManager;

	// setup constructor injection
	@Autowired
	public EmployeeDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Employee> findAll() {

		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee  Order by id DESC", Employee.class);

		List<Employee> employees = theQuery.getResultList();

		return employees;
	}

	@Override
	public Employee findById(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Employee theEmployee = currentSession.get(Employee.class, theId);
		return theEmployee;
	}

	@Override
	public void save(Employee theEmployee) {

		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.clear();
		currentSession.save(theEmployee);
		currentSession.flush();
	}

	@Override
	public void update(Employee theEmployee) {
		try {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.clear();
		currentSession.saveOrUpdate(theEmployee);
		currentSession.flush();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("errorr :: " + e.toString());
		}
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Employee theEmployee = findById(theId);
		theEmployee.setBlocked(true);

		currentSession.saveOrUpdate(theEmployee);

	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery = currentSession.createQuery("SELECT MAX(e.id) FROM Employee e", Integer.class);

			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
