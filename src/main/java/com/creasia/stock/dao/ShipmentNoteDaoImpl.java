package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ShipmentNote;

@Repository
public class ShipmentNoteDaoImpl implements ShipmentNoteDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ShipmentNoteDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ShipmentNote> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ShipmentNote> theQuery =
				currentSession.createQuery("from ShipmentNote Order by id DESC", ShipmentNote.class);
		List<ShipmentNote> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}
	
	@Override
	public long countShipmentNotes() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM ShipmentNote as i ", Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public List<ShipmentNote> findApprovedShipment(){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ShipmentNote> theQuery =
				currentSession.createQuery("from ShipmentNote WHERE postedBy = null and postedDate = null Order by id DESC", ShipmentNote.class);
		List<ShipmentNote> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}

	@Override
	public List<ShipmentNote> findPostedShipment(){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ShipmentNote> theQuery =
				currentSession.createQuery("from ShipmentNote WHERE postedBy <> null and postedDate <> null Order by postedDate DESC", ShipmentNote.class);
		List<ShipmentNote> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}
	
	@Override
	public List<ShipmentNote> findByRequestShipmentId(int theRequestId){
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ShipmentNote> theQuery =
				currentSession.createQuery("from ShipmentNote sn Where sn.requestShipment.id = :theRequestId Order by sn.id DESC", ShipmentNote.class);
		theQuery.setParameter("theRequestId", theRequestId);
		List<ShipmentNote> shipmentNotes = theQuery.getResultList();
		return shipmentNotes;
	}
	@Override
	public ShipmentNote findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ShipmentNote theShipmentNote =
				currentSession.get(ShipmentNote.class, theId);
		return theShipmentNote;
	}

	@Override
	public void save(ShipmentNote theShipmentNote) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theShipmentNote);
			
			currentSession.clear();
			currentSession.save(theShipmentNote);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ShipmentNote theShipmentNote) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theShipmentNote);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ShipmentNote theShipmentNote = findById(theId);
		currentSession.saveOrUpdate(theShipmentNote);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ShipmentNote c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery =
					currentSession.createQuery("SELECT code FROM ShipmentNote ORDER BY code DESC", String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
