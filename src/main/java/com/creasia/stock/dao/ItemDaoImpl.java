package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.TransactionLine;

@Repository
public class ItemDaoImpl implements ItemDAO {

	//define field for entitymanager
	private EntityManager entityManager;
			
	//setup constructor injection
	@Autowired
	public ItemDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
		
	@Override
	public List<Item> findAll() {
		// TODO Auto-generated method stub
		//get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
				
		//create a query 
		Query<Item> theQuery = 
			currentSession.createQuery("from Item  Order by id DESC", Item.class);
				
		//excute query
		List<Item> items = theQuery.getResultList();
				
		//return result
		return items;
	}
	
	@Override
	public long countItem() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM Item as i where i.isBlocked = false", Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public Item findById(int theId) {
		//get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);						
		//create a query 
		Item theQuery = currentSession.get(Item.class, theId);
						
		//return result
		return theQuery;
	}

	@Override
	public void save(Item theItem) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theItem);
		currentSession.flush();
	}

	@Override
	public void delete(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Item theItem = findById(theId);
		theItem.setBlocked(true);
		currentSession.saveOrUpdate(theItem);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery2 = currentSession
					.createQuery("SELECT MAX(id) FROM Item", Integer.class);
			maxId = theQuery2.getSingleResult();
		}catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<Item> findByNameAndUoM(String nameItem, int uomId) {
		List<Item> results = new ArrayList<Item>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Item> theQuery2 = currentSession.createQuery("FROM Item i WHERE i.uoM.id = :uomId and i.name = :nameItem", Item.class);
			theQuery2.setParameter("nameItem", nameItem);
			theQuery2.setParameter("uomId", uomId);
			results = theQuery2.getResultList();
		}catch (Exception e) {
			results = null;
		}

		return results;
	}
}
