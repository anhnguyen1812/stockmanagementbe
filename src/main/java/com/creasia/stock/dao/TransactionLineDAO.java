package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.TransactionLine;

public interface TransactionLineDAO {
	
	public List<TransactionLine> findAll();

	public TransactionLine findById(int theId);

	public void save(TransactionLine theTransactionLine);

	public void update(TransactionLine theTransactionLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<TransactionLine> findByTransactionId(int theProvinceId);
}
