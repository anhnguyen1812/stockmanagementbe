package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Department;
import com.creasia.stock.entity.District;

@Repository
public class DepartmentDaoImpl implements DepartmentDAO {

	//define field for entitymanager
	private EntityManager entityManager;
		
		//setup constructor injection
	@Autowired
	public DepartmentDaoImpl(EntityManager theEntityManager) {
			entityManager = theEntityManager;
	}
			
	@Override
	public List<Department> findAll() {

		Session currentSession = entityManager.unwrap(Session.class);
		Query<Department> theQuery =
				currentSession.createQuery("from Department  Order by id DESC", Department.class);
		
		List<Department> departments = theQuery.getResultList();
		
		return departments;
	}

	public List<Department> findDepartmentByCompanyId(int companyId){
		List<Department> results = new ArrayList<Department>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Department> theQuery2 = currentSession
					.createQuery("FROM Department D WHERE D.company.id = :companyId", Department.class);
			theQuery2.setParameter("companyId", companyId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
	
	@Override
	public Department findById(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Department theDepartment = currentSession.get(Department.class, theId);
		return theDepartment;
	}

	@Override
	public void save(Department theDepartment) {

		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.save(theDepartment);
		currentSession.flush();
	}
	
	@Override
	public void update(Department theDepartment) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theDepartment);
		currentSession.flush();
	}
	
	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		Department theDepartment = findById(theId);
		
		currentSession.delete(theDepartment);
		
	}
	
	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(v.id) FROM Department v", Integer.class);
			
			 maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
}
