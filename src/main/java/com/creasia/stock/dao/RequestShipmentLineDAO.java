package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.RequestShipmentLine;

public interface RequestShipmentLineDAO {
	
	public List<RequestShipmentLine> findAll();

	public RequestShipmentLine findById(int theId);

	public void save(RequestShipmentLine theRequestShipmentLine);

	public void update(RequestShipmentLine theRequestShipmentLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<RequestShipmentLine> findByRequestShipmentId(int theProvinceId);
}
