package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ReceiptNote;

@Repository
public class ReceiptNoteDaoImpl implements ReceiptNoteDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ReceiptNoteDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ReceiptNote> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ReceiptNote> theQuery =
				currentSession.createQuery("from ReceiptNote Order by id DESC", ReceiptNote.class);
		List<ReceiptNote> receiptNotes = theQuery.getResultList();
		return receiptNotes;
	}
	
	@Override
	public long countReceiptNotes() {
		long count = 0;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Long> theQuery2 = currentSession
					.createQuery("SELECT count(i.id) FROM ReceiptNote as i ", Long.class);
			System.out.println("count count:: " + theQuery2.getSingleResult());
			return (long) theQuery2.getSingleResult();
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public ReceiptNote findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ReceiptNote theReceiptNote =
				currentSession.get(ReceiptNote.class, theId);
		return theReceiptNote;
	}

	@Override
	public void save(ReceiptNote theReceiptNote) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theReceiptNote);
			
			currentSession.clear();
			currentSession.save(theReceiptNote);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ReceiptNote theReceiptNote) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theReceiptNote);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ReceiptNote theReceiptNote = findById(theId);
		currentSession.saveOrUpdate(theReceiptNote);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ReceiptNote c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public String findLastCode() {
		String maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<String> theQuery =
					currentSession.createQuery("SELECT code FROM ReceiptNote ORDER BY code DESC", String.class);
			maxId = theQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			maxId = "";
		}
		return maxId;
	}
}
