package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.RequestShipment;

public interface RequestShipmentDAO {
	
	public List<RequestShipment> findAll();

	public RequestShipment findById(int theId);

	public void save(RequestShipment theRequestShipment);

	public void update(RequestShipment theRequestShipment);

	public void delete(int theId);

	public int findNewCode();
	
	public String findLastCode();
	
	public List<RequestShipment> findRequestVerified(int stage);
}
