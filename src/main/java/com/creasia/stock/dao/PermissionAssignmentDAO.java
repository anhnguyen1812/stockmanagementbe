package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;

public interface PermissionAssignmentDAO {
	
	public List<PermissionAssignment> findAll();

	public PermissionAssignment findById(int theId);

	public void save(PermissionAssignment theUserAssignmentStock);
	
	public List<User> getUserByStockId(int stockId);

	public void update(PermissionAssignment per);

	public List<PermissionAssignment> getPermissionByUser(long userId);

	public List<Employee> getUserAdmin();

	public Employee getUserManager(int userId);

//	public void update(int theId);

}
