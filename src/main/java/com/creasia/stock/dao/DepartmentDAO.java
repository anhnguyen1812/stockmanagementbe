package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.Department;

public interface DepartmentDAO {

	public List<Department> findAll();

	public Department findById(int theId);

	public void save(Department theDepartment);

	public void update(Department theDepartment);

	public void delete(int theId);

	public int findNewCode();

	public List<Department> findDepartmentByCompanyId(int companyId);
}
