package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ApprovedShipmentLine;

@Repository
public class ApprovedShipmentLineDaoImpl implements ApprovedShipmentLineDAO {

	private EntityManager entityManager;
	
	@Autowired
	public ApprovedShipmentLineDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<ApprovedShipmentLine> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<ApprovedShipmentLine> theQuery =
				currentSession.createQuery("from ApprovedShipmentLine  Order by id DESC", ApprovedShipmentLine.class);
		List<ApprovedShipmentLine> receiptNoteLines = theQuery.getResultList();
		return receiptNoteLines;
	}

	@Override
	public ApprovedShipmentLine findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		ApprovedShipmentLine theApprovedShipmentLine =
				currentSession.get(ApprovedShipmentLine.class, theId);
		return theApprovedShipmentLine;
	}

	@Override
	public void save(ApprovedShipmentLine theApprovedShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			System.out.println("currentSession: " + theApprovedShipmentLine);
			
			currentSession.clear();
			currentSession.save(theApprovedShipmentLine);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
	
	@Override
	public void update(ApprovedShipmentLine theApprovedShipmentLine) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theApprovedShipmentLine);
			currentSession.flush();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		ApprovedShipmentLine theApprovedShipmentLine = findById(theId);
		currentSession.saveOrUpdate(theApprovedShipmentLine);
	}

	@Override
	public int findNewCode() {
		int maxId;
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Integer> theQuery =
					currentSession.createQuery("SELECT MAX(c.id) FROM ApprovedShipmentLine c", Integer.class);
			
			maxId = theQuery.getSingleResult();
		} catch (Exception e) {
			maxId = 0;
		}
		return maxId;
	}
	
	@Override
	public List<ApprovedShipmentLine> findByApprovedShipmentId(int theApprovedShipmentId) {
		
		List<ApprovedShipmentLine> results = new ArrayList<ApprovedShipmentLine>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<ApprovedShipmentLine> theQuery2 = currentSession
					.createQuery("FROM ApprovedShipmentLine r WHERE r.ApprovedShipment.id = :theApprovedShipmentId", ApprovedShipmentLine.class);
			theQuery2.setParameter("theApprovedShipmentId", theApprovedShipmentId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}
}
