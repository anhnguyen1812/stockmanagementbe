package com.creasia.stock.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.ReceiptNote;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.User;
import com.creasia.stock.entity.Vendor;

@Repository
public class UsersDaoImpl implements UsersDAO {

	private EntityManager entityManager;

	// setup constructor injection
	@Autowired
	public UsersDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<User> findAll() {
		try {
			System.out.println("error :: :)) --- " );
			Session currentSession = entityManager.unwrap(Session.class);
			Query<User> theQuery = currentSession.createQuery("from User Order by id DESC", User.class);

			List<User> users = theQuery.getResultList();

			return users;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error :: :)) --- " + e.toString());
			return null;
		}
		
	}
	
//	@Override
//	public List<User> findAll() {
//		try {
//			Session currentSession = entityManager.unwrap(Session.class);
//			Query<User> theQuery = currentSession.createQuery("select pa.users from PermissionAssignment pa JOIN pa.users Order by pa.id DESC", User.class);
//
//			List<User> users = theQuery.getResultList();
//
//			return users;
//		} catch (Exception e) {
//			System.out.println("error:: " + e.toString());
//			// TODO: handle exception
//			return null;
//		}
//		
//	}
	@Override
	public User findById(long theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		User theUser = currentSession.get(User.class, theId);
		return theUser;
	}
	
	@Override
	public List<User> findByEmployeeId(int theEmployeeId) {
		System.out.println("theEmployeeId " + theEmployeeId);
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();

			Query<User> theQuery = currentSession
					.createQuery("FROM User c  WHERE c.employee.id =:theEmployeeId", User.class);
			theQuery.setParameter("theEmployeeId", theEmployeeId);
			List<User> result = theQuery.getResultList();
			System.out.println("result theEmployeeId " + result);
			return result;
		} catch (Exception e) {
			System.out.println("result Exception " +e.toString());
			return null;
		}
	}

	@Override
	public User findByUsenamePassword(int theId) {

		return null;
	}

	@Override
	public void save(User theUser) {
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.save(theUser);
			currentSession.flush();
		}catch(Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}

	@Override
	public void update(User theUser) {
		try {
			System.out.println("user2::: " + theUser.getRoles());
			Session currentSession = entityManager.unwrap(Session.class);
			currentSession.clear();
			currentSession.saveOrUpdate(theUser);
			System.out.println("user2::: " + theUser);
			currentSession.flush();
		} catch (Exception e) {
			System.out.println("user2::: " + e.toString());
		}
	}
	
	@Override
	public List<User> findUserPost(){
		
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<User> theQuery =
					currentSession.createQuery("SELECT u FROM User u ", User.class);
			List<User> users = theQuery.getResultList();
			return users;
			
		} catch (NullPointerException e) {
			//System.out.println("NullPointerException= " + e.toString());
			return null;
		}catch (Exception e) {
			System.out.println("Exception= " + e.toString());
			return null;
		}
	}
}
