package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReturnReceiptLine;

public interface ReturnReceiptLineDAO {
	
	public List<ReturnReceiptLine> findAll();

	public ReturnReceiptLine findById(int theId);

	public void save(ReturnReceiptLine theReturnReceiptLine);

	public void update(ReturnReceiptLine theReturnReceiptLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<ReturnReceiptLine> findByReturnReceiptId(int theProvinceId);
}
