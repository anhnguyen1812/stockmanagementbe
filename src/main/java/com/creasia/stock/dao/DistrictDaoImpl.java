package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.entity.Province;

@Repository
public class DistrictDaoImpl implements DistrictDAO {

	private EntityManager entityManager;

	@Autowired
	public DistrictDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<District> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<District> theQuery = currentSession.createQuery("from District", District.class);
		List<District> districts = theQuery.getResultList();
		return districts;
	}

	@Override
	public District findById(int theId) {
		System.out.println(theId);
		Session currentSession = entityManager.unwrap(Session.class);
		District theDistrict = currentSession.get(District.class, theId);
		System.out.println("district" + theDistrict);
		return theDistrict;
	}

	@Override
	public void save(District theDistrict) {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.clear();
		currentSession.saveOrUpdate(theDistrict);
		currentSession.flush();
	}

	@Override
	public void delete(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		District theDistrict = findById(theId);
		currentSession.saveOrUpdate(theDistrict);
	}

	@Override
	public List<District> findDistrictByProvinceId(int theProvinceId) {
		List<District> results = new ArrayList<District>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<District> theQuery2 = currentSession
					.createQuery("FROM District D WHERE D.province.id = :theProvinceId", District.class);
			theQuery2.setParameter("theProvinceId", theProvinceId);
			// System.out.print("theProvinces dao : " + theProvinceId);
			results = theQuery2.getResultList();

			System.out.print("theResults : " + results);
			return results;
		} catch (Exception e) {
			System.out.println("exception dao: " + e.toString());
		}
		return results;
	}

//	@Override
//	public List<District> findDistrictByProvinceId(int theProvinceId) {
//		Session currentSession = entityManager.unwrap(Session.class);
//		Query<District> theQuery = currentSession.createQuery("from District where id =: theProvinceId", District.class);
//		theQuery.setParameter("theProvinceid", theProvinceId);
//		List<District> districts = theQuery.getResultList();
//		return districts;
//	}
}
