package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ReturnShipmentLine;

public interface ReturnShipmentLineDAO {
	
	public List<ReturnShipmentLine> findAll();

	public ReturnShipmentLine findById(int theId);

	public void save(ReturnShipmentLine theReturnShipmentLine);

	public void update(ReturnShipmentLine theReturnShipmentLine);

	public void delete(int theId);

	public int findNewCode();
	
	public List<ReturnShipmentLine> findByReturnShipmentId(int theProvinceId);
}
