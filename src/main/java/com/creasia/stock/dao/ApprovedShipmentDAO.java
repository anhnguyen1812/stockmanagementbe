package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ApprovedShipment;

public interface ApprovedShipmentDAO {

	public List<ApprovedShipment> findAll();

	public List<ApprovedShipment> findApprovedShipment();

//	public List<ApprovedShipment> findPostedShipment();

	public ApprovedShipment findById(int theId);

	public List<ApprovedShipment> findByRequestShipmentId(int theRequestId);

	public void save(ApprovedShipment theApprovedShipment);

	public void update(ApprovedShipment theApprovedShipment);

	public void delete(int theId);

	public int findNewCode();

	public String findLastCode();
}
