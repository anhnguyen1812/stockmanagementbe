package com.creasia.stock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.entity.UoM;

@Repository
public class UomDaoImpl implements UomDAO {

	private EntityManager entityManager;
	
	@Autowired
	public UomDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<UoM> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<UoM> theQuery =
				currentSession.createQuery("from UoM  Order by id DESC", UoM.class);
		List<UoM> uoMs = theQuery.getResultList();
		return uoMs;
	}

	@Override
	public List<UoM> findByName(String nameUoM) {
		List<UoM> results = new ArrayList<UoM>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);
			Query<UoM> theQuery2 = currentSession.createQuery("FROM UoM WHERE name = :name", UoM.class);
			theQuery2.setParameter("name", nameUoM);
			results = theQuery2.getResultList();
		}catch (Exception e) {
			results = null;
		}

		return results;
	}
	
	public UoM findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		UoM uoM = currentSession.get(UoM.class, theId);
		
		return uoM;
	}

	@Override
	public void save(UoM theUoM) {

		Session currentSession = entityManager.unwrap(Session.class);
		
		currentSession.clear();
		currentSession.saveOrUpdate(theUoM);
		currentSession.flush();
		
	}

	@Override
	public void delete(int theId) {

		Session currentSession = entityManager.unwrap(Session.class);
		UoM uoM = findById(theId);
		uoM.setBlocked(true);
		
		currentSession.saveOrUpdate(uoM);
		
	}

}
