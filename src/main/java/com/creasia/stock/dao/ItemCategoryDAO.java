package com.creasia.stock.dao;

import java.util.List;

import com.creasia.stock.entity.ItemCategory;

public interface ItemCategoryDAO {

	public List<ItemCategory> findAll();
	
	public ItemCategory findById(int theId);
	public List<ItemCategory> findByName(String name);
	
	public int findNewCode();
	
	public void save(ItemCategory theItemCategory);
	
	public void update(ItemCategory theItemCategory);
	
	public void delete(int theId);
	
}
