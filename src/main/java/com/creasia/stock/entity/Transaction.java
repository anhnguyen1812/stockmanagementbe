package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="sm_transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
		
	@Column(name = "type")
	private int type;
	
	@Column(name = "note_code")
	private String noteCode;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.ALL})
	@JoinColumn(name="project_id")
	private Project project;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="approved_by_id")
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="posted_by_id")
	private Employee postedBy;
	
	public Employee getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(Employee postedBy) {
		this.postedBy = postedBy;
	}

	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="stock_id")
	private Stock stock;
	
	@Column(name = "document_group")
	private int documentGroup;
	
	@Column(name = "created_at")
	private String createdAt;

	@Column(name = "posted_date")
	private String postedDate;
	
	@Column(name = "document_date")
	private String documentDate;
	
	@OneToMany(fetch = FetchType.EAGER,
			cascade= {CascadeType.ALL})
	@JoinColumn(name="transaction_id")
	private List<TransactionLine> transactionLines;

	@Column(name = "total_delivered_qty")
	private int totalQuantity;

	@Column(name = "total_posted_qty")
	private int totalPostedQuantity;
	
	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public List<TransactionLine> getTransactionLines() {
		return transactionLines;
	}

	public void setTransactionLines(List<TransactionLine> transactionLines) {
		this.transactionLines = transactionLines;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getNoteCode() {
		return noteCode;
	}

	public void setNoteCode(String noteCode) {
		this.noteCode = noteCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public int getTotalPostedQuantity() {
		return totalPostedQuantity;
	}

	public void setTotalPostedQuantity(int totalPostedQuantity) {
		this.totalPostedQuantity = totalPostedQuantity;
	}

	public int getDocumentGroup() {
		return documentGroup;
	}

	public void setDocumentGroup(int documentGroup) {
		this.documentGroup = documentGroup;
	}

	public Transaction() {}

	public Transaction(int type, String noteCode, String name, Project project, Employee employee, Stock stock,
			int documentGroup, String createdAt, String documentDate, String postedDate) {
		super();
		this.type = type;
		this.noteCode = noteCode;
		this.name = name;
		this.project = project;
		this.employee = employee;
		this.stock = stock;
		this.documentGroup = documentGroup;
		this.createdAt = createdAt;
		this.postedDate = postedDate;
		this.documentDate = documentDate;
	}
	
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", type=" + type + ", noteCode=" + noteCode + ", name=" + name + ", project="
				+ project + ", employee=" + employee + ", stock=" + stock + ", documentGroup=" + documentGroup
				+ ", createdAt=" + createdAt + ", postedDate=" + postedDate + ", documentDate=" + documentDate
				+ ", transactionLines=" + transactionLines + ", totalQuantity=" + totalQuantity
				+ ", totalPostedQuantity=" + totalPostedQuantity + "]";
	}
}
