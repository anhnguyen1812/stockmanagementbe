package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="sm_return_shipment")
public class ReturnShipment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="shipment_note_id", nullable=false)
	private ShipmentNote shipmentNote;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="return_by_id")
	private Employee returnBy;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "document_at")
	private String documentAt;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="return_shipment_id")
	private List<ReturnShipmentLine> returnShipmentLines;

	@Column(name = "total_delivered_qty")
	private int totalDelivered;
	
	@Column(name = "total_posted_qty")
	private int totalPosted;
	
	@Column(name = "posted_date")
	private String postedDate;
	
	@Column(name = "note")
	private String note;
	
	public ReturnShipment() {
	}

	public ReturnShipment(ShipmentNote shipmentNote, String code, Employee employee, String createdAt,
			 String documentAt, List<ReturnShipmentLine> returnShipmentLines) {
		super();
		this.shipmentNote = shipmentNote;
		this.code = code;
		this.returnBy = employee;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.returnShipmentLines = returnShipmentLines;
	}

	public ReturnShipment(ShipmentNote shipmentNote, String code, Employee employee, String createdAt, String documentAt,
			int totalDelivered, int totalPosted, String postedDate, String note) {
		super();
		this.shipmentNote = shipmentNote;
		this.code = code;
		this.returnBy = employee;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.totalDelivered = totalDelivered;
		this.totalPosted = totalPosted;
		this.postedDate = postedDate;
		this.note = note;
	}

	public int getTotalDelivered() {
		return totalDelivered;
	}

	public void setTotalDelivered(int totalDelivered) {
		this.totalDelivered = totalDelivered;
	}

	public int getTotalPosted() {
		return totalPosted;
	}

	public void setTotalPosted(int totalPosted) {
		this.totalPosted = totalPosted;
	}

	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ShipmentNote getShipmentNote() {
		return shipmentNote;
	}

	public void setShipmentNote(ShipmentNote shipmentNote) {
		this.shipmentNote = shipmentNote;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Employee getReturnBy() {
		return returnBy;
	}

	public void setReturnBy(Employee employee) {
		this.returnBy = employee;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}

	public List<ReturnShipmentLine> getReturnShipmentLines() {
		return returnShipmentLines;
	}

	public void setReturnShipmentLines(List<ReturnShipmentLine> returnShipmentLines) {
		this.returnShipmentLines = returnShipmentLines;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	@Override
	public String toString() {
		return "ReturnShipment [id=" + id + ", shipmentNote=" + shipmentNote + ", code=" + code + ", returnBy=" + returnBy
				+ ", createdAt=" + createdAt + ", documentAt=" + documentAt + ", returnShipmentLines="
				+ returnShipmentLines + ", totalDelivered=" + totalDelivered + ", totalPosted=" + totalPosted
				+ ", postedDate=" + postedDate + "]";
	}
}

