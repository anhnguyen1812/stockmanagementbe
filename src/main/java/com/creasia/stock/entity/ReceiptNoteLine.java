package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_receipt_note_line")
public class ReceiptNoteLine {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
//	@ManyToOne
//    @JoinColumn(name="receipt_note_id", nullable=false)
//	private ReceiptNote receiptNote;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="item_id")
	private Item item;
	
	@Column(name = "quantity")
	private int quantity;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="UoM_id")
	private UoM uoM;
	
	@Column(name = "returned")
	private boolean isReturned;
	
	@Column(name = "item_ext_no")
	private String itemExtNo;
	
	@Column(name = "exp_date")
	private String expDate;
	
	@Column(name = "posted_quantity")
	private int postedQuantity;
	
	@Column(name = "inventory_qty")
	private int inventoryQty;

	public int getPostedQuantity() {
		return postedQuantity;
	}

	public void setPostedQuantity(int postedQuantity) {
		this.postedQuantity = postedQuantity;
	}

	public String getItemExtNo() {
		return itemExtNo;
	}

	public void setItemExtNo(String itemExtNo) {
		this.itemExtNo = itemExtNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public boolean isReturned() {
		return isReturned;
	}

	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}
	
	public ReceiptNoteLine(int id, Item item, int quantity, UoM uoM, boolean isReturned, String itemExtNo,
			String expDate, int postedQuantity) {
		super();
		this.id = id;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.isReturned = isReturned;
		this.itemExtNo = itemExtNo;
		this.expDate = expDate;
		this.postedQuantity = postedQuantity;
	}

	public ReceiptNoteLine(int id, Item item, int quantity, UoM uoM, boolean isReturned) {
		super();
		this.id = id;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.isReturned = isReturned;
	}

	public ReceiptNoteLine(Item item, int quantity, UoM uoM, boolean isReturned, String itemExtNo, String expDate,
			int postedQuantity, int inventoryQty) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.isReturned = isReturned;
		this.itemExtNo = itemExtNo;
		this.expDate = expDate;
		this.postedQuantity = postedQuantity;
		this.inventoryQty = inventoryQty;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public ReceiptNoteLine() {}
	
	public ReceiptNoteLine(int quantity) {
		super();
		this.quantity = quantity;
	}

	public int getInventoryQty() {
		return inventoryQty;
	}

	public void setInventoryQty(int inventoryQty) {
		this.inventoryQty = inventoryQty;
	}

	@Override
	public String toString() {
		return "ReceiptNoteLine [id=" + id + ", item=" + item + ", quantity=" + quantity + ", uoM=" + uoM + "]";
	}
}
