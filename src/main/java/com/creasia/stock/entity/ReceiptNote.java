package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="sm_receipt_note")
public class ReceiptNote {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="vendor_id")
	private Vendor vendor;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="project_id")
	private Project project;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="stock_id")
	private Stock stock;
	
	@Column(name = "created_at")
	private String createdAt;

	@Column(name = "document_at")
	private String documentAt;
	
	@Column(name = "posted_date")
	private String postedDate;
	
	@Column(name = "returned")
	private boolean isReturned;
	
	public boolean isReturned() {
		return isReturned;
	}

	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}
	
	@OneToMany(cascade= {CascadeType.ALL})
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="receipt_note_id")
	private List<ReceiptNoteLine> receiptNoteLines;
	
	@Column(name = "total_delivered_qty")
	private int totalQuantity;
	
	@Column(name = "total_posted_qty")
	private int totalPostedQuantity;
		
	public int getTotalPostedQuantity() {
		return totalPostedQuantity;
	}

	public void setTotalPostedQuantity(int totalPostedQuantity) {
		this.totalPostedQuantity = totalPostedQuantity;
	}

	
	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public ReceiptNote(int id, String code, Vendor vendor, Project project, Employee employee, Stock stock,
			String createdAt, String documentAt, String postedDate, boolean isReturned,
			List<ReceiptNoteLine> receiptNoteLines, int totalQuantity, int totalPostedQuantity) {
		super();
		this.id = id;
		this.code = code;
		this.vendor = vendor;
		this.project = project;
		this.employee = employee;
		this.stock = stock;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.postedDate = postedDate;
		this.isReturned = isReturned;
		this.receiptNoteLines = receiptNoteLines;
		this.totalQuantity = totalQuantity;
		this.totalPostedQuantity = totalPostedQuantity;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public List<ReceiptNoteLine> getReceiptNoteLines() {
		return receiptNoteLines;
	}

	public void setReceiptNoteLines(List<ReceiptNoteLine> receiptNoteLines) {
		this.receiptNoteLines = receiptNoteLines;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}

	public ReceiptNote() {}
	
	public ReceiptNote(String code, String createdAt, String documentAt) {
		super();
		this.code = code;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
	}

	@Override
	public String toString() {
		return "ReceiptNote [id=" + id + ", code=" + code + ", createdAt=" + createdAt + ", documentAt=" + documentAt
				+ ", totalQuantity=" + totalQuantity + "]";
	}
}


