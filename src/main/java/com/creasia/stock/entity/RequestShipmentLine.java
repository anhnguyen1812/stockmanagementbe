package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sm_request_shipment_line")
public class RequestShipmentLine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "item_id")
	private Item item;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "UoM_id")
	private UoM uoM;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "out_std_qty")
	private int outStdQty;

	@Column(name = "shipted_qty")
	private int shiptedQty;

	@Column(name = "item_ext_no")
	private String itemExtNo;
	
	public String getItemExtNo() {
		return itemExtNo;
	}

	public void setItemExtNo(String itemExtNo) {
		this.itemExtNo = itemExtNo;
	}

	
	public RequestShipmentLine(Item item, UoM uoM, int quantity, int outStdQty, int shiptedQty,String itemExtNo) {
		super();
		this.item = item;
		this.uoM = uoM;
		this.quantity = quantity;
		this.outStdQty = outStdQty;
		this.shiptedQty = shiptedQty;
		this.itemExtNo = itemExtNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getOutStdQty() {
		return outStdQty;
	}

	public void setOutStdQty(int outStdQty) {
		this.outStdQty = outStdQty;
	}

	public int getShiptedQty() {
		return shiptedQty;
	}

	public void setShiptedQty(int shiptedQty) {
		this.shiptedQty = shiptedQty;
	}

	public RequestShipmentLine() {
	}

	public RequestShipmentLine(int id, Item item, UoM uoM, int quantity, int outStdQty, int shiptQty) {
		super();
		this.id = id;
		this.item = item;
		this.uoM = uoM;
		this.quantity = quantity;
		this.outStdQty = outStdQty;
		this.shiptedQty = shiptQty;
	}

	@Override
	public String toString() {
		return "RequestShipmentLine [id=" + id + ", item=" + item + ", uoM=" + uoM + ", quantity=" + quantity
				+ ", outStdQty=" + outStdQty + ", shiptedQty=" + shiptedQty + "]";
	}

	
//	@Override
//	public String toString() {
//		return "RequestShipmentLine [id=" + id + ", quantity=" + quantity + "]";
//	}
}
