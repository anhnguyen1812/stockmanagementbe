package com.creasia.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sm_permission")
public class Permission {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "is_blocked")
	private boolean isBlocked;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Permission() {}
	
	public Permission(String name, String createdAt, boolean isBlocked) {
		super();
		this.name = name;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Permission [id=" + id + ", name=" + name + ", createdAt=" + createdAt + ", isBlocked=" + isBlocked
				+ "]";
	}
}
