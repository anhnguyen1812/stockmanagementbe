package com.creasia.stock.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.StaleObjectStateException;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Entity
@Table(name="sm_shipment_note")
public class ShipmentNote {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="approved_shipment_id")
	private ApprovedShipment approvedShipment;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="approve_by_id")
	private Employee approveBy;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="posted_by_id")
	private Employee postedBy;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "document_at")
	private String documentAt;
	
	@Column(name = "posted_date")
	private String postedDate;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="shipment_note_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ShipmentNoteLine> shipmentNoteLines;

	@Column(name = "returned")
	private boolean isReturned;
	
	@Column(name = "total_delivered_qty")
	private int totalQuantity;
	
	@Column(name = "total_posted_qty")
	private int totalPostedQuantity;
		
	@Column(name = "recipient_name")
	private String recipientName;
	
	@Column(name = "shipping_unit")
	private String shippingUnit;
	
	@Column(name = "means_of_conveyance")
	private String 	meansOfConveyance;
	
	@Column(name = "license_plates")
	private String 	licensePlates;
	
	@Column(name = "phone")
	private String 	phone;
	
	public int getTotalPostedQuantity() {
		return totalPostedQuantity;
	}
	
	public void setTotalPostedQuantity(int totalPostedQuantity) {
		this.totalPostedQuantity = totalPostedQuantity;
	}
	
	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public boolean isReturned() {
		return isReturned;
	}

	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}
	
	public Employee getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(Employee postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getShippingUnit() {
		return shippingUnit;
	}

	public void setShippingUnit(String shippingUnit) {
		this.shippingUnit = shippingUnit;
	}

	public String getMeansOfConveyance() {
		return meansOfConveyance;
	}

	public void setMeansOfConveyance(String meansOfConveyance) {
		this.meansOfConveyance = meansOfConveyance;
	}

	public String getLicensePlates() {
		return licensePlates;
	}

	public void setLicensePlates(String licensePlates) {
		this.licensePlates = licensePlates;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ShipmentNote(int id, ApprovedShipment approvedShipment, String code, Employee approveBy, String createdAt,
			String documentAt, String postedDate, List<ShipmentNoteLine> shipmentNoteLines, boolean isReturned,
			int totalQuantity, int totalPostedQuantity) {
		super();
		this.id = id;
		this.approvedShipment = approvedShipment;
		this.code = code;
		this.approveBy = approveBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.postedDate = postedDate;
		this.shipmentNoteLines = shipmentNoteLines;
		this.isReturned = isReturned;
		this.totalQuantity = totalQuantity;
		this.totalPostedQuantity = totalPostedQuantity;
	}

	public ShipmentNote(int id, ApprovedShipment approvedShipment, String code, Employee approveBy, String createdAt,
			String documentAt, List<ShipmentNoteLine> shipmentNoteLines, int totalQuantity, boolean isReturned) {
		super();
		this.id = id;
		this.approvedShipment = approvedShipment;
		this.code = code;
		this.approveBy = approveBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.shipmentNoteLines = shipmentNoteLines;
		this.totalQuantity = totalQuantity;
		this.isReturned = isReturned;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public ShipmentNote() {
	}

	public ShipmentNote(ApprovedShipment approvedShipment, String code, Employee approveBy, String createdAt,
			 String documentAt, List<ShipmentNoteLine> shipmentNoteLines) {
		super();
		this.approvedShipment = approvedShipment;
		this.code = code;
		this.approveBy = approveBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.shipmentNoteLines = shipmentNoteLines;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ApprovedShipment getApprovedShipment() {
		return approvedShipment;
	}

	public void setApprovedShipment(ApprovedShipment approvedShipment) {
		this.approvedShipment = approvedShipment;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Employee getApproveBy() {
		return approveBy;
	}

	public void setApproveBy(Employee approveBy) {
		this.approveBy = approveBy;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}

	public List<ShipmentNoteLine> getShipmentNoteLines() {
		return shipmentNoteLines;
	}

	public void setShipmentNoteLines(List<ShipmentNoteLine> shipmentNoteLines) {
		this.shipmentNoteLines = shipmentNoteLines;
	}

	@Override
	public String toString() {
		return "ShipmentNote [id=" + id + ", approvedShipment=" + approvedShipment + ", code=" + code + ", approveBy="
				+ approveBy + ", createdAt=" + createdAt + ", documentAt=" + documentAt + ", shipmentNoteLines=" + shipmentNoteLines + ", totalQuantity=" + totalQuantity + "]";
	}
	
	
//	@PutMapping("/shipmentNotes")
//	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
//	public Entities updateShipmentNote(@RequestBody ShipmentNote theShipmentNote) {
//		if (theShipmentNote.getId() == 0) {
//			return new Entities(1, theShipmentNote, "body is null");
//		}
//		try {
//			ShipmentNote shipment = shipmentNoteService.findById(theShipmentNote.getId());
//			
//			if (theShipmentNote.getPostedDate() == null) {
//				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
//				LocalDateTime now = LocalDateTime.now();
//				shipment.setPostedDate(dtf.format(now));
//			} else {
//				shipment.setPostedDate(theShipmentNote.getPostedDate());
//			}
//			shipment.setPostedBy(theShipmentNote.getPostedBy());
//			shipment.setTotalPostedQuantity(shipment.getTotalQuantity());
//			
//			System.out.println("the shipment Note: " + theShipmentNote.getPostedDate());
//			System.out.println(" shipment : " + shipment.getPostedDate());
//			
//			System.out.println("shipment code " + shipment.getCode());
//			Transaction transaction = transactionService.findByNoteCode(shipment.getCode());
//			transaction.setPostedDate(shipment.getPostedDate());
//			transaction.setDocumentGroup(-1);
//			transaction.setPostedBy(theShipmentNote.getPostedBy());
//			transaction.setTotalPostedQuantity(transaction.getTotalQuantity());
//
//			int postedQuantity = 0;
//			for (int i = 0; i < shipment.getShipmentNoteLines().size(); i++) {
//				postedQuantity = shipment.getShipmentNoteLines().get(i).getQuantity();
//				shipment.getShipmentNoteLines().get(i).setPostedQuantity(postedQuantity);
//			}
//			for (int i = 0; i < transaction.getTransactionLines().size(); i++) {
//				postedQuantity = transaction.getTransactionLines().get(i).getQuantity();
//				transaction.getTransactionLines().get(i).setPostedQuantity(postedQuantity);
//				transaction.getTransactionLines().get(i).setExpDate(null);
//			}
//			try {
//				shipmentNoteService.update(shipment);
//				transactionService.update(transaction);
//				System.out.println(
//						"shipment posted := " + shipment.getTotalPostedQuantity() + " " + shipment.getPostedDate());
//				System.out.println("transaction := " + transaction);
//				System.out.println("shipment := " + shipment);
//			} catch (OptimisticLockingFailureException e) {
//				return new Entities(1, theShipmentNote, "Fail to server");
//			} catch (StaleObjectStateException e) {
//				return new Entities(1, theShipmentNote, "Fail to server");
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//			return new Entities(1, theShipmentNote, "Fail to server");
//		}
//		// System.out.println("update success");
//		Entities entities = new Entities(0, theShipmentNote, HttpStatus.OK.getReasonPhrase());
//		return entities;
//	}
	
}



