package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="sm_item")
public class Item {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;

	@Column(name = "item_ext_no")
	private String itemExtNo;
	
	@Column(name = "name_search")
	private String nameSearch;
	
	
//	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
//			CascadeType.DETACH, CascadeType.REFRESH})
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="UoM_id")
	private UoM uoM;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "unit_cost")
	private int unit_cost;
	
	@Column(name = "created_at")
	private String createAt;
	
	@Column(name = "is_blocked")
	private boolean isBlocked;

	
	@ManyToOne( fetch = FetchType.EAGER,
			cascade= {CascadeType.ALL})
	@JoinColumn(name = "category_id")
	private ItemCategory itemCategory;
	
	public Item() {
		
	}

	public Item(String code, String name, String itemExtNo, String nameSearch, String type,
			int unit_cost, String createAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.itemExtNo = itemExtNo;
		this.nameSearch = nameSearch;
		this.type = type;
		this.unit_cost = unit_cost;
		this.createAt = createAt;
		this.isBlocked = isBlocked;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getItemExtNo() {
		return itemExtNo;
	}

	public void setItemExtNo(String itemExtNo) {
		this.itemExtNo = itemExtNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameSearch() {
		return nameSearch;
	}

	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getUnit_cost() {
		return unit_cost;
	}

	public void setUnit_cost(int unit_cost) {
		this.unit_cost = unit_cost;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public ItemCategory getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(ItemCategory itemCategory) {
		this.itemCategory = itemCategory;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", code=" + code + ", name=" + name
				+ ", nameSearch=" + nameSearch + ", type=" + type
				+ ", unit_cost=" + unit_cost + ", createAt=" + createAt + ", isBlocked=" + isBlocked + "]";
	}

}





