package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_permission_assignment")
public class PermissionAssignment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="stock_id")
	private Stock stock;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.ALL})
	@JoinColumn(name="user_id")
	private User users;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="role_id")
	private Role roles;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="create_by_id")
	private Employee createBy;
	
	@Column(name = "created_at")
	private String 	createdAt;

	@Column(name = "starting_date")
	private String startingDate;
	
	@Column(name = "ending_date")
	private String endingDate;
	
	@Column(name = "description")
	private String description;
	
	@Column(name="is_blocked")
	private boolean isBlock;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

	public Role getRoles() {
		return roles;
	}

	public void setRoles(Role roles) {
		this.roles = roles;
	}

	public Employee getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Employee createBy) {
		this.createBy = createBy;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public String getEndingDate() {
		return endingDate;
	}

	public void setEndingDate(String endingDate) {
		this.endingDate = endingDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setBlock(boolean isBlock) {
		this.isBlock = isBlock;
	}

	public PermissionAssignment( Stock stocks, User users) {
		super();
		this.stock = stocks;
		this.users = users;
	}
	
	public PermissionAssignment(int id, Stock stocks, User users, Role roles, Employee createBy, String createdAt,
			String startingDate, String endingDate, String description) {
		super();
		this.id = id;
		this.stock = stocks;
		this.users = users;
		this.roles = roles;
		this.createBy = createBy;
		this.createdAt = createdAt;
		this.startingDate = startingDate;
		this.endingDate = endingDate;
		this.description = description;
	}

	public PermissionAssignment(int id, Stock stock, User users, Role roles, Employee createBy, String createdAt,
			String startingDate, String endingDate, String description, boolean isBlock) {
		super();
		this.id = id;
		this.stock = stock;
		this.users = users;
		this.roles = roles;
		this.createBy = createBy;
		this.createdAt = createdAt;
		this.startingDate = startingDate;
		this.endingDate = endingDate;
		this.description = description;
		this.isBlock = isBlock;
	}

	public PermissionAssignment() {
		super();
	}

	@Override
	public String toString() {
		return "PermissionAssignment [id=" + id + ", stock=" + stock + ", users=" + users + ", roles=" + roles
				+ ", createBy=" + createBy + ", createdAt=" + createdAt + ", startingDate=" + startingDate
				+ ", endingDate=" + endingDate + ", description=" + description + ", isBlock=" + isBlock + "]";
	}

}
