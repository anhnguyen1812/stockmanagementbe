package com.creasia.stock.entity;

public enum ERole {
	ROLE_USER,
    ROLE_POST,
    ROLE_ADMIN,
    ROLE_APPROVE,
    ROLE_VERIFY
}
