package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_approved_shipment_line")
public class ApprovedShipmentLine {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "item_id")
	private Item item;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "UoM_id")
	private UoM uoM;

	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "inventory_qty")
	private int inventoryQty;

	@Column(name = "item_ext_no")
	private String itemExtNo;

	@Column(name = "exp_date")
	private String expDate;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getInventoryQty() {
		return inventoryQty;
	}

	public void setInventoryQty(int inventoryQty) {
		this.inventoryQty = inventoryQty;
	}

	public String getItemExtNo() {
		return itemExtNo;
	}

	public void setItemExtNo(String itemExtNo) {
		this.itemExtNo = itemExtNo;
	}
	
	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public ApprovedShipmentLine() {}
	
	public ApprovedShipmentLine(Item item, UoM uoM, int quantity, int inventoryQty, String itemExtNo) {
		super();
		this.item = item;
		this.uoM = uoM;
		this.quantity = quantity;
		this.inventoryQty = inventoryQty;
		this.itemExtNo = itemExtNo;
	}

	public ApprovedShipmentLine(Item item, UoM uoM, int quantity, int inventoryQty, String itemExtNo, String expDate) {
		super();
		this.item = item;
		this.uoM = uoM;
		this.quantity = quantity;
		this.inventoryQty = inventoryQty;
		this.itemExtNo = itemExtNo;
		this.expDate = expDate;
	}
	
	@Override
	public String toString() {
		return "ApprovedShipmentLine [id=" + id + ", item=" + item + ", uoM=" + uoM + ", quantity=" + quantity
				+ ", itemExtNo=" + itemExtNo + "]";
	}
}
