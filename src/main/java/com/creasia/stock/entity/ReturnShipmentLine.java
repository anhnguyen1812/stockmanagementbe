package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_return_shipment_line")
public class ReturnShipmentLine {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="item_id")
	private Item item;
	
	@Column(name = "quantity")
	private int quantity;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="UoM_id")
	private UoM uoM;
	
	@Column(name = "item_ext_no")
	private String itemExtNo;
	
	@Column(name = "exp_date")
	private String expDate;
	
	@Column(name = "posted_quantity")
	private int postedQuantity;

	public int getPostedQuantity() {
		return postedQuantity;
	}

	public void setPostedQuantity(int postedQuantity) {
		this.postedQuantity = postedQuantity;
	}

	public String getItemExtNo() {
		return itemExtNo;
	}

	public void setItemExtNo(String itemExtNo) {
		this.itemExtNo = itemExtNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
	public ReturnShipmentLine(Item item, int quantity, UoM uoM, String itemExtNo, String expDate, int postedQuantity) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.itemExtNo = itemExtNo;
		this.expDate = expDate;
		this.postedQuantity = postedQuantity;
	}

	public ReturnShipmentLine(int id, Item item, int quantity, UoM uoM) {
		super();
		this.id = id;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
	}

	public ReturnShipmentLine() {
		super();
	}

	public ReturnShipmentLine(Item item, int quantity, UoM uoM) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}

	@Override
	public String toString() {
		return "ReturnShipmentLine [id=" + id + ", item=" + item + ", quantity=" + quantity + ", uoM=" + uoM + "]";
	}
}
