package com.creasia.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sm_province")
public class Province {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;

	@Column(name = "name_search")
	private String nameSearch;
	
	public Province() {
		
	}
	
	public Province(String name, String nameSearch) {
		super();
		this.name = name;
		this.nameSearch = nameSearch;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getNameSearch() {
		return nameSearch;
	}

	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	@Override
	public String toString() {
		return "Province [id=" + id + ", name=" + name + ", nameSearch=" + nameSearch + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getNameSearch()=" + getNameSearch() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
}
