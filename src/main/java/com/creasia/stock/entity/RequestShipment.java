package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="sm_request_shipment")
public class RequestShipment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="project_id")
	private Project project;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="create_by_id")
	private Employee createBy;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="stock_id")
	private Stock stock;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="verified_by_id")
	private Employee verifiedBy;
	
	@Column(name = "verified_at")
	private String verifiedAt;

	@Column(name = "document_at")
	private String documentAt;
	
	@Column(name = "approved")
	private int approved;
	
	@Column(name = "is_delete")
	private boolean isDelete;
		
	@OneToMany(cascade= {CascadeType.ALL})
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="request_shipment_id")
	private List<RequestShipmentLine> requestShipmentLines;
		
	@Column(name = "total_quantity")
	private int totalQuantity;

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Employee getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Employee employee) {
		this.createBy = employee;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Employee getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(Employee verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedAt() {
		return verifiedAt;
	}

	public void setVerifiedAt(String verifiedAt) {
		this.verifiedAt = verifiedAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}
	
	public List<RequestShipmentLine> getRequestShipmentLines() {
		return requestShipmentLines;
	}

	public void setRequestShipmentLines(List<RequestShipmentLine> requestShipmentLines) {
		this.requestShipmentLines = requestShipmentLines;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public RequestShipment() {}
	
	public RequestShipment(String code, String createdAt, String documentAt, int approved) {
		super();
		this.code = code;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.approved = approved;
	}
	
	public RequestShipment(int id, String code, Project project, Employee createBy, Stock stock, String createdAt,
			Employee verifiedBy, String verifiedAt, String documentAt, int approved, boolean isDelete,
			List<RequestShipmentLine> requestShipmentLines, int totalQuantity) {
		super();
		this.id = id;
		this.code = code;
		this.project = project;
		this.createBy = createBy;
		this.stock = stock;
		this.createdAt = createdAt;
		this.verifiedBy = verifiedBy;
		this.verifiedAt = verifiedAt;
		this.documentAt = documentAt;
		this.approved = approved;
		this.isDelete = isDelete;
		this.requestShipmentLines = requestShipmentLines;
		this.totalQuantity = totalQuantity;
	}

	@Override
	public String toString() {
		return "RequestShipment [id=" + id + ", code=" + code + ", project=" + project + ", createBy=" + createBy
				+ ", stock=" + stock + ", createdAt=" + createdAt + ", verifiedBy=" + verifiedBy + ", verifiedAt="
				+ verifiedAt + ", documentAt=" + documentAt + ", approved=" + approved + ", isDelete=" + isDelete
				+ ", requestShipmentLines=" + requestShipmentLines + ", totalQuantity=" + totalQuantity + "]";
	}
}
