package com.creasia.stock.entity;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(	name = "ms_users", 
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username"),
			@UniqueConstraint(columnNames = "email") 
		})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 20)
	@JoinColumn(name="username")
	private String username;

	@NotBlank
	@Size(max = 50)
	@Email
	@JoinColumn(name="email")
	private String email;

	@NotBlank
	@Size(max = 120)
	@JoinColumn(name="password")
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "ms_user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@Column(name="blocked")
	private boolean isBlock;
	
	@Column(name="is_first_login")
	private boolean isFirstLogin;
	
	public User() {
	}

	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public User(String username, String email, String password, Employee employee, boolean isBlock, boolean isFirstLogin) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.employee = employee;
		this.isBlock = isBlock;
		this.isFirstLogin = isFirstLogin;
	}
	
	public User(Long id, String username, String email, String password, Set<Role> roles, Employee employee, boolean isBlock, boolean isFirstLogin) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.roles = roles;
		this.employee = employee;
		this.isBlock = isBlock;
		this.isFirstLogin = isFirstLogin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setBlock(boolean isBlock) {
		this.isBlock = isBlock;
	}
	
	public boolean isFirstLogin() {
		return isFirstLogin;
	}

	public void setFirstLogin(boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password + ", roles="
				+ roles + ", employee=" + employee + ", isBlock=" + isBlock + ", isFirstLogin=" + isFirstLogin + "]";
	}
}
