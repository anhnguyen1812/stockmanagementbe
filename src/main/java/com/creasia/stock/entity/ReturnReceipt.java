package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="sm_return_receipt")
public class ReturnReceipt {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="receipt_note_id", nullable=false)
	private ReceiptNote receiptNote;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="return_by_id")
	private Employee returnBy;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "document_at")
	private String documentAt;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="approved_by_id")
	private Employee approvedBy;
	
	private String approvedAt;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="return_receipt_id")
	private List<ReturnReceiptLine> returnReceiptLines;

	@Column(name = "total_delivered_qty")
	private int totalDelivered;
	
	@Column(name = "total_posted_qty")
	private int totalPosted;
	
	@Column(name = "posted_date")
	private String postedDate;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "returned")
	private boolean isReturned;
	
	public ReturnReceipt() {
	}

	public ReturnReceipt(ReceiptNote receiptNote, String code, Employee employee, String createdAt,
			 String documentAt, List<ReturnReceiptLine> returnReceiptLines) {
		super();
		this.receiptNote = receiptNote;
		this.code = code;
		this.returnBy = employee;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.returnReceiptLines = returnReceiptLines;
	}

	public ReturnReceipt(ReceiptNote receiptNote, String code, Employee employee, String createdAt, String documentAt,
			int totalDelivered, int totalPosted, String postedDate, String note) {
		super();
		this.receiptNote = receiptNote;
		this.code = code;
		this.returnBy = employee;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.totalDelivered = totalDelivered;
		this.totalPosted = totalPosted;
		this.postedDate = postedDate;
		this.note = note;
	}

	public ReturnReceipt(ReceiptNote receiptNote, String code, Employee returnBy, String createdAt, String documentAt,
			Employee approvedBy, String approvedAt, List<ReturnReceiptLine> returnReceiptLines, int totalDelivered,
			int totalPosted, String postedDate, String note) {
		super();
		this.receiptNote = receiptNote;
		this.code = code;
		this.returnBy = returnBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.approvedBy = approvedBy;
		this.approvedAt = approvedAt;
		this.returnReceiptLines = returnReceiptLines;
		this.totalDelivered = totalDelivered;
		this.totalPosted = totalPosted;
		this.postedDate = postedDate;
		this.note = note;
	}

	public Employee getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Employee approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getApprovedAt() {
		return approvedAt;
	}

	public void setApprovedAt(String approvedAt) {
		this.approvedAt = approvedAt;
	}

	public int getTotalDelivered() {
		return totalDelivered;
	}

	public void setTotalDelivered(int totalDelivered) {
		this.totalDelivered = totalDelivered;
	}

	public int getTotalPosted() {
		return totalPosted;
	}

	public void setTotalPosted(int totalPosted) {
		this.totalPosted = totalPosted;
	}

	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public ReceiptNote getReceiptNote() {
		return receiptNote;
	}

	public void setReceiptNote(ReceiptNote receiptNote) {
		this.receiptNote = receiptNote;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Employee getReturnBy() {
		return returnBy;
	}

	public void setReturnBy(Employee employee) {
		this.returnBy = employee;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}

	public List<ReturnReceiptLine> getReturnReceiptLines() {
		return returnReceiptLines;
	}

	public void setReturnReceiptLines(List<ReturnReceiptLine> returnReceiptLines) {
		this.returnReceiptLines = returnReceiptLines;
	}

	public boolean isReturned() {
		return isReturned;
	}

	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}

	@Override
	public String toString() {
		return "ReturnReceipt [id=" + id + ", receiptNote=" + receiptNote + ", code=" + code + ", returnBy=" + returnBy
				+ ", createdAt=" + createdAt + ", documentAt=" + documentAt + ", returnReceiptLines="
				+ returnReceiptLines + ", totalDelivered=" + totalDelivered + ", totalPosted=" + totalPosted
				+ ", postedDate=" + postedDate + "]";
	}
}

