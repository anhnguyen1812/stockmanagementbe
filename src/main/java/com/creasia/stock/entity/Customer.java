package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_customer")
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "name_search")
	private String nameSearch;
	
	@Column(name = "name_contact")
	private String nameContact;
	
	@Column(name = "phone_contact")
	private String 	phoneContact;
	
	@Column(name = "email_contact")
	private String emailContact;
	
	@Column(name = "phone")
	private String 	phone;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="district_id")
	private District district;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "type")
	private String 	type;
	
	@Column(name = "groups")
	private String groups;
	
	@Column(name = "GST_code")
	private String 	gSTCode;
	
	@Column(name = "GST_posting_group")
	private Boolean GSTPostingGroup;
	
	@Column(name = "payment_terms_code")
	private int paymentTermsCode;
	
	@Column(name = "created_at")
	private String 	createdAt;
	
	@Column(name = "is_blocked")
	private boolean isBlocked;
	
	public Customer() {
		
	}

	public Customer(String code, String name, String nameSearch, String nameContact, String phoneContact,
			String emailContact, String phone, String address, String type, String groups, String gSTCode,
			Boolean gSTPostingGroup, int paymentTermsCode, String createdAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.nameSearch = nameSearch;
		this.nameContact = nameContact;
		this.phoneContact = phoneContact;
		this.emailContact = emailContact;
		this.phone = phone;
		this.address = address;
		this.type = type;
		this.groups = groups;
		this.gSTCode = gSTCode;
		this.GSTPostingGroup = gSTPostingGroup;
		this.paymentTermsCode = paymentTermsCode;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}
	
	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameSearch() {
		return nameSearch;
	}

	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	public String getNameContact() {
		return nameContact;
	}

	public void setNameContact(String nameContact) {
		this.nameContact = nameContact;
	}

	public String getPhoneContact() {
		return phoneContact;
	}

	public void setPhoneContact(String phoneContact) {
		this.phoneContact = phoneContact;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}


	public String getgSTCode() {
		return gSTCode;
	}

	public void setgSTCode(String gSTCode) {
		this.gSTCode = gSTCode;
	}

	public Boolean getGSTPostingGroup() {
		return GSTPostingGroup;
	}

	public void setGSTPostingGroup(Boolean gSTPostingGroup) {
		GSTPostingGroup = gSTPostingGroup;
	}

	public int getPaymentTermsCode() {
		return paymentTermsCode;
	}

	public void setPaymentTermsCode(int paymentTermsCode) {
		this.paymentTermsCode = paymentTermsCode;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", code=" + code + ", name=" + name + ", nameSearch=" + nameSearch
				+ ", nameContact=" + nameContact + ", phoneContact=" + phoneContact + ", emailContact=" + emailContact
				+ ", phone=" + phone + ", address=" + address + ", type=" + type + ", groups=" + groups + ", GSTCode="
				+ gSTCode + ", GSTPostingGroup=" + GSTPostingGroup + ", paymentTermsCode=" + paymentTermsCode
				+ ", createdAt=" + createdAt + ", isBlocked=" + isBlocked + "]";
	}
}
