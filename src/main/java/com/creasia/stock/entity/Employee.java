package com.creasia.stock.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="ms_employee")
public class Employee {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="department_id")
	private Department department;
	
	@Column(name = "position")
	private String position;
	
	@Column(name = "birthday")
	private String birthday;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "phone")
	private String phone;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="district_id")
	private District district;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "created_at")
	private String 	createdAt;
	
	@Column(name = "is_blocked")
	private boolean isBlocked;
	
//	@OneToMany(mappedBy = "manager")
//	@LazyCollection(LazyCollectionOption.FALSE)
//	private List<Employee> employees = new ArrayList();
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "manager_id")
	private Employee manager;

//	public Employee(int id, String code, String name, Department department, String position, String birthday,
//			String email, String phone, District district, String address, String createdAt, boolean isBlocked,
//			List<Employee> employees, Employee manager) {
//		super();
//		this.id = id;
//		this.code = code;
//		this.name = name;
//		this.department = department;
//		this.position = position;
//		this.birthday = birthday;
//		this.email = email;
//		this.phone = phone;
//		this.district = district;
//		this.address = address;
//		this.createdAt = createdAt;
//		this.isBlocked = isBlocked;
//		this.employees = employees;
//		this.manager = manager;
//	}

	public Employee(int id, String code, String name, Department department, String position, String birthday,
			String email, String phone, District district, String address, String createdAt, boolean isBlocked,
			 Employee manager) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.department = department;
		this.position = position;
		this.birthday = birthday;
		this.email = email;
		this.phone = phone;
		this.district = district;
		this.address = address;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
		this.manager = manager;
	}

//	public List<Employee> getEmployees() {
//		return employees;
//	}
//
//	public void setEmployees(List<Employee> employees) {
//		this.employees = employees;
//	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public Employee() {}
	
	public Employee(int id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}

	public Employee(String code, String name, String position, String birthday, String email, String phone, String address,
			String createdAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.position = position;
		this.birthday = birthday;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", code=" + code + ", name=" + name + ", department=" + department + ", position="
				+ position + ", birthday=" + birthday + ", email=" + email + ", phone=" + phone + ", district="
				+ district + ", address=" + address + ", createdAt=" + createdAt + ", isBlocked=" + isBlocked
				+ ", manager=" + manager + "]";
	}
}
