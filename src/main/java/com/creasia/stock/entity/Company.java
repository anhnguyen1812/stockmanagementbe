package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_company")
public class Company {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "groups")
	private String groups;
	
	@Column(name = "phone")
	private String phone;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="district_id")
	private District district;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "name_contact")
	private String nameContact;
	
	@Column(name = "phone_contact")
	private String phoneContact;
	
	@Column(name = "email_contact")
	private String emailContact;
	
	@Column(name = "created_at")
	private String 	createdAt;

	@Column(name = "is_blocked")
	private boolean isBlocked;
	
	public Company() {
		
	}
	
	public Company(String code, String name, String groups, String phone, District district, String address,
			String nameContact, String phoneContact, String emailContact, String createdAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.groups = groups;
		this.phone = phone;
		this.district = district;
		this.address = address;
		this.nameContact = nameContact;
		this.phoneContact = phoneContact;
		this.emailContact = emailContact;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}


	public Company(String code, String name, String groups, String phone, String address, String nameContact, String phoneContact, String emailContact, String createdAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.groups = groups;
		this.phone = phone;
		this.address = address;
		this.nameContact = nameContact;
		this.phoneContact = phoneContact;
		this.emailContact = emailContact;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}
	
	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public int getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneContact() {
		return phoneContact;
	}

	public void setPhoneContact(String phoneContact) {
		this.phoneContact = phoneContact;
	}

	public String getNameContact() {
		return nameContact;
	}

	public void setNameContact(String nameContact) {
		this.nameContact = nameContact;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", code=" + code + ", name=" + name + ", groups=" + groups + ", phone=" + phone
				+ ", address=" + address + ", nameContact=" + nameContact + ", phoneContact=" + phoneContact
				+ ", emailContact=" + emailContact + ", createdAt=" + createdAt + ", isBlocked=" + isBlocked + "]";
	}

	
}






