package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_stock")
public class Stock {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="district_id")
	private District district;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "is_blocked")
	private boolean isBlocked;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	public Stock() {
		
	}

	public Stock(String code, String name, String address, String createdAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.address = address;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Stock [id=" + id + ", code=" + code + ", name=" + name + ", address=" + address + ", createdAt="
				+ createdAt + ", isBlocked=" + isBlocked + "]";
	}

	public Stock(int id, String code, String name, District district, String address, String createdAt,
			boolean isBlocked) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.district = district;
		this.address = address;
		this.createdAt = createdAt;
		this.isBlocked = isBlocked;
	}
}
