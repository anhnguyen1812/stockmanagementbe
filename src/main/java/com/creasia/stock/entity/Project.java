package com.creasia.stock.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sm_project")
public class Project {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="company_id")
	private Company company;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="pm_id")
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	@Column(name = "types")
	private String types;
	
	@Column(name = "duration")
	private String duration;
	
	@Column(name = "groups")
	private String groups;
	
	@Column(name = "temp_term")
	private String tempTerm;
	
	@Column(name = "start_date")
	private String startDate;
	
	@Column(name = "finish_date")
	private String finishDate;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;
	
	@Column(name="is_blocked")
	private boolean isBlocked;

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}
	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getTempTerm() {
		return tempTerm;
	}

	public void setTempTerm(String tempTerm) {
		this.tempTerm = tempTerm;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Project() {}
	
	public Project( String code, String name, String startDate, String types, String groups, String duration, String tempTerm, String finishDate, String createdAt, String updatedAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.types = types;
		this.groups = groups;
		this.duration = duration;
		this.tempTerm = tempTerm;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", code=" + code + ", name=" + name + ", company=" + company + ", employee="
				+ employee + ", customer=" + customer + ", types=" + types + ", groups=" + groups + ", tempTerm="
				+ tempTerm + ", startDate=" + startDate + ", finishDate=" + finishDate + ", createdAt=" + createdAt
				+ ", updatedAt=" + updatedAt + ", isBlocked=" + isBlocked + "]";
	}
}
