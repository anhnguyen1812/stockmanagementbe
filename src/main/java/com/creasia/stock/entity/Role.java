package com.creasia.stock.entity;


import javax.persistence.*;

@Entity
@Table(name = "ms_role")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	@JoinColumn(name="name")
	private ERole name;

	@Column(name = "description")
	private String description;
	
	public Role() {

	}
	
	public Role(ERole name) {
		super();
		this.name = name;
	}

	public Role(ERole name, String description) {
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ERole getName() {
		return name;
	}

	public void setName(ERole name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
