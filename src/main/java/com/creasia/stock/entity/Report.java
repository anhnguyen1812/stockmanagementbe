package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.Entity;

//import javax.persistence.CascadeType;
//import javax.persistence.FetchType;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;

public class Report {

	private String name;
	
	private Stock stock;

	private Item item;
	
	private long quantity;
	
	private long quantityIn;
	
	private long quantityOut;
	
	private String dayOfMonth;

	private UoM uoM;
	
	private String postedDate;
	
	private String expDate;

	private Transaction transaction;
	
	private TransactionLine transactionLine;
	
	private Project project;
	
	private List<Stock> listStock;
	
	private List<Integer> listQty;
	
	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public long getQuantityIn() {
		return quantityIn;
	}

	public String getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public void setQuantityIn(long quantityIn) {
		this.quantityIn = quantityIn;
	}

	public long getQuantityOut() {
		return quantityOut;
	}

	public void setQuantityOut(long quantityOut) {
		this.quantityOut = quantityOut;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public TransactionLine getTransactionLine() {
		return transactionLine;
	}

	public void setTransactionLine(TransactionLine transactionLine) {
		this.transactionLine = transactionLine;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public UoM getUoM() {
		return uoM;
	}

	public void setUoM(UoM uoM) {
		this.uoM = uoM;
	}
	
	public List<Stock> getListStock() {
		return listStock;
	}

	public void setListStock(List<Stock> listStock) {
		this.listStock = listStock;
	}

	public List<Integer> getListQty() {
		return listQty;
	}

	public void setListQty(List<Integer> listQty) {
		this.listQty = listQty;
	}

	public Report() {
	}
	
	public Report(Stock stock, long quantity) {
		super();
		this.stock = stock;
		this.quantity = quantity;
	}

	public Report(Project project, long quantity) {
		super();
		this.project = project;
		this.quantity = quantity;
	}
	
	public Report(Item item, UoM uoM, List<Stock> listStock, List<Integer> listQty, long quantity) {
		super();
		this.item = item;
		this.uoM = uoM;
		this.listStock = listStock;
		this.listQty = listQty;
		this.quantity = quantity;
	}

	public Report( Transaction transaction, Item item,  UoM uoM, long quantity) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.transaction = transaction;
	}

	public Report( Transaction transaction, Item item,  UoM uoM, String expDate, long quantity) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.transaction = transaction;
	}
	
	public Report( Project project , Item item, UoM uoM, long quantityIn, long quantityOut, long quantity) {
		super();
		this.quantityIn = quantityIn;
		this.quantityOut = quantityOut;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.project = project;
	}

	public Report( Project project , Item item, UoM uoM, String expDate, long quantityIn, long quantityOut, long quantity) {
		super();
		this.quantityIn = quantityIn;
		this.quantityOut = quantityOut;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.expDate = expDate;
		this.project = project;
	}
	
	public Report( Stock stock, Project project , Item item, UoM uoM, String expDate, long quantity) {
		super();
		this.stock = stock;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.expDate = expDate;
		this.project = project;
	}
	
	public Report(Project project, Item item, UoM uoM, long quantity) {
		super();
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.project = project;
	}

	public Report(Stock stock, Item item, UoM uoM, long quantity) {
		super();
		this.stock = stock;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
	}
	
	public Report(Transaction transaction, TransactionLine transactionLine) {
		super();
		this.transaction = transaction;
		this.transactionLine = transactionLine;
	}

	public Report(Transaction transaction, Stock stock, Item item, long quantity, UoM uoM) {
		super();
		this.stock = stock;
		this.item = item;
		this.quantity = quantity;
		this.uoM = uoM;
		this.transaction = transaction;
	}
	
	public Report(Item item2, long quantity2) {
		this.item = item2;
		this.quantity = quantity2;		
	}
	
	public Report(Stock stock, Item item, String expDate, long quantity) {
		super();
		this.stock = stock;
		this.item = item;
		this.expDate = expDate;
		this.quantity = quantity;
	}
	
	public Report(Project project, long quantityIn, long quantityOut, long quantity) {
		this.quantityOut = quantityOut;
		this.quantityIn = quantityIn;
		this.quantity = quantity;
		this.project = project;
	}

	public Report(Stock stock, long quantityIn, long quantityOut, long quantity) {
		this.quantityOut = quantityOut;
		this.quantityIn = quantityIn;
		this.quantity = quantity;
		this.stock = stock;
	}

	public Report(String postedDate, long quantityIn, long quantityOut, long quantity, String dayOfMonth) {
		this.quantityOut = quantityOut;
		this.quantityIn = quantityIn;
		this.quantity = quantity;
		this.postedDate = postedDate;
		this.dayOfMonth = dayOfMonth;
	}

	@Override
	public String toString() {
		return "Report [stock=" + stock + ", item=" + item + ", quantity=" + quantity + ", uoM=" + uoM + "]";
	}
}
