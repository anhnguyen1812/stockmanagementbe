package com.creasia.stock.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="sm_approved_shipment")
public class ApprovedShipment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="request_shipment_id")
	private RequestShipment requestShipment;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade= {CascadeType.MERGE})
	@JoinColumn(name="approve_by_id")
	private Employee approveBy;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "document_at")
	private String documentAt;
	
	@Column(name = "is_posted")
	private boolean isPosted;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="approved_shipment_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ApprovedShipmentLine> approvedShipmentLines;
	
	@Column(name = "total_delivered_qty")
	private int totalQuantity;

	public boolean isPosted() {
		return isPosted;
	}

	public void setPosted(boolean isPosted) {
		this.isPosted = isPosted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public RequestShipment getRequestShipment() {
		return requestShipment;
	}

	public void setRequestShipment(RequestShipment requestShipment) {
		this.requestShipment = requestShipment;
	}

	public Employee getApproveBy() {
		return approveBy;
	}

	public void setApproveBy(Employee approveBy) {
		this.approveBy = approveBy;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDocumentAt() {
		return documentAt;
	}

	public void setDocumentAt(String documentAt) {
		this.documentAt = documentAt;
	}

	public List<ApprovedShipmentLine> getApprovedShipmentLines() {
		return approvedShipmentLines;
	}

	public void setApprovedShipmentLines(List<ApprovedShipmentLine> approvedShipmentLines) {
		this.approvedShipmentLines = approvedShipmentLines;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public ApprovedShipment() {
		
	}
	public ApprovedShipment(String code, RequestShipment requestShipment, Employee approveBy, String createdAt,
			String documentAt, int totalQuantity) {
		super();
		this.code = code;
		this.requestShipment = requestShipment;
		this.approveBy = approveBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.totalQuantity = totalQuantity;
	}
	
	public ApprovedShipment(String code, RequestShipment requestShipment, Employee approveBy, String createdAt,
			String documentAt, List<ApprovedShipmentLine> approvedShipmentLines, int totalQuantity) {
		super();
		this.code = code;
		this.requestShipment = requestShipment;
		this.approveBy = approveBy;
		this.createdAt = createdAt;
		this.documentAt = documentAt;
		this.approvedShipmentLines = approvedShipmentLines;
		this.totalQuantity = totalQuantity;
	}

	@Override
	public String toString() {
		return "ApprovedShipment [id=" + id + ", code=" + code + ", requestShipment=" + requestShipment + ", approveBy="
				+ approveBy + ", createdAt=" + createdAt + ", documentAt=" + documentAt + ", approvedShipmentLines="
				+ approvedShipmentLines + ", totalQuantity=" + totalQuantity + "]";
	}
}
