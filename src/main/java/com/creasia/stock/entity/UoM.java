package com.creasia.stock.entity;


import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sm_uom")
public class UoM {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "created_at")
	private String createAt;
	
	@Column(name="is_blocked")
	private boolean isBlocked;
	
//	@OneToMany(fetch = FetchType.LAZY,
//			cascade= {CascadeType.ALL})
//	@JoinColumn(name = "UoM_id")
//	private List<Item> items;
	
	public UoM() {
		
	}

	public UoM(String name, String createAt, boolean isBlocked) {
		super();
		this.name = name;
		this.createAt = createAt;
		this.isBlocked = isBlocked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

//	public List<Item> getItems() {
//		return items;
//	}
//
//	public void setItems(List<Item> items) {
//		this.items = items;
//	}
//	
//	public void add(Item termpItem) {
//		if (items ==null) {
//			items = new ArrayList<>();
//		}
//		items.add(termpItem);
//		//termpItem.(this);
//	}

	@Override
	public String toString() {
		return "UoM [id=" + id + ", name=" + name + ", createAt=" + createAt + 
				", isBlocked=" + isBlocked + "]";
	}
	
	
}
