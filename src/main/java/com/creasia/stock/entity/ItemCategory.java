package com.creasia.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;

import net.bytebuddy.asm.Advice.This;

@Entity
@Table(name="sm_item_category")
public class ItemCategory {

	//define field
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@NotBlank
	@Column(name = "name")
	private String name;
	
	@Column(name="created_at")
	private String createAt;
	
	@Column(name="is_blocked")
	private boolean isBlocked;

//	@OneToMany( fetch = FetchType.LAZY,
//			cascade= {CascadeType.ALL})
//	@JoinColumn(name = "category_id")
//	private List<Item> items;
	
	//define constructor
	public ItemCategory(){
		
	} 
	public ItemCategory(String code, String name, String createAt, boolean isBlocked) {
		super();
		this.code = code;
		this.name = name;
		this.createAt = createAt;
		this.isBlocked = isBlocked;
	}
	
	//define getter/setter
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateAt() {
		return createAt;
	}
	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}
	public boolean isBlocked() {
		return isBlocked;
	}
	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
//	public List<Item> getItems() {
//		return items;
//	}
//	public void setItems(List<Item> items) {
//		this.items = items;
//	}
//
//	public void add(Item termpItem) {
//		if (items ==null) {
//			items = new ArrayList<>();
//		}
//		items.add(termpItem);
//		//termpItem.setItemCategory(this);
//	}
	
	//define to string
	@Override
	public String toString() {
		return "ItemCategory [id=" + id + ", code=" + code + ", name=" + name + ", createAt=" + createAt
				+ ", isBlocked=" + isBlocked + "]";
	}
	public String toJson()
	{
		ObjectMapper mapper = new ObjectMapper();
			
		String json = "";
		try {
			json = mapper.writeValueAsString(This.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		return json;
		
	}
	
	
}
