package com.creasia.stock.security.jwt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.creasia.stock.entity.Entities;

@Component
@ControllerAdvice
public class AuthEntryPointJwt implements AuthenticationEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(AuthEntryPointJwt.class);
	private JSONObject json = new JSONObject();
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		logger.error("Unauthorized error: {}", authException.getMessage());
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	@ExceptionHandler(value = { AccessDeniedException.class })
	  public void commence(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex ) throws IOException {
		
		logger.error("Unauthorized error: {}", ex.getMessage());
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized" );
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		//return new Entities(1, response.toString(), "Unauthorized");
//		response.setContentType("application/json");
//		json.put("", response.get)
//	    String output = json.toString();
//	    PrintWriter writer = response.getWriter();
//	    writer.write(output);
//	    writer.close();
//		System.out.println("response: " + response.getStatus());
//		
//		return null;
	}
}
