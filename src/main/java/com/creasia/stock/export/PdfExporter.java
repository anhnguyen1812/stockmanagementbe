package com.creasia.stock.export;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.basic.BasicTabbedPaneUI.FocusHandler;
import javax.swing.text.StyledEditorKit.FontFamilyAction;

import com.creasia.stock.entity.ShipmentNote;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.FontFactoryImp;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.AsianFontMapper;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class PdfExporter {
	
	private Font font;
	 private void writeTableHeader(PdfPTable table) {
	        PdfPCell cell = new PdfPCell();
	        cell.setBackgroundColor(Color.WHITE);
	        cell.setPadding(5);
	         
//	        Font font = FontFactory.getFont(FontFactory.HELVETICA);
	     
	        font.setColor(Color.BLACK);
	         
	        cell.setPhrase(new Phrase("STT", font));
	        table.addCell(cell);
	         
	        cell.setPhrase(new Phrase("Mã hàng", font));
	        table.addCell(cell);
	         
	        cell.setPhrase(new Phrase("Tên hàng", font));
	        table.addCell(cell);
	         
//	        cell.setPhrase(new Phrase("Mã scheme/ Mã chương trình", font));
//	        table.addCell(cell);
	         
	        cell.setPhrase(new Phrase("Tên dự án", font));
	        table.addCell(cell);    
	        
	        cell.setPhrase(new Phrase("ĐVT", font));
	        table.addCell(cell); 
	        
	        cell.setPhrase(new Phrase("ExpDate", font));
	        table.addCell(cell); 
	        
	        cell.setPhrase(new Phrase("Số lượng xuất", font));
	        table.addCell(cell); 
	        
	        cell.setPhrase(new Phrase("Ghi chú", font));
	        table.addCell(cell); 
	        
	    }
	     
	    private void writeTableData(PdfPTable table, ShipmentNote shipmentNote) {
	    
	    	for(int i = 0; i< shipmentNote.getShipmentNoteLines().size(); i++) {
	    		table.addCell(new Phrase(String.valueOf(i+1), font));
	    		table.addCell(new Phrase((shipmentNote.getShipmentNoteLines().get(i).getItem().getCode()),font));
	    		table.addCell(new Phrase((shipmentNote.getShipmentNoteLines().get(i).getItem().getName()),font));
//	    		table.addCell(new Phrase((shipmentNote.getApprovedShipment().getRequestShipment().getProject().getCode()),font));
	    		table.addCell(new Phrase((shipmentNote.getApprovedShipment().getRequestShipment().getProject().getName()),font));
	    		table.addCell(new Phrase((shipmentNote.getShipmentNoteLines().get(i).getUoM().getName()),font));
	    		table.addCell(new Phrase((shipmentNote.getShipmentNoteLines().get(i).getExpDate()),font));
	    		table.addCell(new Phrase(String.valueOf(shipmentNote.getShipmentNoteLines().get(i).getPostedQuantity()),font));
	    		table.addCell(new Phrase(" ",font));
	    	}
	    
	           
	    }
	    
	    private void writeFooterPage(Document document) {
	    	//Stocker
	    	
	    	
	        font.setColor(Color.BLACK);
	        
	        font.setSize(10);
	      
	        Paragraph date = new Paragraph(".../.../2021", font);
	        date.add("                                                  ");
	        date.add(".../.../2021");
	        date.add("                                                             ");
	        date.add(".../.../2021");
	        date.setAlignment(Paragraph.ALIGN_LEFT);
	        date.setIndentationLeft(50);
	        date.setSpacingBefore(50);
	        document.add(date);
	
	        font.setColor(Color.BLACK);
	        
	        font.setSize(10);
	        font.setStyle(1);
	        Paragraph title = new Paragraph("Thủ kho", font);
	        title.add("                                                  ");
	        title.add("Nguời vận chuyển");
	        title.add("                                                  ");
	        title.add("Người nhận");
	        title.setAlignment(Paragraph.ALIGN_LEFT);
	        title.setIndentationLeft(50);
	       
	         
	     
	     
	        document.add(title);
	       
	       
	    }
	     
	    public void export(HttpServletResponse response, ShipmentNote shipmentNote ) throws DocumentException, IOException {
	        Document document = new Document(PageSize.A4);
	        PdfWriter.getInstance(document, response.getOutputStream());
	        document.open();
	        Path path = null;
			try {
				path = Paths.get("/var/www/stock/classes/logo_creasia.png");
//				path = Paths.get(ClassLoader.getSystemResource("logo_creasia.png").toURI());
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("e.toString(): " + e.toString());
			}
			
			BaseFont bfComic = null;
	        File file = new File("/var/www/stock/classes/STIXTwoText.ttf");
//	        ClassLoader classLoader = getClass().getClassLoader();
//	        File file = new File(classLoader.getResource("STIXTwoText.ttf").getFile());
	        System.out.println("file: " + file);
	        String absolutePath = file.getAbsolutePath();
			try {
				bfComic = BaseFont.createFont(absolutePath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			} catch (DocumentException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			font = new Font(bfComic, 12);
	        font.setColor(Color.BLACK);
			
	        PdfPTable tableHeader = new PdfPTable(2);
	        tableHeader.setWidthPercentage(100f);
	        PdfPCell cellHeader = new PdfPCell();
	        cellHeader.setBorderWidth(0);
	        Image img = Image.getInstance(path.toAbsolutePath().toString());
	        img.scalePercent(10);
	        Paragraph header = new Paragraph("",font);
//	        header.add(img);
	        header.add(new Chunk(img, 0, 0, true));
//	        header.setAlignment(Paragraph.ALIGN_);
//	        document.add(header);
	        cellHeader.addElement(header);
	        tableHeader.addCell(cellHeader);
	        document.add(tableHeader);
	        
	        font.setSize(10);
	        font.setStyle(0);
	        Paragraph documentNo = new Paragraph("Document date:   " + shipmentNote.getDocumentAt() , font);
	        documentNo.add("\n");
//	        Date finish = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(shipmentNote.getDocumentAt());
	        documentNo.add("Document No:   " + shipmentNote.getCode());
	        documentNo.add("\n");
	        documentNo.add("Request By:   " + shipmentNote.getApprovedShipment().getRequestShipment().getCreateBy().getName());
//	        header.add(documentNo);
	        documentNo.setAlignment(Paragraph.ALIGN_RIGHT);
	        documentNo.setIndentationRight(15);
	        cellHeader = new PdfPCell();
	        cellHeader.setBorderWidth(0);
	        cellHeader.addElement(documentNo);
	        tableHeader.addCell(cellHeader);
	        document.add(tableHeader);
	        
	        font.setSize(18);
	        font.setStyle(1);
	        Paragraph title = new Paragraph("PHIẾU XUẤT KHO", font);
	        title.setAlignment(Paragraph.ALIGN_CENTER);
	        
	        document.add(title);
	   
	        font.setSize(12);
	        font.setStyle(0);
	        Paragraph note_title = new Paragraph("(Kiêm biên bản giao nhận hàng)", font);
	        note_title.setAlignment(Paragraph.ALIGN_CENTER);
	        document.add(note_title);
	        
	        font.setSize(12);
	        font.setStyle(1);
	        Paragraph p_1 = new Paragraph("I. THÔNG TIN NHẬN HÀNG", font);
	        p_1.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1.setSpacingBefore(30);
	        document.add(p_1);
	        
	        font.setSize(12);
	        font.setStyle(0);
	        
	        Paragraph p_1a = new Paragraph("Họ tên người nhận:" + "              ", font);
	        p_1a.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1a.setSpacingBefore(2);
	        p_1a.add(shipmentNote.getRecipientName());
	        
	        Paragraph p_1b = new Paragraph("Đơn vị:" + "                                    ", font);
	        p_1b.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1b.add(shipmentNote.getShippingUnit());
	        
	        Paragraph p_1c = new Paragraph("Phương tiện vận chuyển:" + "    ", font);
	        p_1c.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1c.add(shipmentNote.getMeansOfConveyance());
	       
	        Paragraph p_1d = new Paragraph("Số xe:" + "                                      ", font);
	        p_1d.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1d.add(shipmentNote.getLicensePlates());
	       
	        Paragraph p_1e = new Paragraph("Số điện thoại:" + "                        ", font);
	        p_1e.setAlignment(Paragraph.ALIGN_LEFT);
	        p_1e.add(shipmentNote.getPhone());
	        
	        document.add(p_1a);
	        document.add(p_1b);
	        document.add(p_1c);
	        document.add(p_1d);
	        document.add(p_1e);
	        
	        font.setSize(12);
	        font.setStyle(1);
	        Paragraph p_2 = new Paragraph("I. THÔNG TIN HÀNG HÓA", font);
	        p_2.setAlignment(Paragraph.ALIGN_LEFT);
	        p_2.setSpacingBefore(10);
	        p_2.setSpacingAfter(5);
	        document.add(p_2);
	        font.setStyle(0);

	         
	        PdfPTable table = new PdfPTable(8);
	        table.setWidthPercentage(100f);
	        table.setWidths(new float[] {1.0f, 2.5f, 3.0f, 2.5f, 1.5f,2.0f,1.75f,1.75f});
	        table.setSpacingBefore(10);
	         
	        writeTableHeader(table);
	        writeTableData(table, shipmentNote);
	        
	       
	        document.add(table);
	        writeFooterPage(document);
	        document.close();
	         
	    }
}
