package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.VendorDAO;
import com.creasia.stock.entity.Customer;
import com.creasia.stock.entity.Vendor;

@Service
public class VendorServiceImpl implements VendorService {

	private VendorDAO vendorDAO;
	
	@Autowired
	public VendorServiceImpl(VendorDAO theVendorDAO) {
	
		vendorDAO = theVendorDAO;
	}
	
	
	@Override
	@Transactional
	public List<Vendor> findAll() {

		return vendorDAO.findAll();
	}

	@Override
	@Transactional
	public Vendor findById(int theId) {
		
		return vendorDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Vendor theVendor) {

		vendorDAO.save(theVendor);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {

		vendorDAO.delete(theId);
	}

	@Override
	@Transactional
	public void update(Vendor theVendor) {
		//System.out.println("the company: " + theCustomer);
		vendorDAO.update(theVendor);
	}
	
	@Override
	@Transactional
	public int findNewCode() {
		return vendorDAO.findNewCode();
	}
}



