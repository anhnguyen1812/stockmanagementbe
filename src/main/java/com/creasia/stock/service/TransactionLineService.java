package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.TransactionLine;

public interface TransactionLineService {
	
	public List<TransactionLine> findAll();

	public TransactionLine findById(int theId);

	public void save(TransactionLine theTransactionLine);

	public void update(TransactionLine theTransactionLine);

	public int findNewCode();

	public void deleteById(int theId);
	
	public List<TransactionLine> findByTransactionId(int theProvinceId);
}
