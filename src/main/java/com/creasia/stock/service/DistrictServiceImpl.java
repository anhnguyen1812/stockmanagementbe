package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.DistrictDAO;
import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Province;

@Service
public class DistrictServiceImpl implements DistrictService {

	private DistrictDAO districtDAO;
	
	@Autowired
	public DistrictServiceImpl(DistrictDAO theDistrictDAO) {
		districtDAO = theDistrictDAO;
	}
	
	@Override
	@Transactional
	public List<District> findAll() {
		// TODO Auto-generated method stub
		return districtDAO.findAll();
	}

	@Override
	@Transactional
	public District findById(int theId) {
		// TODO Auto-generated method stub
		return districtDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(District theDistrict) {
		// TODO Auto-generated method stub
		districtDAO.save(theDistrict);

	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		districtDAO.delete(theId);
	}
	
	@Override
	@Transactional
	public List<District> findDistrictByProvinceId(int theProvinceId) {
		try {
			return districtDAO.findDistrictByProvinceId(theProvinceId);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error: " + e.toString());
		}
		return districtDAO.findDistrictByProvinceId(theProvinceId);
	}

}
