package com.creasia.stock.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ReturnReceiptDAO;
import com.creasia.stock.entity.ReturnReceipt;

@Service
public class ReturnReceiptServiceImpl implements ReturnReceiptService {

	private ReturnReceiptDAO returnReceiptDAO;

	@Autowired
	public ReturnReceiptServiceImpl(ReturnReceiptDAO theReturnReceiptDAO) {
		returnReceiptDAO = theReturnReceiptDAO;
	}

	@Override
	@Transactional
	public List<ReturnReceipt> findAll() {
		// TODO Auto-generated method stub
		return returnReceiptDAO.findAll();
	}

	@Override
	@Transactional
	public List<ReturnReceipt> findByRequestShipmentId(int theRequestId) {
		return returnReceiptDAO.findByRequestShipmentId(theRequestId);
	}

	@Override
	@Transactional
	public ReturnReceipt findById(int theId) {
		// TODO Auto-generated method stub
		return returnReceiptDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(ReturnReceipt theReturnReceipt) {
		// TODO Auto-generated method stub
		try {
			returnReceiptDAO.save(theReturnReceipt);
		} catch (ConstraintViolationException e) {
			// TODO: handle exception
			System.out.println(" error " + e.toString());
		}

	}

	@Override
	@Transactional
	public void update(ReturnReceipt theReturnReceipt) {
		// TODO Auto-generated method stub
		returnReceiptDAO.update(theReturnReceipt);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return returnReceiptDAO.findNewCode();
	}

	@Override
	@Transactional
	public String findLastCode() {
		return returnReceiptDAO.findLastCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		returnReceiptDAO.delete(theId);

	}

}
