package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.TransactionLineDAO;
import com.creasia.stock.entity.TransactionLine;

@Service
public class TransactionLineServiceImpl implements TransactionLineService {

	private TransactionLineDAO transactionLineDAO;

	@Autowired
	public TransactionLineServiceImpl(TransactionLineDAO theTransactionLineDAO) {
		transactionLineDAO = theTransactionLineDAO;
	}

	@Override
	@Transactional
	public List<TransactionLine> findAll() {
		// TODO Auto-generated method stub
		return transactionLineDAO.findAll();
	}

	@Override
	@Transactional
	public TransactionLine findById(int theId) {
		// TODO Auto-generated method stub
		return transactionLineDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(TransactionLine theTransactionLine) {
		// TODO Auto-generated method stub
		transactionLineDAO.save(theTransactionLine);

	}

	@Override
	@Transactional
	public void update(TransactionLine theTransactionLine) {
		// TODO Auto-generated method stub
		transactionLineDAO.update(theTransactionLine);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return transactionLineDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		transactionLineDAO.delete(theId);

	}

	@Override
	@Transactional
	public List<TransactionLine> findByTransactionId(int theProvinceId) {

		return transactionLineDAO.findByTransactionId(theProvinceId);
	}
}
