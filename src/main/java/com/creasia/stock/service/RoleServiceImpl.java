package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.entity.Role;
import com.creasia.stock.dao.RoleDAO;

@Service
public class RoleServiceImpl implements RoleService {
	private RoleDAO roleDAO;

	@Autowired
	public RoleServiceImpl(RoleDAO theRoleDAO) {
		roleDAO = theRoleDAO;
	}

	@Override
	@Transactional
	public List<Role> findAll() {

		return roleDAO.findAll();
	}
}
