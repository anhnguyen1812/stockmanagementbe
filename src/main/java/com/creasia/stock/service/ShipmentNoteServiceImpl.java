package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ShipmentNoteDAO;
import com.creasia.stock.entity.ShipmentNote;

@Service
public class ShipmentNoteServiceImpl implements ShipmentNoteService {

	private ShipmentNoteDAO shipmentNoteDAO;
	
	@Autowired
	public ShipmentNoteServiceImpl(ShipmentNoteDAO theShipmentNoteDAO) {
		shipmentNoteDAO = theShipmentNoteDAO;
	}
	
	@Override
	@Transactional
	public List<ShipmentNote> findAll() {
		// TODO Auto-generated method stub
		return shipmentNoteDAO.findAll();
	}
	
	@Override
	@Transactional
	public long countShipmentNotes(){
		System.out.println("itemDAO.countItem():: " + shipmentNoteDAO.countShipmentNotes());
		return shipmentNoteDAO.countShipmentNotes();
	}
	
	@Override
	@Transactional
	public List<ShipmentNote> findApprovedShipment(){
		return shipmentNoteDAO.findApprovedShipment();
	}
	
	@Override
	@Transactional
	public List<ShipmentNote> findPostedShipment(){
		return shipmentNoteDAO.findPostedShipment();
	}

	@Override
	@Transactional
	public List<ShipmentNote> findByRequestShipmentId(int theRequestId){
		return shipmentNoteDAO.findByRequestShipmentId(theRequestId);
	}
	
	@Override
	@Transactional
	public ShipmentNote findById(int theId) {
		// TODO Auto-generated method stub
		return shipmentNoteDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(ShipmentNote theShipmentNote) {
		// TODO Auto-generated method stub
		shipmentNoteDAO.save(theShipmentNote);
		

	}

	@Override
	@Transactional
	public void update(ShipmentNote theShipmentNote) {
		// TODO Auto-generated method stub
		shipmentNoteDAO.update(theShipmentNote);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return shipmentNoteDAO.findNewCode();
	}
	
	@Override
	@Transactional
	public String findLastCode() {
		return shipmentNoteDAO.findLastCode();
	}
	
	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		shipmentNoteDAO.delete(theId);

	}

}
