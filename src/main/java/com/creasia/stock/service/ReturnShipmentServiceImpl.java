package com.creasia.stock.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ReturnShipmentDAO;
import com.creasia.stock.entity.ReturnShipment;

@Service
public class ReturnShipmentServiceImpl implements ReturnShipmentService {

	private ReturnShipmentDAO returnShipmentDAO;

	@Autowired
	public ReturnShipmentServiceImpl(ReturnShipmentDAO theReturnShipmentDAO) {
		returnShipmentDAO = theReturnShipmentDAO;
	}

	@Override
	@Transactional
	public List<ReturnShipment> findAll() {
		// TODO Auto-generated method stub
		return returnShipmentDAO.findAll();
	}

	@Override
	@Transactional
	public List<ReturnShipment> findByRequestShipmentId(int theRequestId) {
		return returnShipmentDAO.findByRequestShipmentId(theRequestId);
	}

	@Override
	@Transactional
	public ReturnShipment findById(int theId) {
		// TODO Auto-generated method stub
		return returnShipmentDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(ReturnShipment theReturnShipment) {
		// TODO Auto-generated method stub
		try {
			returnShipmentDAO.save(theReturnShipment);
		} catch (ConstraintViolationException e) {
			// TODO: handle exception
			System.out.println(" error " + e.toString());
		}

	}

	@Override
	@Transactional
	public void update(ReturnShipment theReturnShipment) {
		// TODO Auto-generated method stub
		returnShipmentDAO.update(theReturnShipment);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return returnShipmentDAO.findNewCode();
	}

	@Override
	@Transactional
	public String findLastCode() {
		return returnShipmentDAO.findLastCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		returnShipmentDAO.delete(theId);

	}

}
