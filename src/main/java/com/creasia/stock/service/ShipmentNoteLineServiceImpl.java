package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ShipmentNoteLineDAO;
import com.creasia.stock.entity.ShipmentNoteLine;

@Service
public class ShipmentNoteLineServiceImpl implements ShipmentNoteLineService {

	private ShipmentNoteLineDAO shipmentNoteLineDAO;

	@Autowired
	public ShipmentNoteLineServiceImpl(ShipmentNoteLineDAO theShipmentNoteLineDAO) {
		shipmentNoteLineDAO = theShipmentNoteLineDAO;
	}

	@Override
	@Transactional
	public List<ShipmentNoteLine> findAll() {
		// TODO Auto-generated method stub
		return shipmentNoteLineDAO.findAll();
	}

	@Override
	@Transactional
	public ShipmentNoteLine findById(int theId) {
		// TODO Auto-generated method stub
		return shipmentNoteLineDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(ShipmentNoteLine theShipmentNoteLine) {
		// TODO Auto-generated method stub
		shipmentNoteLineDAO.save(theShipmentNoteLine);

	}

	@Override
	@Transactional
	public void update(ShipmentNoteLine theShipmentNoteLine) {
		// TODO Auto-generated method stub
		shipmentNoteLineDAO.update(theShipmentNoteLine);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return shipmentNoteLineDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		shipmentNoteLineDAO.delete(theId);

	}

	@Override
	@Transactional
	public List<ShipmentNoteLine> findByShipmentNoteId(int theProvinceId) {

		return shipmentNoteLineDAO.findByShipmentNoteId(theProvinceId);
	}
}
