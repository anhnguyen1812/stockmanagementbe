package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.PermissionAssignmentDAO;
import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;

@Service
public class PermissionAssignmentServiceImpl implements PermissionAssignmentService {

	private PermissionAssignmentDAO permissionAssignmentDAO;

	@Autowired
	public PermissionAssignmentServiceImpl(PermissionAssignmentDAO thePermissionAssignmentDAO) {
		permissionAssignmentDAO = thePermissionAssignmentDAO;
	}

	@Override
	@Transactional
	public List<PermissionAssignment> findAll() {
		return permissionAssignmentDAO.findAll();
	}

	@Override
	@Transactional
	public PermissionAssignment findById(int theId) {
		return permissionAssignmentDAO.findById(theId);
	}

	@Override
	@Transactional
	public List<PermissionAssignment> getPermissionByUser(long userId) {
		return permissionAssignmentDAO.getPermissionByUser(userId);
	}

	@Override
	@Transactional
	public void save(PermissionAssignment thePermissionAssignment) {
		try {
			permissionAssignmentDAO.save(thePermissionAssignment);
		} catch (Exception e) {

		}
	}

	@Override
	@Transactional
	public List<User> getUserByStockId(int stockId) {
		return permissionAssignmentDAO.getUserByStockId(stockId);
	}

	@Override
	@Transactional
	public List<Employee> getUserAdmin() {
		return permissionAssignmentDAO.getUserAdmin();
	}
	
	@Override
	@Transactional
	public Employee getUserManager(int userId) {
		
		return permissionAssignmentDAO.getUserManager(userId);
	}

	@Override
	@Transactional
	public void update(PermissionAssignment per) {
		// TODO Auto-generated method stub
		try {
			permissionAssignmentDAO.update(per);
		} catch (Exception e) {

		}
	}
}
