package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.StockDAO;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Vendor;

@Service
public class StockServiceImpl implements StockService {

	private StockDAO stockDAO;
	
	@Autowired
	public StockServiceImpl(StockDAO theStockDAO) {
		stockDAO = theStockDAO;
	}
	
	@Override
	@Transactional
	public List<Stock> findAll() {
		// TODO Auto-generated method stub
		
		return stockDAO.findAll();
	}

	@Override
	@Transactional
	public Stock findById(int theId) {
		// TODO Auto-generated method stub
		return stockDAO.findById(theId);
	}

	@Override
	@Transactional
	public long countStock() {
		System.out.println("itemDAO.countItem():: " + stockDAO.countStock());
		return stockDAO.countStock();
	}
	@Override
	@Transactional
	public void save(Stock theStock) {
		// TODO Auto-generated method stub
		try {
			stockDAO.save(theStock);
		} catch (Exception e) {
			System.out.println("Exception SERVICE: " + e.toString());
		}
		
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		stockDAO.delete(theId);
	}

	@Override
	@Transactional
	public void update(Stock theStock) {
		//System.out.println("the company: " + theCustomer);
		stockDAO.update(theStock);
	}
	
	@Override
	@Transactional
	public int findNewCode(String theHeaderCode) {
		try {
			System.out.println("theHeaderCode: " + theHeaderCode);
			return stockDAO.findNewCode(theHeaderCode);
		} catch (Exception e) {
			System.out.println("exception: " + e.toString());
			return 0;
		}
		
	}
}
