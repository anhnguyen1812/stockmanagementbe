package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ReturnReceipt;

public interface ReturnReceiptService {
	
	public List<ReturnReceipt> findAll();

	public ReturnReceipt findById(int theId);
	
	public List<ReturnReceipt> findByRequestShipmentId(int theRequestId);

	public void save(ReturnReceipt theReturnReceipt);

	public void update(ReturnReceipt theReturnReceipt);

	public int findNewCode();
	
	public String findLastCode();

	public void deleteById(int theId);
}
