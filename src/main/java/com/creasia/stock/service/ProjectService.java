package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.RequestShipmentLine;

public interface ProjectService {

	public List<Project> findAll();
	
	public long countProject();
	
	public Project findById(int theId);
	
	public long findByCode(String theCode);
	
	public void save(Project theProject);
	
	public void update(Project theProject);
	
	public int findNewCode() ;
	
	public List<Project>  findProjectByPmId(int pmId);
	
	public void deleteById(int theId);
}
