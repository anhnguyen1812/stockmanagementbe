package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ApprovedShipmentLineDAO;
import com.creasia.stock.entity.ApprovedShipmentLine;

@Service
public class ApprovedShipmentLineServiceImpl implements ApprovedShipmentLineService {

	private ApprovedShipmentLineDAO approvedShipmentLineDAO;

	@Autowired
	public ApprovedShipmentLineServiceImpl(ApprovedShipmentLineDAO theApprovedShipmentLineDAO) {
		approvedShipmentLineDAO = theApprovedShipmentLineDAO;
	}

	@Override
	@Transactional
	public List<ApprovedShipmentLine> findAll() {
		// TODO Auto-generated method stub
		return approvedShipmentLineDAO.findAll();
	}

	@Override
	@Transactional
	public ApprovedShipmentLine findById(int theId) {
		// TODO Auto-generated method stub
		return approvedShipmentLineDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(ApprovedShipmentLine theApprovedShipmentLine) {
		// TODO Auto-generated method stub
		approvedShipmentLineDAO.save(theApprovedShipmentLine);

	}

	@Override
	@Transactional
	public void update(ApprovedShipmentLine theApprovedShipmentLine) {
		// TODO Auto-generated method stub
		approvedShipmentLineDAO.update(theApprovedShipmentLine);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return approvedShipmentLineDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		approvedShipmentLineDAO.delete(theId);

	}

	@Override
	@Transactional
	public List<ApprovedShipmentLine> findByApprovedShipmentId(int theProvinceId) {

		return approvedShipmentLineDAO.findByApprovedShipmentId(theProvinceId);
	}
}