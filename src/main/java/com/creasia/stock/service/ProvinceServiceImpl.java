package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.creasia.stock.dao.ProvinceDAO;
import com.creasia.stock.entity.Province;

@Service
public class ProvinceServiceImpl implements ProvinceService {

	private ProvinceDAO provinceDAO;
	
	@Autowired
	public ProvinceServiceImpl(ProvinceDAO theProvinceDAO) {
		provinceDAO = theProvinceDAO;
	}
	
	@Override
	public List<Province> findAll() {
		// TODO Auto-generated method stub
		return provinceDAO.findAll();
	}

	@Override
	public Province findById(int theId) {
		// TODO Auto-generated method stub
		return provinceDAO.findById(theId);
	}

	@Override
	public void save(Province theProvince) {
		// TODO Auto-generated method stub
		provinceDAO.save(theProvince);

	}

	@Override
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		provinceDAO.delete(theId);
	}

}
