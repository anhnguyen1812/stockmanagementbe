package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.UomDAO;
import com.creasia.stock.entity.UoM;

@Service
public class UomSerciceImpl implements UomService {

	private UomDAO uomDAO;
	
	@Autowired
	public UomSerciceImpl(UomDAO theUomDAO) {
		uomDAO = theUomDAO;
	}
	
	@Override
	@Transactional
	public List<UoM> findAll() {
		
		return uomDAO.findAll();
	}

	@Override
	@Transactional
	public UoM findById(int theId) {

		return uomDAO.findById(theId);
	}

	@Override
	@Transactional
	public List<UoM> findByName(String name){
		return uomDAO.findByName(name);
	}

	

	@Override
	@Transactional
	public void save(UoM theUoM) {

		uomDAO.save(theUoM);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {

		uomDAO.delete(theId);
	}

}
