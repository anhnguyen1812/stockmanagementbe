package com.creasia.stock.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.creasia.stock.dao.EmployeeDAO;
import com.creasia.stock.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDAO employeeDAO;

	@Autowired
	public EmployeeServiceImpl(EmployeeDAO theEmployeeDAO) {

		employeeDAO = theEmployeeDAO;
	}

	@Override
	@Transactional
	public List<Employee> findAll() {

		return employeeDAO.findAll();
	}

	@Override
	@Transactional
	public Employee findById(int theId) {

		return employeeDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Employee theEmployee) {

		employeeDAO.save(theEmployee);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {

		employeeDAO.delete(theId);
	}

	@Override
	@Transactional
	public void update(Employee theEmployee) {
		System.out.println("the company: " + theEmployee);
		employeeDAO.update(theEmployee);
	}

	@Override
	@Transactional
	public int findNewCode() {
		return employeeDAO.findNewCode();
	}

}
