package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ReturnShipment;

public interface ReturnShipmentService {
	
	public List<ReturnShipment> findAll();

	public ReturnShipment findById(int theId);
	
	public List<ReturnShipment> findByRequestShipmentId(int theRequestId);

	public void save(ReturnShipment theReturnShipment);

	public void update(ReturnShipment theReturnShipment);

	public int findNewCode();
	
	public String findLastCode();

	public void deleteById(int theId);
}
