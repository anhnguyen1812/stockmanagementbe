package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ShipmentNoteLine;

public interface ShipmentNoteLineService {
	
	public List<ShipmentNoteLine> findAll();

	public ShipmentNoteLine findById(int theId);

	public void save(ShipmentNoteLine theShipmentNoteLine);

	public void update(ShipmentNoteLine theShipmentNoteLine);

	public int findNewCode();

	public void deleteById(int theId);
	
	public List<ShipmentNoteLine> findByShipmentNoteId(int theProvinceId);
}
