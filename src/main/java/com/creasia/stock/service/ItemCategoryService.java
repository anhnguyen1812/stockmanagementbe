package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ItemCategory;

public interface ItemCategoryService {

	public List<ItemCategory> findAll();
	
	public ItemCategory findById(int theId);
	public List<ItemCategory> findByName(String name);
	public int findNewCode();
	
	public void save(ItemCategory theItemCategory);
	
	public void update(ItemCategory theItemCategory);
	
	public void deleteById(int theId);
	
	
}
