package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Department;
import com.creasia.stock.entity.District;

public interface DepartmentService {

	public List<Department> findAll();

	public Department findById(int theId);

	public void save(Department theDepartment);

	public void update(Department theDepartment);

	public void deleteById(int theId);

	public int findNewCode();

	public List<Department> findDepartmentByCompanyId(int companyId);
}
