package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Province;

public interface ProvinceService {

	public List<Province> findAll();
	
	public Province findById(int theId);
	
	public void save(Province theProvince);
	
	public void deleteById(int theId);
}
