package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ReceiptNoteLine;

public interface ReceiptNoteLineService {
	
	public List<ReceiptNoteLine> findAll();

	public ReceiptNoteLine findById(int theId);

	public void save(ReceiptNoteLine theReceiptNoteLine);

	public void update(ReceiptNoteLine theReceiptNoteLine);

	public int findNewCode();

	public void deleteById(int theId);
	
	public List<ReceiptNoteLine> findByReceiptNoteId(int theProvinceId);
}
