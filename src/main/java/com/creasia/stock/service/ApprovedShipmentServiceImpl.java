package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ApprovedShipmentDAO;
import com.creasia.stock.entity.ApprovedShipment;

@Service
public class ApprovedShipmentServiceImpl implements ApprovedShipmentService {

	private ApprovedShipmentDAO approvedShipmentDAO;
	
	@Autowired
	public ApprovedShipmentServiceImpl(ApprovedShipmentDAO theApprovedShipmentDAO) {
		approvedShipmentDAO = theApprovedShipmentDAO;
	}
	
	@Override
	@Transactional
	public List<ApprovedShipment> findAll() {
		// TODO Auto-generated method stub
		
		return approvedShipmentDAO.findAll();
	}
	
	@Override
	@Transactional
	public List<ApprovedShipment> findApprovedShipment(){
		return approvedShipmentDAO.findApprovedShipment();
	}
	
//	@Override
//	@Transactional
//	public List<ApprovedShipment> findPostedShipment(){
//		return approvedShipmentDAO.findPostedShipment();
//	}

	@Override
	@Transactional
	public List<ApprovedShipment> findByRequestShipmentId(int theRequestId){
		return approvedShipmentDAO.findByRequestShipmentId(theRequestId);
	}
	
	@Override
	@Transactional
	public ApprovedShipment findById(int theId) {
		// TODO Auto-generated method stub
		return approvedShipmentDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(ApprovedShipment theApprovedShipment) {
		// TODO Auto-generated method stub
		approvedShipmentDAO.save(theApprovedShipment);
	}

	@Override
	@Transactional
	public void update(ApprovedShipment theApprovedShipment) {
		// TODO Auto-generated method stub
		approvedShipmentDAO.update(theApprovedShipment);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return approvedShipmentDAO.findNewCode();
	}
	
	@Override
	@Transactional
	public String findLastCode() {
		return approvedShipmentDAO.findLastCode();
	}
	
	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		approvedShipmentDAO.delete(theId);

	}

}
