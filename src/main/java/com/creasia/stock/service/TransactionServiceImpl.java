package com.creasia.stock.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.TransactionDAO;
import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.Report;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.UoM;

@Service
public class TransactionServiceImpl implements TransactionService {

	private TransactionDAO transactionDAO;

	@Autowired
	public TransactionServiceImpl(TransactionDAO theTransactionDAO) {
		transactionDAO = theTransactionDAO;
	}

	@Override
	@Transactional
	public List<Transaction> findAll() {
		// TODO Auto-generated method stub
		return transactionDAO.findAll();
	}

	@Override
	@Transactional
	public long countTransactionInToday() {
//		System.out.println("itemDAO.countItem():: " + projectDAO.countProject());
		return transactionDAO.countTransactionInToday();
	}

	@Override
	@Transactional
	public Transaction findById(int theId) {
		// TODO Auto-generated method stub
		return transactionDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(Transaction theTransaction) {
		// TODO Auto-generated method stub
		transactionDAO.save(theTransaction);

	}

	@Override
	@Transactional
	public void update(Transaction theTransaction) {
		// TODO Auto-generated method stub
		transactionDAO.update(theTransaction);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return transactionDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		transactionDAO.delete(theId);
	}

	@Override
	@Transactional
	public List<Object[]> getReportInStock() {
		return transactionDAO.getReportInStock();
	}

	@Override
	@Transactional
	public List<Object[]> getReportByProject() {
		return transactionDAO.getReportByProject();
	}

	@Override
	@Transactional
	public Transaction findByNoteCode(String theNoteCode) {
		Transaction transaction = transactionDAO.findByNoteCode(theNoteCode);
//		System.out.println("tran seveice: " + transaction);
		return transaction;
	}

	@Override
	@Transactional
	public long findInventory(int stockId, int itemId, int uomId) {
		try {
			long x = transactionDAO.findInventory(stockId, itemId, uomId);
			System.out.println("itemId Service==== " + x);
			return x;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return 0;
		}
	}

	@Override
	@Transactional
	public long findInventoryByStockAndProject(int stockId, int projectId, int itemId, int uomId) {
		try {
			long x = transactionDAO.findInventoryByStockAndProject(stockId, projectId, itemId, uomId);
			System.out.println("itemId Service==== " + x);
			return x;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return 0;
		}
	}

	@Override
	@Transactional
	public long findInventoryByItemAndExpDate(int projectId, int stockId, int itemId, int uomId, String expDate) {
		try {
			long x = transactionDAO.findInventoryByItemAndExpDate(projectId, stockId, itemId, uomId, expDate);
			System.out.println("itemId Service==== " + x);
			return x;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return 0;
		}
	}

	@Override
	@Transactional
	public List<Report> getReportByProjectIdStockIdExpDate(int stockId, int projectId, int itemId, int uomId) {
		try {
			List<Object[]> resultList = transactionDAO.getReportByProjectIdStockIdExpDate(stockId, projectId, itemId,
					uomId);
			if (resultList == null) {
				return null;
			}
			List<Report> reportList = new ArrayList<Report>();
			Stock stock = new Stock();
			Project project = new Project();
			Item item = new Item();
			UoM uoM = new UoM();
			String expDate = "";
//			int name = 0;
			for (Object[] result : resultList) {
				stock = (Stock) result[0];
				project = (Project) result[1];
				item = (Item) result[2];
				uoM = (UoM) result[3];
				expDate = (String) result[4];
				long quantity = ((Number) result[5]).intValue();
				System.out.println("result Report: stock " + stock.getId() + ", item " + item.getName() + ", UoM "
						+ uoM.getName() + ", quantity " + quantity);
				if (quantity > 0) {
					reportList.add(new Report(stock, project, item, uoM, expDate, quantity));
				}
			}

			System.out.println("reportListAll: " + reportList);
			return reportList;

//			return resultList;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

//	@Transactional
//	public List<Report> getReportItemExpDate(int stockId, int projectId, int itemId, int uomId) {
//		List<Object[]> resultList = getReportByProjectIdStockIdExpDate(stockId, projectId, itemId, uomId);
//		System.out.println("result Report: " + resultList);
//		if (resultList == null) {
//			return null;
//		}
//		List<Report> reportList = new ArrayList<Report>();
//		Stock stock = new Stock();
//		Project project = new Project();
//		Item item = new Item();
//		UoM uoM = new UoM();
//		String expDate = "";
////		int name = 0;
//		for (Object[] result : resultList) {
//			stock = (Stock) result[0];
//			project = (Project) result[1];
//			item = (Item) result[2];
//			uoM = (UoM) result[3];
//			expDate = (String) result[4];
//			long quantity = ((Number) result[5]).intValue();
//			System.out.println("result Report: stock " + stock.getId() + ", item " + item.getName() + ", UoM "
//					+ uoM.getName() + ", quantity " + quantity);
//			if (quantity != 0) {
//				reportList.add(new Report(stock, project, item, uoM, expDate, quantity));
//			}
//		}
//		return reportList;
//	}

	@Override
	@Transactional
	public List<Object[]> getReportAll() {
		try {
			List<Object[]> reportListAll = transactionDAO.getReportAll();
			System.out.println("reportListAll: " + reportListAll);
			return reportListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getReportByStockId(int stockId) {
		try {
			List<Object[]> reportListAll = transactionDAO.getReportByStockId(stockId);
			System.out.println("reportListAll: " + reportListAll);
			return reportListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getItemLedgeEntry() {
		try {
			List<Object[]> transactionListAll = transactionDAO.getItemLedgeEntry();
			System.out.println("transactionListAll: " + transactionListAll);
			return transactionListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getItemLedgeEntryByProject(int projectId) {
		try {
			List<Object[]> transactionListAll = transactionDAO.getItemLedgeEntryByProject(projectId);
			System.out.println("transactionListAllProject: " + transactionListAll);
			return transactionListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getItemLedgeEntryByStock(int stockId) {
		try {
			List<Object[]> transactionListAll = transactionDAO.getItemLedgeEntryByStock(stockId);
			System.out.println("transactionListAllStock: " + transactionListAll);
			return transactionListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getAllReportByProject() {
		try {
			List<Object[]> listReportByProjectAll = transactionDAO.getAllReportByProject();
			System.out.println("transactionListAll: " + listReportByProjectAll);
			return listReportByProjectAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getReportByProjectId(int projectId) {
		try {
			List<Object[]> reportListAll = transactionDAO.getReportByProjectId(projectId);
			System.out.println("reportListAll: " + reportListAll);
			return reportListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getReportdetailByProjectIdItemId(int projectId, int itemId, int uoM, String expDate) {
		try {
			List<Object[]> reportListAll = transactionDAO.getReportdetailByProjectIdItemId(projectId, itemId, uoM,
					expDate);
			System.out.println("reportListAll: " + reportListAll);
			return reportListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getReportByItem() {
		try {
			List<Object[]> reportListAll = transactionDAO.getReportByItem();
			System.out.println("reportListAll: " + reportListAll);
			return reportListAll;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}

	@Override
	@Transactional
	public List<Object[]> getReportStockTop(){
		try {
			List<Object[]> reportStockTop = transactionDAO.getReportStockTop();
			System.out.println("reportListAll: " + reportStockTop);
			return reportStockTop;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}
	

	@Override
	@Transactional
	public List<Object[]> getReportProjectTop(){
		try {
			List<Object[]> reportProjectTop = transactionDAO.getReportProjectTop();
			System.out.println("reportListAll: " + reportProjectTop);
			return reportProjectTop;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}
	
	@Override
	@Transactional
	public List<Object[]> getReportInMonth(){
		try {
			List<Object[]> reportInMonth = transactionDAO.getReportInMonth();
			System.out.println("reportListAll: " + reportInMonth);
			return reportInMonth;
		} catch (UnexpectedRollbackException e) {
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}
	
	@Override
	@Transactional
	public List<Object[]> getExpirationSoon() {		
		try {
			List<Object[]> expSoon = transactionDAO.getExpirationSoon();
			System.out.println("reportListAll: " + expSoon);
			return expSoon;
		} catch (UnexpectedRollbackException e) {  
			System.out.println("exception==== " + e.toString());
			return null;
		}
	}
}
