package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.RequestShipment;

public interface RequestShipmentService {
	
	public List<RequestShipment> findAll();

	public RequestShipment findById(int theId);

	public void save(RequestShipment theRequestShipment);

	public void update(RequestShipment theRequestShipment);

	public int findNewCode();

	public void deleteById(int theId);
	
	public String findLastCode() ;
	
	public List<RequestShipment> findRequestVerified(int stage);
}
