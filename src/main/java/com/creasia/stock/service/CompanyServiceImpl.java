package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.CompanyDAO;
import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.ItemCategory;

@Service
public class CompanyServiceImpl implements CompanyService {

	private CompanyDAO companyDAO;
	
	@Autowired
	public CompanyServiceImpl(CompanyDAO theCompanyDAO) {
		companyDAO = theCompanyDAO;
	}
	
	@Override
	@Transactional
	public List<Company> findAll() {

		return companyDAO.findAll();
	}

	@Override
	@Transactional
	public Company findById(int theId) {
		
		return companyDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Company theCompany) {
		//System.out.println("the company: " + theCompany);
		companyDAO.save(theCompany);
	}

	@Override
	@Transactional
	public void update(Company theCompany) {
		//System.out.println("the company: " + theCompany);
		companyDAO.update(theCompany);
	}
	
	@Override
	@Transactional
	public void deleteById(int theId) {

		companyDAO.delete(theId);
	}
	
	@Override
	@Transactional
	public int findNewCode() {
		return companyDAO.findNewCode();
	}

}
