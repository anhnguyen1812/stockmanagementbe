package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.User;

public interface UserService {

	public List<User> findAll();
	
	public User findById(long theId);

	public User findByUsenamePassword(int theId);

	public void save(User theUser);
	
	public void update(User theUser);
	
	public List<User> findUserPost();
	
	public List<User> findByEmployeeId(int theEmployeeId) ;
}
