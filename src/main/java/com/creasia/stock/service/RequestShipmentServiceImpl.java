package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.RequestShipmentDAO;
import com.creasia.stock.entity.RequestShipment;

@Service
public class RequestShipmentServiceImpl implements RequestShipmentService {

	private RequestShipmentDAO RequestShipmentDAO;
	
	@Autowired
	public RequestShipmentServiceImpl(RequestShipmentDAO theRequestShipmentDAO) {
		RequestShipmentDAO = theRequestShipmentDAO;
	}
	
	@Override
	@Transactional
	public List<RequestShipment> findAll() {
		// TODO Auto-generated method stub
		return RequestShipmentDAO.findAll();
	}

	@Override
	@Transactional
	public List<RequestShipment> findRequestVerified(int stage){
		return RequestShipmentDAO.findRequestVerified(stage);
	}
	
	@Override
	@Transactional
	public RequestShipment findById(int theId) {
		// TODO Auto-generated method stub
		return RequestShipmentDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(RequestShipment theRequestShipment) {
		// TODO Auto-generated method stub
		RequestShipmentDAO.save(theRequestShipment);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void update(RequestShipment theRequestShipment) {
		// TODO Auto-generated method stub
		RequestShipmentDAO.update(theRequestShipment);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return RequestShipmentDAO.findNewCode();
	}

	@Override
	@Transactional
	public String findLastCode() {
		return RequestShipmentDAO.findLastCode();
	}
	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		RequestShipmentDAO.delete(theId);

	}
}
