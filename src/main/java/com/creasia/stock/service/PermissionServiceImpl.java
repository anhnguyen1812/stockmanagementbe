package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.PermissionDAO;
import com.creasia.stock.entity.Permission;

@Service
public class PermissionServiceImpl implements PermissionService {

	private PermissionDAO permissionDAO;
	
	@Autowired
	public PermissionServiceImpl(PermissionDAO thePermissionDAO) {
	
		permissionDAO = thePermissionDAO;
	}
	
	
	@Override
	@Transactional
	public List<Permission> findAll() {

		return permissionDAO.findAll();
	}

	@Override
	@Transactional
	public Permission findById(int theId) {
		
		return permissionDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Permission thePermission) {

		permissionDAO.save(thePermission);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {

		permissionDAO.delete(theId);
	}

	@Override
	@Transactional
	public void update(Permission thePermission) {
		//System.out.println("the company: " + theCustomer);
		permissionDAO.update(thePermission);
	}
	
	@Override
	@Transactional
	public int findNewCode() {
		return permissionDAO.findNewCode();
	}
}



