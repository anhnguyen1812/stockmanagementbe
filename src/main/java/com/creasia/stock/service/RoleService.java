package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Role;

public interface RoleService {
	public List<Role> findAll();
}
