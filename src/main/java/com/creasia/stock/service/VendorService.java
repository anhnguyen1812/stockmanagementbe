package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Vendor;

public interface VendorService {

	public List<Vendor> findAll();
	
	public Vendor findById(int theId);
	
	public void save(Vendor theVendor);
	
	public void update(Vendor theVendor);
	
	public void deleteById(int theId);
	
	public int findNewCode() ;
}
