package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.UoM;


public interface UomService {

	public List<UoM> findAll();
	
	public UoM findById(int theId);
	public List<UoM> findByName(String name);
	public void save(UoM theUoM);
	
	public void deleteById(int theId);
}
