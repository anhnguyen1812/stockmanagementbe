package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ReceiptNoteDAO;
import com.creasia.stock.entity.ReceiptNote;

@Service
public class ReceiptNoteServiceImpl implements ReceiptNoteService {

	private ReceiptNoteDAO receiptNoteDAO;
	
	@Autowired
	public ReceiptNoteServiceImpl(ReceiptNoteDAO theReceiptNoteDAO) {
		receiptNoteDAO = theReceiptNoteDAO;
	}
	
	@Override
	@Transactional
	public List<ReceiptNote> findAll() {
		// TODO Auto-generated method stub
		return receiptNoteDAO.findAll();
	}

	@Override
	@Transactional
	public long countReceiptNotes(){
		System.out.println("itemDAO.countItem():: " + receiptNoteDAO.countReceiptNotes());
		return receiptNoteDAO.countReceiptNotes();
	}
	
	@Override
	@Transactional
	public ReceiptNote findById(int theId) {
		// TODO Auto-generated method stub
		return receiptNoteDAO.findById(theId);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void save(ReceiptNote theReceiptNote) {
		// TODO Auto-generated method stub
		receiptNoteDAO.save(theReceiptNote);
		

	}

	@Override
	@Transactional
	public void update(ReceiptNote theReceiptNote) {
		// TODO Auto-generated method stub
		receiptNoteDAO.update(theReceiptNote);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return receiptNoteDAO.findNewCode();
	}

	@Override
	@Transactional
	public String findLastCode() {
		return receiptNoteDAO.findLastCode();
	}
	
	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		receiptNoteDAO.delete(theId);

	}

}
