package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ItemDAO;
import com.creasia.stock.entity.Item;

@Service
public class ItemServiceImpl implements ItemService {

	private ItemDAO itemDAO;
	
	@Autowired
	public ItemServiceImpl(ItemDAO theItemDAO) {
		itemDAO = theItemDAO;
	}
	
	@Override
	@Transactional
	public List<Item> findAll() {

		return itemDAO.findAll();
	}

	@Override
	@Transactional
	public long countItem() {
		System.out.println("itemDAO.countItem():: " + itemDAO.countItem());
		return itemDAO.countItem();
	}
	
	@Override
	@Transactional
	public Item findById(int theId) {
		return itemDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Item theItem) {
		itemDAO.save(theItem);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		itemDAO.delete(theId);
	}

	@Override
	@Transactional
	public int findNewCode() {
		return itemDAO.findNewCode();
	}
	
	@Override
	@Transactional
	public List<Item> findByNameAndUoM(String name, int uomId) {
		return itemDAO.findByNameAndUoM(name, uomId);
	}
}
