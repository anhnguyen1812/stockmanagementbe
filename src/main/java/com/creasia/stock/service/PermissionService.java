package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Permission;

public interface PermissionService {

	public List<Permission> findAll();

	public Permission findById(int theId);

	public void save(Permission thePermission);

	public void update(Permission thePermission);

	public void deleteById(int theId);

	public int findNewCode();
}
