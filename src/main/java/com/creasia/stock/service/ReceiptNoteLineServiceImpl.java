package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ReceiptNoteLineDAO;
import com.creasia.stock.entity.ReceiptNoteLine;

@Service
public class ReceiptNoteLineServiceImpl implements ReceiptNoteLineService {

	private ReceiptNoteLineDAO receiptNoteLineDAO;

	@Autowired
	public ReceiptNoteLineServiceImpl(ReceiptNoteLineDAO theReceiptNoteLineDAO) {
		receiptNoteLineDAO = theReceiptNoteLineDAO;
	}

	@Override
	@Transactional
	public List<ReceiptNoteLine> findAll() {
		// TODO Auto-generated method stub
		return receiptNoteLineDAO.findAll();
	}

	@Override
	@Transactional
	public ReceiptNoteLine findById(int theId) {
		// TODO Auto-generated method stub
		return receiptNoteLineDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(ReceiptNoteLine theReceiptNoteLine) {
		// TODO Auto-generated method stub
		receiptNoteLineDAO.save(theReceiptNoteLine);

	}

	@Override
	@Transactional
	public void update(ReceiptNoteLine theReceiptNoteLine) {
		// TODO Auto-generated method stub
		receiptNoteLineDAO.update(theReceiptNoteLine);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return receiptNoteLineDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		receiptNoteLineDAO.delete(theId);

	}

	@Override
	@Transactional
	public List<ReceiptNoteLine> findByReceiptNoteId(int theProvinceId) {

		return receiptNoteLineDAO.findByReceiptNoteId(theProvinceId);
	}
}
