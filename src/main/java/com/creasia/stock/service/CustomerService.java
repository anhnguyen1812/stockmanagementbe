package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Customer;

public interface CustomerService {

	public List<Customer> findAll();
	
	public Customer findById(int theId);
	
	public void update(Customer theCustomer);
	
	public void save(Customer theCustomer);
	
	public void deleteById(int theId);
	
	public int findNewCode() ;
}
