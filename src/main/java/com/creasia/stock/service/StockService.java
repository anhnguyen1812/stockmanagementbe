package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Stock;

public interface StockService {

	public List<Stock> findAll();
	
	public Stock findById(int theId);
	
	public long countStock();
	
	public void save(Stock theStock);
	
	public void update(Stock theStock);
	
	public void deleteById(int theId);
	
	public int findNewCode(String theHeaderCode) ;
}
