package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.RequestShipmentLineDAO;
import com.creasia.stock.entity.RequestShipmentLine;

@Service
public class RequestShipmentLineServiceImpl implements RequestShipmentLineService {

	private RequestShipmentLineDAO requestShipmentLineDAO;

	@Autowired
	public RequestShipmentLineServiceImpl(RequestShipmentLineDAO theRequestShipmentLineDAO) {
		requestShipmentLineDAO = theRequestShipmentLineDAO;
	}

	@Override
	@Transactional
	public List<RequestShipmentLine> findAll() {
		// TODO Auto-generated method stub
		return requestShipmentLineDAO.findAll();
	}

	@Override
	@Transactional
	public RequestShipmentLine findById(int theId) {
		// TODO Auto-generated method stub
		return requestShipmentLineDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(RequestShipmentLine theRequestShipmentLine) {
		// TODO Auto-generated method stub
		requestShipmentLineDAO.save(theRequestShipmentLine);

	}

	@Override
	@Transactional
	public void update(RequestShipmentLine theRequestShipmentLine) {
		// TODO Auto-generated method stub
		requestShipmentLineDAO.update(theRequestShipmentLine);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return requestShipmentLineDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		requestShipmentLineDAO.delete(theId);

	}

	@Override
	@Transactional
	public List<RequestShipmentLine> findByRequestShipmentId(int theProvinceId) {

		return requestShipmentLineDAO.findByRequestShipmentId(theProvinceId);
	}
}
