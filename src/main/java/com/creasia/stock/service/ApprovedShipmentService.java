package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ApprovedShipment;

public interface ApprovedShipmentService {

	public List<ApprovedShipment> findAll();

	public List<ApprovedShipment> findApprovedShipment();

//	public List<ApprovedShipment> findPostedShipment();

	public ApprovedShipment findById(int theId);

	public List<ApprovedShipment> findByRequestShipmentId(int theRequestId);

	public void save(ApprovedShipment theApprovedShipment);

	public void update(ApprovedShipment theApprovedShipment);

	public int findNewCode();

	public String findLastCode();

	public void deleteById(int theId);
}
