package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Report;
import com.creasia.stock.entity.Transaction;

public interface TransactionService {

	public List<Transaction> findAll();

	public long countTransactionInToday();

	public Transaction findById(int theId);

	public Transaction findByNoteCode(String theNoteCode);
	
	public List<Object[]> getExpirationSoon();
	
	public List<Object[]> getReportInMonth();

	public List<Object[]> getReportInStock();

	public List<Object[]> getReportByProject();

	public List<Object[]> getReportStockTop();

	public List<Object[]> getReportProjectTop();

	public List<Report> getReportByProjectIdStockIdExpDate(int projectId, int stockId, int itemId, int uomId);

	public void save(Transaction theTransaction);

	public void update(Transaction theTransaction);

	public int findNewCode();

	public void deleteById(int theId);

	public long findInventory(int stockId, int itemId, int uomId);

	public long findInventoryByItemAndExpDate(int projectId, int stockId, int itemId, int uomId, String expDate);

	public long findInventoryByStockAndProject(int stockId, int projectId, int itemId, int uomId);

	public List<Object[]> getReportAll();

	public List<Object[]> getReportByStockId(int stockId);

	public List<Object[]> getItemLedgeEntry();

	public List<Object[]> getItemLedgeEntryByProject(int projectId);

	public List<Object[]> getItemLedgeEntryByStock(int stockId);

	public List<Object[]> getAllReportByProject();

	public List<Object[]> getReportByProjectId(int projectId);

	public List<Object[]> getReportdetailByProjectIdItemId(int projectId, int itemId, int uoM, String expDate);

	List<Object[]> getReportByItem();

}
