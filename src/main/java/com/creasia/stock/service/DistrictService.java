package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Province;

public interface DistrictService {

	public List<District> findAll();
	
	public District findById(int theId);
	
	public void save(District theDistrict);
	
	public void deleteById(int theId);
	
	public List<District> findDistrictByProvinceId(int theProvinceId);
}

