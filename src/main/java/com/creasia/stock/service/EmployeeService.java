package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Employee;

public interface EmployeeService {

	public List<Employee> findAll();

	public Employee findById(int theId);

	public void save(Employee theEmployee);

	public void update(Employee theEmployee);

	public int findNewCode();

	public void deleteById(int theId);
}
