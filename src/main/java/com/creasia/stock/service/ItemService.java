package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Item;

public interface ItemService {

	public List<Item> findAll();
	
	public Item findById(int theId);
	
	public long countItem();
	
	public int findNewCode();
	
	public void save(Item theItem);
	
	public void deleteById(int theId);
	
	public List<Item> findByNameAndUoM(String name, int uomId);
}
