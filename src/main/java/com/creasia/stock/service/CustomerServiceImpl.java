package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.CustomerDAO;
import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	private CustomerDAO customerDAO;
	
	@Autowired
	public CustomerServiceImpl(CustomerDAO theCustomerDAO) {
		customerDAO = theCustomerDAO;
	}
	
	@Override
	@Transactional
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		return customerDAO.findAll();
	}

	@Override
	@Transactional
	public Customer findById(int theId) {
		// TODO Auto-generated method stub
		return customerDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Customer theCustomer) {
		// TODO Auto-generated method stub
		System.out.println("current Service : " + theCustomer);
		customerDAO.save(theCustomer);

	}
	
	@Override
	@Transactional
	public void update(Customer theCustomer) {
		//System.out.println("the company: " + theCustomer);
		customerDAO.update(theCustomer);
	}
	

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		customerDAO.delete(theId);
	}

	@Override
	@Transactional
	public int findNewCode() {
		return customerDAO.findNewCode();
	}
}
