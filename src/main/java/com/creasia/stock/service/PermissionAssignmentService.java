package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;

public interface PermissionAssignmentService {

	public List<PermissionAssignment> findAll();

	public PermissionAssignment findById(int theId);

	public void save(PermissionAssignment thePermissionAssignment);
	
	public List<User> getUserByStockId(int stockId);

	public void update(PermissionAssignment per);
	
	public List<PermissionAssignment> getPermissionByUser(long userId);

	public List<Employee> getUserAdmin();

	public Employee getUserManager(int userId);
}
