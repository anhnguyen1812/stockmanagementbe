package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ReceiptNote;

public interface ReceiptNoteService {
	
	public List<ReceiptNote> findAll();
	
	public long countReceiptNotes();

	public ReceiptNote findById(int theId);

	public void save(ReceiptNote theReceiptNote);

	public void update(ReceiptNote theReceiptNote);

	public int findNewCode();

	public void deleteById(int theId);
	
	public String findLastCode();
}
