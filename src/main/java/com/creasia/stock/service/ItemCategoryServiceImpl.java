package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ItemCategoryDAO;
import com.creasia.stock.entity.ItemCategory;

@Service
public class ItemCategoryServiceImpl implements ItemCategoryService{
	
	private ItemCategoryDAO itemCategoryDAO;
	
	@Autowired
	public ItemCategoryServiceImpl(ItemCategoryDAO theItemCategoryDAO) {
		itemCategoryDAO = theItemCategoryDAO;
	}
	
	@Override
	@Transactional
	public List<ItemCategory> findAll(){
	 
		return itemCategoryDAO.findAll();
	}	
	
	@Override
	@Transactional
	public ItemCategory findById(int theId) {
		
		return itemCategoryDAO.findById(theId);
	}
	
	@Override
	@Transactional
	public List<ItemCategory> findByName(String name) {
		
		return itemCategoryDAO.findByName(name);
	}
	
	
	@Override
	@Transactional
	public int findNewCode() {
		return itemCategoryDAO.findNewCode();
	}
	
	@Override
	@Transactional
	public void save(ItemCategory theItemCategory) {
		itemCategoryDAO.save(theItemCategory);
	}
	
	@Override
	@Transactional
	public void update(ItemCategory theItemCategory) {
		itemCategoryDAO.update(theItemCategory);
	}
	
	@Override
	@Transactional
	public void deleteById(int theId) {
		
		itemCategoryDAO.delete(theId);
	}

}
