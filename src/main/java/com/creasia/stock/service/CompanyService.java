package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.ItemCategory;

public interface CompanyService {

	public List<Company> findAll();
	
	public Company findById(int theId);
	
	public void save(Company theCompany);
	
	public void update(Company theCompany);
	
	public int findNewCode() ;
	
	public void deleteById(int theId);
}
