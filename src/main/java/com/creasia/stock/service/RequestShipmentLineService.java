package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.RequestShipmentLine;

public interface RequestShipmentLineService {
	
	public List<RequestShipmentLine> findAll();
  
	public RequestShipmentLine findById(int theId);

	public void save(RequestShipmentLine theRequestShipmentLine);

	public void update(RequestShipmentLine theRequestShipmentLine);

	public int findNewCode();

	public void deleteById(int theId);
	
	public List<RequestShipmentLine> findByRequestShipmentId(int theRequestShipmentId);
}

