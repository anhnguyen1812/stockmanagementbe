package com.creasia.stock.service;

import java.util.List;

import com.creasia.stock.entity.ApprovedShipmentLine;

public interface ApprovedShipmentLineService {

	public List<ApprovedShipmentLine> findAll();

	public ApprovedShipmentLine findById(int theId);

	public void save(ApprovedShipmentLine theApprovedShipmentLine);

	public void update(ApprovedShipmentLine theApprovedShipmentLine);

	public int findNewCode();

	public void deleteById(int theId);
	
	public List<ApprovedShipmentLine> findByApprovedShipmentId(int theProvinceId);
}
