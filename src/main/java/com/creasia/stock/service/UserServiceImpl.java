package com.creasia.stock.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.UsersDAO;
import com.creasia.stock.entity.Role;
import com.creasia.stock.entity.User;

@Service
public class UserServiceImpl implements UserService {

	private UsersDAO usersDAO;
	
	@Autowired
	public UserServiceImpl(UsersDAO theUsersDAO) {
		usersDAO = theUsersDAO;
	}
	
	@Override
	@Transactional
	public List<User> findAll() {
		try {
			return usersDAO.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error :: :)) --- " + e.toString());
			return null;
		}
		
	}

	@Override
	@Transactional
	public User findByUsenamePassword(int theId) {
		return usersDAO.findByUsenamePassword(theId);
	}
	
	@Override
	@Transactional
	public List<User> findUserPost(){
		List<User> users = usersDAO.findAll();
		if (users == null) {
			return null;
		}
		List<User> listUser = new ArrayList<User>();
		for (User u: users) {
			for (Role r : u.getRoles()) {
				if (r.getId() == 3) {
					listUser.add(u);
				}
			}
		}
		return listUser;
	}
	
	@Override
	@Transactional
	public List<User> findByEmployeeId(int theEmployeeId) {
		try {
			return usersDAO.findByEmployeeId(theEmployeeId);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	@Transactional
	public User findById(long theId) {
		return usersDAO.findById(theId);
	}
	
	@Override
	@Transactional
	public void save(User theUoM) {
		usersDAO.save(theUoM);
	}
	
	@Override
	@Transactional
	public void update(User theUser) {
		try {
			usersDAO.update(theUser);
		} catch (Exception e) {
			System.out.println("exception :" + e.toString());
		}
	}
}
