package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.DepartmentDAO;
import com.creasia.stock.entity.Department;
import com.creasia.stock.entity.District;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentDAO departmentDAO;
	
	@Autowired
	public DepartmentServiceImpl(DepartmentDAO theDepartmentDAO) {
	
		departmentDAO = theDepartmentDAO;
	}
	
	
	@Override
	@Transactional
	public List<Department> findAll() {

		return departmentDAO.findAll();
	}

	@Override
	@Transactional
	public List<Department> findDepartmentByCompanyId(int companyId) {
		try {
			return departmentDAO.findDepartmentByCompanyId(companyId);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error: " + e.toString());
		}
		return departmentDAO.findDepartmentByCompanyId(companyId);
	}
	
	@Override
	@Transactional
	public Department findById(int theId) {
		
		return departmentDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Department theDepartment) {

		departmentDAO.save(theDepartment);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {

		departmentDAO.delete(theId);
	}

	@Override
	@Transactional
	public void update(Department theDepartment) {
		//System.out.println("the company: " + theCustomer);
		departmentDAO.update(theDepartment);
	}
	
	@Override
	@Transactional
	public int findNewCode() {
		return departmentDAO.findNewCode();
	}
}



