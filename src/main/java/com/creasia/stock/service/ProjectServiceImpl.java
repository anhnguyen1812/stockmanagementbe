package com.creasia.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasia.stock.dao.ProjectDAO;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.RequestShipmentLine;

@Service
public class ProjectServiceImpl implements ProjectService {

	private ProjectDAO projectDAO;
	
	@Autowired
	public ProjectServiceImpl(ProjectDAO theProjectDAO) {
		projectDAO = theProjectDAO;
	}
	
	@Override
	@Transactional
	public List<Project> findAll() {
		// TODO Auto-generated method stub
		return projectDAO.findAll();
	}
	
	@Override
	@Transactional
	public long findByCode(String theCode) {
		return projectDAO.findByCode(theCode);
	}
	
	@Override
	@Transactional
	public List<Project>  findProjectByPmId(int pmId){
		return projectDAO.findProjectByPmId(pmId);
	}
	
	@Override
	@Transactional
	public long countProject() {
		System.out.println("itemDAO.countItem():: " + projectDAO.countProject());
		return projectDAO.countProject();
	}
	
	@Override
	@Transactional
	public Project findById(int theId) {
		// TODO Auto-generated method stub
		return projectDAO.findById(theId);
	}

	@Override
	@Transactional
	public void save(Project theProject) {
		// TODO Auto-generated method stub
		projectDAO.save(theProject);
		

	}

	@Override
	@Transactional
	public void update(Project theProject) {
		// TODO Auto-generated method stub
		projectDAO.update(theProject);

	}

	@Override
	@Transactional
	public int findNewCode() {
		// TODO Auto-generated method stub
		return projectDAO.findNewCode();
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		// TODO Auto-generated method stub
		projectDAO.delete(theId);

	}

}
