package com.creasia.stock.rest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.ReceiptNoteLine;
import com.creasia.stock.entity.RequestShipment;
import com.creasia.stock.entity.RequestShipmentLine;
import com.creasia.stock.entity.ShipmentNote;
import com.creasia.stock.entity.User;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.RequestShipmentLineService;
import com.creasia.stock.service.RequestShipmentService;
import com.creasia.stock.service.ShipmentNoteLineService;
import com.creasia.stock.service.ShipmentNoteLineServiceImpl;
import com.creasia.stock.service.ShipmentNoteService;
import com.creasia.stock.util.Email;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class RequestShipmentRestController {

	private RequestShipmentService requestShipmentService;
	private RequestShipmentLineService requestShipmentLineService;
	private PermissionAssignmentService permissionAssignmentService;
	private Email email = new Email();

	@Autowired
	public RequestShipmentRestController(RequestShipmentService theRequestShipmentService,
			RequestShipmentLineService theRequestShipmentLineService,
			PermissionAssignmentService thePermissionAssignmentService) {
		requestShipmentService = theRequestShipmentService;
		requestShipmentLineService = theRequestShipmentLineService;
		permissionAssignmentService = thePermissionAssignmentService;
	}

	@GetMapping("/requestShipments")
	public Entities findAll() {
		try {
			List<RequestShipment> requestShipmentList = requestShipmentService.findAll();
			if (requestShipmentList == null) {
				return new Entities(1, requestShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
			} else {
				return new Entities(0, requestShipmentList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "SERVER ERROR");
		}
	}

	@GetMapping("/requestShipments/{requestShipmentId}")
	public Entities getRequestShipment(@PathVariable int requestShipmentId) {

		RequestShipment requestShipment = requestShipmentService.findById(requestShipmentId);
		if (requestShipment == null) {
			return new Entities(1, requestShipment, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, requestShipment, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/requestShipments/getRequestShipmentByStage")
	public Entities getRequestVerified(@RequestParam("stage") int stage) {

		try {
			List<RequestShipment> requestShipmentList = requestShipmentService.findRequestVerified(stage);
			if (requestShipmentList == null) {
				return new Entities(1, requestShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());

			} else {
				return new Entities(0, requestShipmentList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "SERVER ERROR");
		}

	}

	@PostMapping("/requestShipments")
	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVE') or hasRole('POST') or hasRole('USER') or hasRole('VERIFY')")
	public Entities addRequestShipment(@RequestBody RequestShipment theRequestShipment) {

		if (theRequestShipment.getCreateBy() == null || theRequestShipment.getProject() == null) {
			// name == null
			System.out.println("theRequestShipment" + theRequestShipment);
			return new Entities(1, theRequestShipment, "Body Invalid");
		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theRequestShipment.setCreatedAt(dtf.format(now));
		theRequestShipment.setApproved(-1);
		try {
			int totalQuantity = 0;
			List<RequestShipmentLine> theRequestShipmentLine = theRequestShipment.getRequestShipmentLines();

			for (int i = 0; i < theRequestShipmentLine.size(); i++) {
				RequestShipmentLine rsLine = theRequestShipmentLine.get(i);
				rsLine.setOutStdQty(rsLine.getQuantity());
				rsLine.setShiptedQty(0);
				theRequestShipmentLine.set(i, rsLine);
				totalQuantity += rsLine.getQuantity();
			}
			if (totalQuantity == 0) {
				return new Entities(1, theRequestShipment, "No item choosed");
			} else {
				theRequestShipment.setTotalQuantity(totalQuantity);
			}

			theRequestShipment.setRequestShipmentLines(theRequestShipmentLine);
			System.out.println("theRequestShipment : " + theRequestShipment);
			requestShipmentService.save(theRequestShipment);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theRequestShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theRequestShipment, "Fail to server");
		}
		Entities entities = new Entities(0, theRequestShipment, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/requestShipments")
	@PreAuthorize("hasRole('ADMIN') or hasRole('VERIFY')")
	public Entities updateRequestShipment(@RequestBody RequestShipment theRequestShipment) {
		if (theRequestShipment.getCreateBy() == null || theRequestShipment.getProject() == null) {
			return new Entities(1, theRequestShipment, "Body Invalid");
		}
		try {
			int id = theRequestShipment.getId();
			RequestShipment theRequest = requestShipmentService.findById(id);
			theRequestShipment.setCreatedAt(theRequest.getCreatedAt());
			int totalQuantity = 0;
			for (RequestShipmentLine rnLine : theRequestShipment.getRequestShipmentLines()) {
				totalQuantity += rnLine.getQuantity();
			}
			if (totalQuantity == 0) {
				return new Entities(1, theRequestShipment, "No item choosed");
			} else {
				theRequestShipment.setTotalQuantity(totalQuantity);
			}
			requestShipmentService.update(theRequestShipment);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theRequestShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theRequestShipment, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theRequestShipment, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/verifyRequestShipment")
	@PreAuthorize("hasRole('ADMIN') or hasRole('VERIFY')")
	public Entities verifyRequestShipment(@RequestBody RequestShipment theRequestShipment) {
		if (theRequestShipment.getVerifiedBy() == null || theRequestShipment.getId() == 0) {
			return new Entities(1, theRequestShipment, "Body Invalid");
		}
		try {
			int id = theRequestShipment.getId();
			RequestShipment theRequest = requestShipmentService.findById(id);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			theRequest.setVerifiedAt(dtf.format(now));
			theRequest.setVerifiedBy(theRequestShipment.getVerifiedBy());
			theRequest.setApproved(0);
			requestShipmentService.update(theRequest);
			Entities entities = new Entities(0, theRequest, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theRequestShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theRequestShipment, "Fail to server");
		}
	}

	@GetMapping("/getNewRequestShipmentCode")
	public Entities getNewRequestShipmentCode() {
		String newCode;
		String lastCode = requestShipmentService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if (lastCode == "") {
			newCode = "RS/" + year + "/000001";
		} else {

			String headerCode = lastCode.substring(4, 6);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(7);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				int tempInt = newMainCode.length();
				if (tempInt == 1) {
					st_0 = "00000";
				}
				if (tempInt == 2) {
					st_0 = "0000";
				}
				if (tempInt == 3) {
					st_0 = "000";
				}
				if (tempInt == 4) {
					st_0 = "00";
				}
				if (tempInt == 5) {
					st_0 = "0";
				}
				if (tempInt == 6) {
					st_0 = "";
				}
				newCode = "RSN/" + year + "/" + st_0 + newMainCode;
			} else {
				newCode = "RSN/" + year + "/000001";
			}
			// System.out.println("newCode "+ newCode);
		}
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("requestShipmentCode", newCode);
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	@GetMapping(value = "/sendemailVerify")
	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVE') or hasRole('POST') or hasRole('USER') or hasRole('VERIFY')")
	public Entities sendemailVerify(@RequestParam("requestId") int requestId)
			throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		try {

			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
			System.out.println("listReceive: " + listReceive);
			RequestShipment rs = requestShipmentService.findById(requestId);
			System.out.println("RS: " + rs);
//			Employee manager = permissionAssignmentService.getUserManager(rs.getCreateBy().getId());
			Employee emp = rs.getCreateBy();
			if (emp == null) {
				return new Entities(1, 0, "No Create By");
			}
//			Employee manager = permissionAssignmentService.getUserManager(rs.getCreateBy().getId());
			Employee manager = emp.getManager();
			System.out.println("manager: " + manager);
			if (manager == null) {
				manager = rs.getCreateBy();
			}
			if (listReceive.indexOf(manager) != -1) {
				listReceive.remove(manager);
			}
			String requestCode = requestShipmentService.findById(requestId).getCode();
			String name = manager.getName();
			// send email
			String subject = "Verify Request Shipment";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;'> Request Shipment <a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/requestShipment\"> "
					+ requestCode + "</a> has been created.</p>"
					+ "<p style=' font-family: Arial;' > Please go to system to check and Verify it! </p> <br>"
					+ "<p style=' font-family: Arial;' >Thanks & regards!</p>"
					+ "<p style=' font-family: Arial;' >Tech Teams</p>";
			List<String> emails = new ArrayList<String>();
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}

			System.out.println("subject: " + subject + " emails " + emails);
			email.sendmail(subject, "<h4 style='font-family: Arial;'>" + content1 + name + ", </h4>" + content2, emails,
					manager.getEmail());
			entities = new Entities(0, emails, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			return new Entities(1, 0, "Error send email");
		}
	}
}
