package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.service.TransactionLineService;

@RestController
@RequestMapping("api")
public class TransactionLineRestController {

	private TransactionLineService transactionLineService;

	@Autowired
	public TransactionLineRestController(TransactionLineService theTransactionLineService) {
		transactionLineService = theTransactionLineService;
	}

	@GetMapping("/transactionLines")
	public Entities findAll() {
		List<TransactionLine> transactionLineList = transactionLineService.findAll();
		if (transactionLineList == null) {
			return new Entities(1, transactionLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, transactionLineList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/transactionLines/{transactionLineId}")
	public Entities getTransactionLine(@PathVariable int transactionLineId) {
		TransactionLine transactionLine = transactionLineService.findById(transactionLineId);
		if (transactionLine == null) {
			return new Entities(1, transactionLine, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, transactionLine, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/transactionLines")
	public Entities addTransactionLine(@RequestBody TransactionLine theTransactionLine) {
		if (theTransactionLine.getItem() == null || theTransactionLine.getQuantity() < 1) {
			return new Entities(1, theTransactionLine, "Body Invalid");
		}
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
//		LocalDateTime now = LocalDateTime.now();
//		theTransactionLine.setCreatedAt(dtf.format(now));
		try {
			transactionLineService.save(theTransactionLine);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theTransactionLine, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theTransactionLine, "Fail to server");
		}
		Entities entities = new Entities(0, theTransactionLine, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/transactionLines")
	public Entities updateTransactionLine(@RequestBody TransactionLine theTransactionLine) {
		if (theTransactionLine.getItem() == null || theTransactionLine.getQuantity() < 1) {
			return new Entities(1, theTransactionLine, "Body Invalid");
		}
		try {
			transactionLineService.update(theTransactionLine);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theTransactionLine, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theTransactionLine, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theTransactionLine, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/transactionLines/{transactionLineId}")
	public Entities deleteTransactionLine(@PathVariable int transactionLineId) {
		TransactionLine transactionLine = transactionLineService.findById(transactionLineId);
		if (transactionLine == null) {
			return new Entities(1, transactionLine, "Error Delete");
		}
		return findAll();
	}

	@GetMapping("/findLineByTransactionId/{idTransaction}")
	public Entities findByTransactionId(@PathVariable int idTransaction) {
		System.out.println("theTransaction : " + idTransaction);
		List<TransactionLine> transactionLineList = transactionLineService.findByTransactionId(idTransaction);
		if (transactionLineList == null) {
			return new Entities(1, transactionLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, transactionLineList, HttpStatus.OK.getReasonPhrase());
		}
	}
}
