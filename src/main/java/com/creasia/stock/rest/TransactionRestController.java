package com.creasia.stock.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.Report;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.entity.UoM;
import com.creasia.stock.export.ExcelExporter;
import com.creasia.stock.service.TransactionService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class TransactionRestController {

	private TransactionService transactionService;

	@Autowired
	public TransactionRestController(TransactionService theTransactionService) {
		transactionService = theTransactionService;
	}

	@GetMapping("/transactions")
	public Entities findAll() {

		List<Transaction> transactionList = transactionService.findAll();
		if (transactionList == null) {
			return new Entities(1, transactionList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, transactionList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/countTransactionInToday")
	public Entities countTransactionInToday() {
		long count = transactionService.countTransactionInToday();
		System.out.println("item List:: " + count);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countTransactionInToday", count);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}

	@GetMapping("/transactions/{transactionId}")
	public Entities getTransaction(@PathVariable int transactionId) {

		Transaction transaction = transactionService.findById(transactionId);
		if (transaction == null) {
			return new Entities(1, transaction, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, transaction, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/findByNoteCode")
	public Transaction findByNoteCode(@RequestParam("stockId") String theNoteCode) {
		Transaction transaction = transactionService.findByNoteCode(theNoteCode);
//		System.out.println("transaction== " + transaction);
//		System.out.println("transactionService.find== " + transactionService.findByNoteCode(theNoteCode));
		return transaction;
	}

	@GetMapping("/getInventoryInStock")
	public Entities findInventoryInStock() {
		List<Object[]> resultList = transactionService.getReportInStock();
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		Stock stock = new Stock();
		for (Object[] result : resultList) {
			stock = (Stock) result[0];
			long quantity = ((Number) result[1]).intValue();
			reportList.add(new Report(stock, quantity));
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());

	}

	@GetMapping("/getInventoryByProject")
	public Entities findInventoryByProject() {
		List<Object[]> resultList = transactionService.getReportByProject();
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		Project project = new Project();
		for (Object[] result : resultList) {
			project = (Project) result[0];
			long quantity = ((Number) result[1]).intValue();
			reportList.add(new Report(project, quantity));
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());

	}

	@GetMapping("/getInventoryInStockByItem")
	public Entities findInventoryInStockByItem(@RequestParam("stockId") int stockId, @RequestParam("itemId") int itemId,
			@RequestParam("uomId") int uomId) {
		if (stockId == 0 || itemId == 0 || uomId == 0) {
			return new Entities(1, null, "Not find");
		}
		long inventory = transactionService.findInventory(stockId, itemId, uomId);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("inventoryByItemInStock", inventory);
		System.out.println("inventory=== " + inventory);
		if (inventory == -1) {
			return new Entities(1, mapCode, "Not find");
		} else {
			return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/getInventoryInStockByProjectAndItem")
	public Entities findInventoryInStockByProjectAndItem(@RequestParam("stockId") int stockId,
			@RequestParam("projectId") int projectId, @RequestParam("itemId") int itemId,
			@RequestParam("uomId") int uomId) {
		if (stockId == 0 || itemId == 0 || uomId == 0) {
			return new Entities(1, null, "Not find");
		}
		long inventory = transactionService.findInventoryByStockAndProject(stockId, projectId, itemId, uomId);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("inventoryByItemInStock", inventory);
		System.out.println("inventory=== " + inventory);
		if (inventory == -1) {
			return new Entities(1, mapCode, "Not find");
		} else {
			return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/getReportByProjectIdStockIdExpDate")
	public Entities getReportByProjectIdStockIdExpDate(@RequestParam("stockId") int stockId,
			@RequestParam("projectId") int projectId, @RequestParam("itemId") int itemId,
			@RequestParam("uomId") int uomId) {
		List<Report> resultList = transactionService.getReportByProjectIdStockIdExpDate(stockId, projectId, itemId,
				uomId);
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return null;
		}
		return new Entities(0, resultList, HttpStatus.OK.getReasonPhrase());
	}

	@GetMapping("/getReportAll")
	public Entities getReportAll() {

		List<Object[]> resultList = transactionService.getReportAll();
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		Stock stock = new Stock();
		Item item = new Item();
		UoM uoM = new UoM();
		for (Object[] result : resultList) {
			stock = (Stock) result[0];
			item = (Item) result[1];
			uoM = (UoM) result[2];
			long quantity = ((Number) result[3]).intValue();
			System.out.println("result Report: stock " + stock.getId() + ", item " + item.getName() + ", UoM "
					+ uoM.getName() + ", quantity " + quantity);
			reportList.add(new Report(stock, item, uoM, quantity));
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
	}

	@GetMapping("/getReportItemStock")
	public Entities getReportItemStock() {
		try {
			List<Object[]> resultList = transactionService.getReportAll();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			reportList = reportStockCheck(resultList);
			if (reportList != null) {
				return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
			} else {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
		} catch (Exception e) {
			return new Entities(1, null, e.toString());
		}
	}

	public List<Report> reportStockCheck(List<Object[]> resultList) {
		try {
			List<Report> reportList = new ArrayList<Report>();
			Stock stock = new Stock();
			Item item = new Item();
			UoM uoM = new UoM();
			long qty = 0;
			Stock stock2 = new Stock();
			Item item2 = new Item();
			UoM uoM2 = new UoM();
			long qty2 = 0;
			List<Stock> listStock = new ArrayList<Stock>();
			List<Integer> listQty;
			long totalQty;
			int i = 0;
			int j = 0;
			while (i < resultList.size()) {

				stock = (Stock) resultList.get(i)[0];
				if (!listStock.contains(stock)) {
					listStock.add(stock);
				}
				i++;
			}
			i = 0;
			while (i < resultList.size()) {
				System.out.println("i = " + i);
				listQty = new ArrayList<Integer>();
				for (int x = 0; x < listStock.size(); x++) {
					listQty.add(0);
				}
				item = (Item) resultList.get(i)[1];
				uoM = (UoM) resultList.get(i)[2];
				qty = (long) resultList.get(i)[3];
				stock = (Stock) resultList.get(i)[0];
				if (listStock.indexOf(stock) != -1) {
					listQty.set(listStock.indexOf(stock), (int) qty);
				}
				totalQty = qty;
				j = i + 1;
				while (j < resultList.size()) {
					item2 = (Item) resultList.get(j)[1];
					uoM2 = (UoM) resultList.get(j)[2];
					qty2 = (long) resultList.get(j)[3];
					stock2 = (Stock) resultList.get(j)[0];
					if ((item2 == item) && (uoM2 == uoM)) {
						if (listStock.indexOf(stock2) != -1) {
							listQty.set(listStock.indexOf(stock2), (int) qty2);
							totalQty += qty2;
							System.out.println("listQty = " + listQty);
						}
						System.out.println("qty = " + qty2);
						resultList.remove(j);
					} else {
						j++;
					}
				}
				reportList.add(new Report(item, uoM, listStock, listQty, totalQty));
				i++;
			}
			return reportList;
		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping("/getReportByStockId")
	public Entities getReportByStockId(@RequestParam("stockId") int stockId) {

		List<Object[]> resultList = transactionService.getReportByStockId(stockId);
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		Stock stock = new Stock();
		Item item = new Item();
		UoM uoM = new UoM();
		for (Object[] result : resultList) {
			stock = (Stock) result[0];
			item = (Item) result[1];
			uoM = (UoM) result[2];
			long quantity = ((Number) result[3]).intValue();
			reportList.add(new Report(stock, item, uoM, quantity));
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
	}

	@GetMapping("/getReportByItem")
	public Entities getReportByItem() {
		try {
			List<Object[]> resultList = transactionService.getReportByItem();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			Item item = new Item();
			for (Object[] result : resultList) {
				item = (Item) result[0];
				long quantity = ((Number) result[1]).intValue();
				reportList.add(new Report(item, quantity));
			}
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		} catch (Exception e) {
			return new Entities(1, null, "Error Server");
		}
	}

	@GetMapping("/getReportStockTop")
	public Entities getReportStockTop() {
		try {
			List<Object[]> resultList = transactionService.getReportStockTop();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			Report report;
			Stock stock = new Stock();
			long quantityIn = 0;
			long quantityOut = 0;
			long quantity = 0;
			long quantityJ = 0;
			long quantityInTotal = 0;
			long quantityOutTotal = 0;
			long quantityTotal = 0;
			int i = 0;
			int j = 0;
			while (i < resultList.size()) {
				stock = (Stock) resultList.get(i)[0];
				quantity = ((Number) resultList.get(i)[2]).intValue();
				if (((Number) resultList.get(i)[1]).intValue() == -1) {
					quantityOut = quantity;
					quantityIn = 0;
				} else {
					quantityOut = 0;
					quantityIn = quantity;
				}
				report = new Report(stock, quantityIn, quantityOut, 0);
				j = (int) i + 1;
				while ((j < resultList.size()) && (resultList.get(j)[0] != stock)) {
					j++;
				}
				if (j < resultList.size()) {
					if ((resultList.get(j)[0] == stock)) {
						quantityJ = ((Number) resultList.get(j)[2]).intValue();
						if (((Number) resultList.get(j)[1]).intValue() == -1) {
							report.setQuantityOut(report.getQuantityOut() + quantityJ);

						}
						if (((Number) resultList.get(j)[1]).intValue() == 1) {
							report.setQuantityIn(report.getQuantityIn() + quantityJ);
						}
					}
					resultList.remove(j);
				}
				report.setQuantity(report.getQuantityIn() + report.getQuantityOut());
				quantityInTotal += report.getQuantityIn();
				quantityOutTotal += report.getQuantityOut();
				quantityTotal += report.getQuantity();
				reportList.add(report);
				if (j == (i + 1)) {
					i = j;
				} else {
					i = i + 1;
				}
			}
			Stock stock2 = new Stock();
			report = new Report(stock2, quantityInTotal, quantityOutTotal, quantityTotal);
			reportList.add(report);
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		} catch (Exception e) {
			return new Entities(1, null, "Error Server");
		}
	}

	@GetMapping("/getReportProjectTop")
	public Entities getReportProjectTop() {
		try {
			List<Object[]> resultList = transactionService.getReportProjectTop();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			Report report;
			Project project = new Project();
			long quantityIn = 0;
			long quantityOut = 0;
			long quantity = 0;
			long quantityJ = 0;
			long quantityInTotal = 0;
			long quantityOutTotal = 0;
			long quantityTotal = 0;
			int i = 0;
			int j = 0;
			while (i < resultList.size()) {
				project = (Project) resultList.get(i)[0];
				quantity = ((Number) resultList.get(i)[2]).intValue();
				if (((Number) resultList.get(i)[1]).intValue() == -1) {
					quantityOut = quantity;
					quantityIn = 0;
				} else {
					quantityOut = 0;
					quantityIn = quantity;
				}
				report = new Report(project, quantityIn, quantityOut, 0);
				j = (int) i + 1;
				while ((j < resultList.size()) && (resultList.get(j)[0] != project)) {
					j++;
				}
				if (j < resultList.size()) {
					if ((resultList.get(j)[0] == project)) {
						quantityJ = ((Number) resultList.get(j)[2]).intValue();
						if (((Number) resultList.get(j)[1]).intValue() == -1) {
							report.setQuantityOut(report.getQuantityOut() + quantityJ);

						}
						if (((Number) resultList.get(j)[1]).intValue() == 1) {
							report.setQuantityIn(report.getQuantityIn() + quantityJ);
						}
					}
					resultList.remove(j);
				}
				report.setQuantity(report.getQuantityIn() + report.getQuantityOut());
				reportList.add(report);
				quantityInTotal += report.getQuantityIn();
				quantityOutTotal += report.getQuantityOut();
				quantityTotal += report.getQuantity();
				if (j == (i + 1)) {
					i = j;
				} else {
					i = i + 1;
				}
			}
			Stock stock2 = new Stock();
			report = new Report(stock2, quantityInTotal, quantityOutTotal, quantityTotal);
			reportList.add(report);
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		} catch (Exception e) {
			return new Entities(1, null, "Error Server");
		}
	}
	
	@GetMapping("/getExpirationSoon")
	public Entities getExpirationSoon() {
		try {
			List<Object[]> resultList = transactionService.getExpirationSoon();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			Report report;
			Stock stock = new Stock();
			Item item = new Item();
			String expDate = "";
			long quantity = 0;
			int i = 0;
			while (i < resultList.size()) {
				
				stock = (Stock) resultList.get(i)[0];
				item = (Item) resultList.get(i)[1];
				expDate = (String) resultList.get(i)[2];
				quantity = ((Number) resultList.get(i)[3]).intValue();
				report = new Report(stock, item, expDate, quantity);
				reportList.add(report);
				i++;
			}
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		}catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
	}

	@GetMapping("/getReportLastMonth")
	public Entities getReportInMonth() {
		try {
			LocalDate now = LocalDate.now();
			LocalDate dateIndex = now.plusDays(-30);

			List<Object[]> resultList = transactionService.getReportInMonth();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			int dayIndex = dateIndex.getDayOfMonth();
			int monthIndex = dateIndex.getMonth().getValue();
			System.out.println("result Report firstDay: " + dayIndex);
			int monthLast = now.getMonth().getValue();
			int year = now.getYear();
			int dayOfMonthLast = 30;
			if (monthLast == 1) {
				year = year - 1;
				monthLast = 12;
			} else if (monthLast > 1) {
				monthLast = monthLast - 1;
			}
			if (monthLast == 2) {
				LocalDate localDate1 = LocalDate.of(year, monthLast, 1);
				if (localDate1.isLeapYear()) {
					dayOfMonthLast = 29;
				} else {
					dayOfMonthLast = 28;
				}
			} else if (monthLast == 1 || monthLast == 3 || monthLast == 5 || monthLast == 7 || monthLast == 8
					|| monthLast == 10 || monthLast == 12) {
				dayOfMonthLast = 31;
			}
			List<Report> reportList = new ArrayList<Report>();
			Report report;
			String postedDate = "";
			int day;
			int month;
			long quantityIn = 0;
			long quantityOut = 0;
			long quantity = 0;
			long quantityJ = 0;
			int i = 0;
			int j = 0;
			while (i < resultList.size()) {
				day = ((Number) resultList.get(i)[1]).intValue();
				month = ((Number) resultList.get(i)[2]).intValue();
				System.out.println("monthIndex11 ^^^^  : " + dayIndex + "-- " + monthIndex + "  /// "+ day + "-- " + month );
				while ((monthIndex < month) || ((monthIndex >= month) && (day > dayIndex))) {
					System.out.println("monthIndex ^^^^  : " + dayIndex + "-- " + monthIndex + "  /// "+ day + "-- " + month );
					report = new Report(Integer.toString(dayIndex), 0, 0, 0,
							Integer.toString(dayIndex) + "/" + Integer.toString(monthIndex));
					reportList.add(report);
					if (dayIndex < dayOfMonthLast) {
						dayIndex++;
					} else {
						dayIndex = 1;
						monthIndex++;
					}
				}
				postedDate = (String) resultList.get(i)[0];
				quantity = ((Number) resultList.get(i)[4]).intValue();
				if (((Number) resultList.get(i)[3]).intValue() == -1) {
					quantityOut = quantity;
					quantityIn = 0;
				} else {
					quantityOut = 0;
					quantityIn = quantity;
				}

				report = new Report(Integer.toString(dayIndex), quantityIn, quantityOut, 0,
						Integer.toString(dayIndex) + "/" + Integer.toString(monthIndex));
				j = (int) i + 1;
				while ((j < resultList.size()) && (!resultList.get(j)[0].equals(postedDate))) {
					j++;
				}
				if (j < resultList.size()) {
					if ((resultList.get(j)[0].equals(postedDate))) {
						quantityJ = ((Number) resultList.get(j)[4]).intValue();
						if (((Number) resultList.get(j)[3]).intValue() == -1) {
							report.setQuantityOut(report.getQuantityOut() + quantityJ);
						}
						if (((Number) resultList.get(j)[3]).intValue() == 1) {
							report.setQuantityIn(report.getQuantityIn() + quantityJ);
						}
					}
					resultList.remove(j);
				}
				report.setQuantity(report.getQuantityIn() + report.getQuantityOut());
				report.setQuantityOut(quantityOut * (-1));
				reportList.add(report);
				if (j == (i + 1)) {
					i = j;
				} else {
					i = i + 1;
				}
				monthIndex = month;
				if (dayIndex < dayOfMonthLast) {
					dayIndex++;
				} else {
					dayIndex = 1;
					monthIndex++;
				}
			}
			while ((monthIndex < now.getMonth().getValue())
					|| ((monthIndex >= now.getMonth().getValue()) && (now.getDayOfMonth() > dayIndex))) {
				report = new Report(Integer.toString(dayIndex), 0, 0, 0,
						Integer.toString(dayIndex) + "/" + Integer.toString(monthIndex));
				reportList.add(report);
				if (dayIndex < dayOfMonthLast) {
					dayIndex++;
				} else {
					dayIndex = 1;
					monthIndex++;
				}
			}
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		} catch (Exception e) {
			return new Entities(1, null, "Error Server");
		}

//		LocalDate now1 = LocalDate.now();
//		LocalDate lastMonth = now1.plusDays ( -30 );
//		
//		List<Object[]> resultList = transactionService.getReportInMonth();
//		System.out.println("result Report: " + resultList);
//		if (resultList == null) {
//			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
//		}
//		LocalDate now = LocalDate.now();
//		int firstDay = lastMonth.getDayOfMonth();
//		System.out.println("result Report firstDay: " + firstDay);
//		int monthLast = now.getMonth().getValue();
//		int year = now.getYear();
//		int dayOfMonthLast = 30;
//		if (monthLast == 1) {
//			year = year - 1;
//			monthLast = 12;
//		} else if (monthLast > 1) {
//			monthLast = monthLast - 1;
//		}
//		if (monthLast == 2) {
//			LocalDate localDate1 = LocalDate.of(year, monthLast, 1);
//			if (localDate1.isLeapYear()) {
//				dayOfMonthLast = 29;
//			} else {
//				dayOfMonthLast = 28;
//			}
//		} else if (monthLast == 1 || monthLast == 3 || monthLast == 5 || monthLast == 7 || monthLast == 8 || monthLast == 10
//				|| monthLast == 12) {
//			dayOfMonthLast = 31;
//		}
//		List<Report> reportList = new ArrayList<Report>();
//		Report report;
//		String postedDate = "";
//		String day = "";
//		long quantityIn = 0;
//		long quantityOut = 0;
//		long quantity = 0;
//		long quantityJ = 0;
//		int i = 0;
//		int j = 0;
//		int day2 = 1;
//		while (i < resultList.size()) {
//			if (((Number) resultList.get(i)[1]).intValue() > day2) {
//				for (int x = day2; x < ((Number) resultList.get(i)[1]).intValue(); x ++){
//					report = new Report(Integer.toString(x), 0, 0, 0, dayOfMonthLast);
//					reportList.add(report);
//				}
//				day2 = ((Number) resultList.get(i)[1]).intValue();
//			}
//			postedDate = (String) resultList.get(i)[0];
//			day = Integer.toString(((Number) resultList.get(i)[1]).intValue());
////			month = Integer.toString(((Number) resultList.get(i)[2]).intValue());
////			dayMonth = day + "/" + month;
//			quantity = ((Number) resultList.get(i)[4]).intValue();
//			if (((Number) resultList.get(i)[3]).intValue() == -1) {
//				quantityOut = quantity;
//				quantityIn = 0;
//			} else {
//				quantityOut = 0;
//				quantityIn = quantity;
//			}
//
//			report = new Report(day, quantityIn, quantityOut, 0, dayOfMonthLast);
//			j = (int) i + 1;
//			while ((j < resultList.size()) && (!resultList.get(j)[0].equals(postedDate))) {
//				j++;
//			}
//			if (j < resultList.size()) {
//				if ((resultList.get(j)[0].equals(postedDate))) {
//					quantityJ = ((Number) resultList.get(j)[4]).intValue();
//					if (((Number) resultList.get(j)[3]).intValue() == -1) {
//						report.setQuantityOut(report.getQuantityOut() + quantityJ);
//					}
//					if (((Number) resultList.get(j)[3]).intValue() == 1) {
//						report.setQuantityIn(report.getQuantityIn() + quantityJ);
//					}
//				}
//				resultList.remove(j);
//			}
//			report.setQuantity(report.getQuantityIn() + report.getQuantityOut());
//			report.setQuantityOut(quantityOut*(-1));
//			reportList.add(report);
//			day2 ++;
//			if (j == (i + 1)) {
//				i = j;
//			} else {
//				i = i + 1;
//			}
//		}
	}

	@GetMapping("/getItemLedgeEntry")
	public Entities getItemLedgeEntry() {

		List<Object[]> resultList = transactionService.getItemLedgeEntry();
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		Transaction transaction = new Transaction();
		TransactionLine transactionLine = new TransactionLine();
		for (Object[] result : resultList) {
			transaction = (Transaction) result[0];
			transactionLine = (TransactionLine) result[1];
			System.out.println("result Report: transaction " + transaction.getId() + ", item "
					+ transactionLine.getItem().getName() + ", UoM " + transactionLine.getItem().getUoM().getName()
					+ ", quantity " + transactionLine.getQuantity());
			reportList.add(new Report(transaction, transactionLine));
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
	}

	@GetMapping("/getAllReportByProject")
	public Entities getAllReportByProject() {
		System.out.println("result Report: " );
		List<Object[]> resultList = transactionService.getAllReportByProject();
		 System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		reportList = findListReportProjectShowInOutQuantity(resultList);
		if (reportList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
	}

	public List<Report> findListReportProjectShowInOutQuantity(List<Object[]> resultList) {
		if (resultList == null) {
			return null;
		}
		System.out.println("result Report```: ");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd h:m");
		LocalDateTime now = LocalDateTime.now();
		Date nowDate = null;
		Date finish = null;
		try {
			nowDate = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).parse(dtf.format(now));
			System.out.println("nowDate " + nowDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Report report;
		List<Report> reportList = new ArrayList<Report>();
		Project project = new Project();
		Item item = new Item();
		UoM uoM = new UoM();
		String expDate = "";
		int name = 0;
		long quantity;
		long quantityIn;
		long quantityOut;
		int i = 0;
		int j = 0;
		int quantityJ = 0;

		while (i < resultList.size()) {
			project = (Project) resultList.get(i)[0];
			item = (Item) resultList.get(i)[1];
			uoM = (UoM) resultList.get(i)[2];
			expDate = (String) resultList.get(i)[3];
			name = (int) resultList.get(i)[4];
			quantity = ((Number) resultList.get(i)[5]).intValue();
			if (quantity < 0) {
				quantityOut = quantity;
				quantityIn = 0;
			} else {
				quantityOut = 0;
				quantityIn = quantity;
			}
			System.out.println("project::::" + project + "item " + item + "expDate" + expDate);
			try {
			report = new Report(project, item, uoM, expDate, quantityIn, quantityOut, 0);
			
			j = i + 1;
			while ((j < resultList.size()) && (resultList.get(j)[0] == project) && (resultList.get(j)[1] == item)
					&& (resultList.get(j)[2] == uoM) && (resultList.get(j)[3] == null || resultList.get(j)[3].equals(expDate))) {
				quantityJ = ((Number) resultList.get(j)[5]).intValue();
				System.out.println("equals +++" + (String) resultList.get(j)[3]);
				if (quantityJ < 0) {
					report.setQuantityOut(report.getQuantityOut() + quantityJ);
				} else {
					report.setQuantityIn(report.getQuantityIn() + quantityJ);
				}
				j++;
			}
			report.setQuantity(report.getQuantityIn() + report.getQuantityOut());
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("error))):::(((::))" + e.toString());
				return null;
			}
//			if (report.getQuantity() != 0)
			try {
				finish = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(report.getProject().getFinishDate());

				if ((report.getQuantity() != 0) || (finish.after(nowDate))) {
					reportList.add(report);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			i = j;
			System.out.println("result Report: stock " + project.getId() + ", item " + item.getName() + ", UoM "
					+ uoM.getName() + ", quantity " + report.getQuantity());
		}
		return reportList;
	}

	@GetMapping("/getReportByProjectId")
	public Entities getReportByProjectId(@RequestParam("projectId") int projectId) {

		List<Object[]> resultList = transactionService.getReportByProjectId(projectId);
		System.out.println("result Report: " + resultList);
		if (resultList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		List<Report> reportList = new ArrayList<Report>();
		reportList = findListReportProjectShowInOutQuantity(resultList);
		if (reportList == null) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
	}

	@GetMapping("/getReportdetailByProjectIdItemId")
	public Entities getReportdetailByProjectIdItemId(@RequestParam("projectId") int projectId,
			@RequestParam("itemId") int itemId, @RequestParam("uoMId") int uoMId,
			@RequestParam("expDate") String expDate) {
		if (projectId == 0 || itemId == 0) {
			return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			// find
			List<Object[]> resultList = transactionService.getReportdetailByProjectIdItemId(projectId, itemId, uoMId,
					expDate);

			System.out.println("result "+ projectId + " " + itemId + " " +uoMId + "expDate:: /" + expDate +"/");
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			Transaction transaction = new Transaction();
			Item item = new Item();
			UoM uoM = new UoM();
			for (Object[] result : resultList) {
				transaction = (Transaction) result[0];
				item = (Item) result[1];
				uoM = (UoM) result[2];
				long quantity = ((Number) result[3]).intValue();
				System.out.println("result Report: stock " + transaction.getId() + ", item " + item.getName() + ", UoM "
						+ uoM.getName() + ", quantity " + quantity);
				reportList.add(new Report(transaction, item, uoM, quantity));
			}
			System.out.println("reportList " + reportList);
			return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/processExportExcelByProject")
	public void processExportExcelByProject(@RequestParam("projectId") int projectId, HttpServletResponse response)
			throws IOException {

		// List Report by Project // List Item Ledge Entry
		List<Object[]> resultItemLegdeEntry;
		List<Object[]> resultListReportBy;
		if (projectId == 0) {
			resultListReportBy = transactionService.getAllReportByProject();
			resultItemLegdeEntry = transactionService.getItemLedgeEntry();
		} else {
			resultListReportBy = transactionService.getReportByProjectId(projectId);
			resultItemLegdeEntry = transactionService.getItemLedgeEntryByProject(projectId);
		}

		List<Report> reportProjectList = new ArrayList<Report>();
		reportProjectList = findListReportProjectShowInOutQuantity(resultListReportBy);
		List<Report> reportItemLegdeEntry = new ArrayList<Report>();
		Transaction transaction = new Transaction();
		TransactionLine transactionLine = new TransactionLine();
		for (Object[] result : resultItemLegdeEntry) {
			transaction = (Transaction) result[0];
			transactionLine = (TransactionLine) result[1];
			System.out.println("result Report Project: transaction " + transaction.getId() + ", item "
					+ transactionLine.getItem().getName() + ", UoM " + transactionLine.getItem().getUoM().getName()
					+ ", quantity " + transactionLine.getQuantity());
			reportItemLegdeEntry.add(new Report(transaction, transactionLine));
		}
		System.out.println("result resultItemLegdeEntry: " + resultItemLegdeEntry);
		System.out.println("result reportItemLegdeEntry: " + reportItemLegdeEntry);

		// Set-up file excel
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		// Call functional
		ExcelExporter excelExporter = new ExcelExporter();
		excelExporter.exportProject(reportProjectList, reportItemLegdeEntry, response);
	}

	@PostMapping("/processExportExcelStockCheck")
	public Entities processExportExcelStockCheck(HttpServletResponse response) throws IOException {
		try {
			List<Object[]> resultList = transactionService.getReportAll();
			System.out.println("result Report: " + resultList);
			if (resultList == null) {
				return new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			}
			List<Report> reportList = new ArrayList<Report>();
			reportList = reportStockCheck(resultList);
			if (reportList != null) {

				response.setContentType("application/octet-stream");
				DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
				String currentDateTime = dateFormatter.format(new Date());
				String headerKey = "Content-Disposition";
				String headerValue = "attachment; filename=stockCheck_" + currentDateTime + ".xlsx";
				response.setHeader(headerKey, headerValue);
				ExcelExporter excelExporter = new ExcelExporter();
				excelExporter.exportStockCheck(reportList, response);
				return new Entities(0, reportList, HttpStatus.OK.getReasonPhrase());
			} else {
				return new Entities(1, null, "No Data");
			}
		} catch (Exception e) {
			return new Entities(1, null, e.toString());
		}
	}

	@GetMapping("/processExportExcelByStock")
	public void processExportExcelByStock(@RequestParam("stockId") int stockId, HttpServletResponse response)
			throws IOException {
		List<Object[]> resultItemLegdeEntry;
		List<Object[]> resultList;
		if (stockId == 0) {
			resultList = transactionService.getReportAll();
			resultItemLegdeEntry = transactionService.getItemLedgeEntry();
		} else {
			resultList = transactionService.getReportByStockId(stockId);
			resultItemLegdeEntry = transactionService.getItemLedgeEntryByStock(stockId);
		}

		List<Report> reportList = new ArrayList<Report>();
		Stock stock = new Stock();
		Item item = new Item();
		UoM uoM = new UoM();
		for (Object[] result : resultList) {
			stock = (Stock) result[0];
			item = (Item) result[1];
			uoM = (UoM) result[2];
			long quantity = ((Number) result[3]).intValue();
			reportList.add(new Report(stock, item, uoM, quantity));
		}

		List<Report> reportItemLegdeEntry = new ArrayList<Report>();
		Transaction transaction = new Transaction();
		TransactionLine transactionLine = new TransactionLine();
		for (Object[] result : resultItemLegdeEntry) {
			transaction = (Transaction) result[0];
			transactionLine = (TransactionLine) result[1];
			reportItemLegdeEntry.add(new Report(transaction, transactionLine));
		}

		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";

		response.setHeader(headerKey, headerValue);

		ExcelExporter excelExporter = new ExcelExporter();

		excelExporter.exportStock(reportList, reportItemLegdeEntry, response);
	}

}
