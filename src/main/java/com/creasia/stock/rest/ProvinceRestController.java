package com.creasia.stock.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Province;
import com.creasia.stock.service.ProvinceService;

@RestController
@RequestMapping("api")
public class ProvinceRestController {

	private ProvinceService provinceService;
	
	public ProvinceRestController(ProvinceService theProvinceService) {
		super();
		this.provinceService = theProvinceService;
	}
	
	@GetMapping("/provinces")
	public Entities findAll(){
		List<Province> provinceList = provinceService.findAll();
		if (provinceList == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, provinceList, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else { 
			return new Entities(0, provinceList, HttpStatus.OK.getReasonPhrase());
		}
		//return provinceService.findAll();
	}
	
	@GetMapping("/provinces/{id}")
	public Entities findById(@RequestBody int itemId) {
		Province theProvince =  provinceService.findById(itemId);
		if (theProvince == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, theProvince, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, theProvince, HttpStatus.OK.getReasonPhrase());
		}
		//return provinceService.findById(itemId);
	}
	
	@PostMapping("/provinces")
	public Entities addProvince(@RequestBody Province theProvince) {
		if (theProvince.getName().equals("") ) {
			//name == null
			return new Entities(1,null, "Name null");
		}
		
		theProvince.setId(0);
		provinceService.save(theProvince);
		Entities entities = new Entities(0,theProvince , HttpStatus.OK.getReasonPhrase());
		return entities;
		//return findAll();
	}
	
	
	
//	@PutMapping("/provinces")
//	public Province updateProvince(@RequestBody Province theProvince) {
//		
//		provinceService.save(theProvince);
//		return theProvince;
//	}
//	
//	@DeleteMapping("/provinces")
//	public List<Province> deleteProvince(@RequestBody int provinceId){
//		provinceService.deleteById(provinceId);
//		return findAll();
//	}
	
}
