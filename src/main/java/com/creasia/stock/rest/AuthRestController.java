package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.dao.RoleRepository;
import com.creasia.stock.dao.UserRepository;
import com.creasia.stock.entity.ERole;
import com.creasia.stock.entity.Role;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.payload.request.LoginRequest;
import com.creasia.stock.payload.request.SignupRequest;
import com.creasia.stock.payload.response.JwtResponse;
import com.creasia.stock.payload.response.MessageResponse;
import com.creasia.stock.security.jwt.JwtUtils;
import com.creasia.stock.security.services.UserDetailsImpl;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.UserService;
import com.creasia.stock.service.StockService;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.PermissionAssignment;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthRestController {

	private UserService userService;

	private StockService stockService;

	private PermissionAssignmentService permissionAssignmentService;

	@Autowired
	public AuthRestController(UserService theUserService, PermissionAssignmentService thePermissionAssignmentService,
			StockService theStockService) {
		userService = theUserService;
		permissionAssignmentService = thePermissionAssignmentService;
		stockService = theStockService;
	}

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public Entities authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		// ResponseEntity<?>
		System.out.println("username: " + loginRequest.getUsername());
		System.out.println("username: " + loginRequest.getPassword());

		if (loginRequest.getUsername().contains("@") == true) {
			int indexOfChar = loginRequest.getUsername().indexOf('@');
			String userName = loginRequest.getUsername().substring(0, indexOfChar);
			loginRequest.setUsername(userName);
		}

		try {
			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

			System.out.println(" roles: abc" + authentication);

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			System.out.println(" roles: def");

			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
					.collect(Collectors.toList());

			System.out.println(" roles: " + roles);
			if (userDetails.getUsername() == null) {
				return new Entities(1, null, "Not user");
			}
			if (userDetails.isBlock() == true) {
				return new Entities(1, null, "User was blocked");
			}
			if (userDetails.isFirstLogin()) {
				System.out.println("isFirstLogin true" + userDetails.isFirstLogin());
				return new Entities(0,
						ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
								userDetails.getEmail(), roles, userDetails.getEmployee(), userDetails.isBlock(),
								userDetails.isFirstLogin())),
						"First Login");
			} else {
				System.out.println("isFirstLogin false " + userDetails.isFirstLogin());
				return new Entities(0,
						ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
								userDetails.getEmail(), roles, userDetails.getEmployee(), userDetails.isBlock(),
								userDetails.isFirstLogin())),
						HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "Username or password is incorrect");
		}
	}

	@PostMapping("/signup")
	public Entities registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		// ResponseEntity<?>
		try {
			System.out.println("getstock() " + signUpRequest.getStock());
			if ( signUpRequest.getStock() != null) {
				if ( signUpRequest.getStock().getId() == 0) {
					signUpRequest.setStock(null);
				}
			}
			if (signUpRequest.getEmployee() == null || signUpRequest.getRole() == null) {
				return new Entities(1,
						ResponseEntity.badRequest().body(new MessageResponse("Error: Body have null value!")),
						"Error: Body have null value!");
			}
			// find user exist?
			System.out.println("getEmployee().getId() " + signUpRequest.getEmployee().getId());

			if (userService.findByEmployeeId(signUpRequest.getEmployee().getId()) != null
					&& userService.findByEmployeeId(signUpRequest.getEmployee().getId()).size() > 0) {
				return new Entities(1,
						ResponseEntity.badRequest().body(new MessageResponse("Error: Body have null value!")),
						"User already exists!");
			}

			signUpRequest.setEmail(signUpRequest.getEmployee().getEmail());
			System.out.println("signUpRequest.getEmployee().getEmail(): " + signUpRequest.getEmployee().getEmail());
			int indexOfChar = signUpRequest.getEmail().indexOf('@');
			String defaultPassword = signUpRequest.getEmail().substring(0, indexOfChar);
			System.out.println("defaultPassword: " + defaultPassword);
			// Create new user's account
			User user = new User(defaultPassword, signUpRequest.getEmail(), encoder.encode(defaultPassword),
					signUpRequest.getEmployee(), false, true);

			Set<String> strRoles = signUpRequest.getRole();
			Set<Role> roles = new HashSet<>();
			System.out.println(" strRoles: " + strRoles);
			if (strRoles == null) {
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);
				System.out.println(" roles: " + roles);
			} else {
				strRoles.forEach(role -> {
					System.out.println(" role: " + role);
					switch (role) {
					case "ROLE_ADMIN":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);

						break;
					case "ROLE_POST":
						Role modRole = roleRepository.findByName(ERole.ROLE_POST)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(modRole);
						break;
					case "ROLE_APPROVE":
						Role approveRole = roleRepository.findByName(ERole.ROLE_APPROVE)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(approveRole);

						break;
					case "ROLE_VERIFY":
						Role veryfyRole = roleRepository.findByName(ERole.ROLE_VERIFY)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(veryfyRole);

						break;
					default:
						Role userRole = roleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);
					}
				});
			}
			System.out.println(" user user: " + user);
			System.out.println(" user username: " + user.getUsername() + user.getPassword());
			user.setEmail(user.getEmployee().getEmail());
			user.setRoles(roles);
			user.setBlock(false);
			user.setFirstLogin(true);
			System.out.println("isPost = " + user);
			try {
				System.out.println("userRepository = " + user);
				userRepository.save(user);
				System.out.println("user = " + user);
				Stock st;
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();

				for (Role role : user.getRoles()) {
					if (role.getId() != 3 ) {
						st = null;
					} else {
						st = signUpRequest.getStock();
					}
					PermissionAssignment permissionAssignment = new PermissionAssignment(st, user);
					permissionAssignment.setRoles(role);
					permissionAssignment.setCreatedAt(dtf.format(now));
					permissionAssignment.setDescription(signUpRequest.getDescription());
					if (signUpRequest.getStartingDate() != null) {
						permissionAssignment.setStartingDate(signUpRequest.getStartingDate());
					} else {
						permissionAssignment.setStartingDate(dtf.format(now));
					}
					permissionAssignment.setCreateBy(signUpRequest.getCreateBy());
					permissionAssignmentService.save(permissionAssignment);
					System.out.println("permissionAssignment " + permissionAssignment);
				}
			} catch (Exception e) {
				// TODO: handle exception
				return new Entities(1, e.toString(), "error server");
			}
			return new Entities(0, ResponseEntity.ok(new MessageResponse("User registered successfully!")),
					"User registered successfully!");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e.toString() faailll: " + e.toString());
			return new Entities(1, e.toString(), "error server");
		}
	}
}