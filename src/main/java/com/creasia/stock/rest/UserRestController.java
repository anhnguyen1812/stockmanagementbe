package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.UoM;
import com.creasia.stock.entity.User;
import com.creasia.stock.entity.Vendor;
import com.creasia.stock.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class UserRestController {

	private UserService userService;
	
	@Autowired
	public UserRestController(UserService theUserService) {
		userService = theUserService;
	}
	
	@Autowired
	PasswordEncoder encoder;
	
	@GetMapping("/Users")
	public Entities findAll(){
		System.out.println("user List:: :)) --- " + null);
		try {
		List<User> userList = userService.findAll();
		System.out.println("user List:: :)) --- " + userList);
		if(userList == null) {
			return new Entities(1, userList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		else {
			return new Entities(0, userList, HttpStatus.OK.getReasonPhrase());
		}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("error :: :)) --- " + e.toString());
			return new Entities(1, null, "SERVER ERROR");
		}
	}
	
	@GetMapping("/findUserPost")
	public Entities findUserPost(){
		
		List<User> userList = userService.findUserPost();
		if(userList == null) {
			return new Entities(1, userList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		else {
			return new Entities(0, userList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/Users")
	public Entities addUser(@RequestBody User theUser) {
		if (theUser.getEmployee() == null) {
			return new Entities(1,null, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		theUser.setEmail(theUser.getEmployee().getEmail());
		theUser.setBlock(false);
		userService.save(theUser);
		Entities Entities = new Entities(0,theUser , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	@PutMapping("/Users")
	@PreAuthorize("hasRole('ADMIN')")
	public Entities updateUser(@RequestBody User theUsers) {
		System.out.println("user " + theUsers);
		if (theUsers.getId() == 0) {
			//name == null
			System.out.println("user " + theUsers.getUsername());
			return new Entities(1,theUsers, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			User user = userService.findById(theUsers.getId());
			user.setBlock(theUsers.isBlock());
			user.setRoles(theUsers.getRoles());
			System.out.println("user1role::: " + user.getRoles());
			userService.update(user);
			System.out.println("user " + theUsers);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theUsers, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theUsers, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theUsers , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/changePassword")
	public Entities changePassword(@RequestBody User theUsers) {
		System.out.println("user " + theUsers);
		if (theUsers.getId() == 0) {
			return new Entities(1,theUsers, "Not find user");
		}
		if (theUsers.getPassword().equals("")) {
			return new Entities(1,theUsers, "password not null");
		}
		try {
			User user = userService.findById(theUsers.getId());
			
			user.setPassword(encoder.encode(theUsers.getPassword()));
			user.setFirstLogin(false);
			userService.update(user);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theUsers, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theUsers, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theUsers , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	
}
