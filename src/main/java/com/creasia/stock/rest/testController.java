package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.service.ItemCategoryService;

@Controller
public class testController {
	
private ItemCategoryService itemCategoryService;
	
	//quick and dirtry: inject itemcategory service
	@Autowired
	public testController(ItemCategoryService theItemCategoryService) {
		itemCategoryService = theItemCategoryService;
	}
	
	@GetMapping("/")
	public String sayHello(){
		return "Hello! Time on server at: " + LocalDateTime.now();
	}

	@GetMapping("/abc")
	public String sayHi(){
		return "Hi hi hi !";
	}
	
//	@GetMapping("/list")
//	public String sayHiHi(Model theModel){
//		
//		theModel.addAttribute("theDate", new java.util.Date());
//		return "helloword";
//	}

	@GetMapping("/list")
	public String listEmployees(Model theModel) {
		
		// get employees from db
		List<ItemCategory> theCategories = itemCategoryService.findAll();
		
		// add to the spring model
		theModel.addAttribute("itemCategories", theCategories);
		
		return "/helloworld";
	}
}