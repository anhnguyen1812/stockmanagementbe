package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Project;
import com.creasia.stock.service.ProjectService;

@RestController
@RequestMapping("api")
public class ProjectRestController {

	private ProjectService projectService;

	@Autowired
	public ProjectRestController(ProjectService theProjectService) {
		projectService = theProjectService;
	}

	@GetMapping("/projects")
	public Entities findAll() {

		List<Project> projectList = projectService.findAll();
		if (projectList == null) {
			return new Entities(1, projectList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, projectList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/getProjectByPmId/{pmId}")
	public Entities findProjectByPmId(@PathVariable int pmId) {

		List<Project> projectList = projectService.findProjectByPmId(pmId);
		if (projectList == null) {
			return new Entities(1, projectList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, projectList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/countProjects")
	public Entities countProjects() {
		long count = projectService.countProject();
		System.out.println("project List:: " + count);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countProject", count);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}
	
	@GetMapping("/projects/{projectId}")
	public Entities getProject(@PathVariable int projectId) {

		Project project = projectService.findById(projectId);
		if (project == null) {
			return new Entities(1, project, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, project, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/projects")
	public Entities addProject(@RequestBody Project theProject) {
//		theProject.setId(0);
//		projectService.save(theProject);
//		return theProject;

		if (theProject.getName().equals("") || theProject.getCompany() == null 
				|| theProject.getCustomer() == null || theProject.getEmployee() == null) {
			// name == null
			return new Entities(1, theProject, "Body Invalid");
		}
		theProject.setCode(theProject.getCode().toUpperCase());
		long numOfProjectByCode = projectService.findByCode(theProject.getCode());
		
		if (numOfProjectByCode > 0) {
			return new Entities(1, theProject, "This project already exists");
		}
		Project project = new Project(theProject.getCode(), theProject.getName(), theProject.getStartDate(),  theProject.getTypes(), theProject.getGroups(), theProject.getDuration() ,theProject.getTempTerm(), theProject.getFinishDate(), theProject.getCreatedAt(), theProject.getUpdatedAt(), false);
		project.setId(0);
		System.out.println("theProject.getCompany() : " + theProject.getCompany().getDistrict());
		System.out.println("theStock.getCustomer() : " + theProject.getCustomer().getDistrict());
		project.setCompany(theProject.getCompany());
		project.setEmployee(theProject.getEmployee());
		project.setCustomer(theProject.getCustomer());
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		project.setCreatedAt(dtf.format(now));
		project.setUpdatedAt(dtf.format(now));
		try {
			projectService.save(project);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theProject, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theProject, "Fail to server");
		}
		Entities entities = new Entities(0, theProject, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/projects")
	public Entities updateProject(@RequestBody Project theProject) {
//		projectService.save(theProject);
//		return theProject;

		if (theProject.getName().equals("") || theProject.getCompany() == null 
				|| theProject.getCustomer() == null || theProject.getEmployee() == null) {
			// name == null
			return new Entities(1, theProject, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theProject.setUpdatedAt(dtf.format(now));
		try {
			projectService.update(theProject);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theProject, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theProject, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theProject, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/projects/{projectId}")
	public Entities deleteProject(@PathVariable int projectId) {

		Project project = projectService.findById(projectId);
		if (project == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + projectId);
		}

		return findAll();
	}

	@GetMapping("/getNewProjectCode")
	public Entities getNewProjectCode() {

		// length code PROJECT = 9 (EX: "PRO000001")
		String lastCode = "";
		String st_0 = "0";
		int lastId = -1;
		try {
			lastId = projectService.findNewCode();
		}catch(Exception e) {
			System.out.print(e.toString());
		}
		if (lastId == 0) {
			lastCode = "PRO000001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 =  new String(new char[6 - temp.length()]).replace("\0", "0");
			lastCode = "PRO" + st_0 + temp;
		}
		Project project = new Project();
		project.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("projectCode", project.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
}
