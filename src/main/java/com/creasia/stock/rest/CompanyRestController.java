package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.dao.CompanyDAO;
import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.entity.Province;
import com.creasia.stock.service.CompanyService;

@RestController
@RequestMapping("api")
public class CompanyRestController {

	private CompanyService companyService;

	
	  public CompanyRestController(CompanyService theCompanyService){ 
		  super();
		  this.companyService = theCompanyService;
	  }
	 
	@GetMapping("/companies")
//	@PreAuthorize("hasRole('ROLE_USER')")
	public Entities getAllCompany() {
		List<Company> company = companyService.findAll();
		if (company == null) {
			return new Entities(1, company, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, company, HttpStatus.OK.getReasonPhrase());
		}
	}


	@GetMapping("/companies/{companyId}")
	public Entities findById(@RequestBody int companyId) {
		Company theCompany = companyService.findById(companyId);
		
		if (theCompany == null) {
			return new Entities(1, theCompany, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, theCompany, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/companies")
	public Entities addCompany(@RequestBody Company theCompany) {
		if (theCompany.getName().equals("") || theCompany.getDistrict().getProvince().equals(null) 
				|| theCompany.getDistrict().equals(null) || theCompany.getAddress().equals(null)) {
			//name == null
			return new Entities(1,theCompany, HttpStatus.NO_CONTENT.getReasonPhrase());
		}

		theCompany.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		theCompany.setCreatedAt(dtf.format(now));
		try {
			companyService.save(theCompany);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theCompany, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theCompany, "Fail to server");
		}
		Entities entities = new Entities(0,theCompany , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/companies")
	public Entities updateCompany(@RequestBody Company theCompany) {
		
		if (theCompany.getName().equals("") || theCompany.getDistrict().getProvince().equals(null) 
				|| theCompany.getDistrict().equals(null) || theCompany.getAddress().equals("")) {
			//name == null
			return new Entities(1,theCompany, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			companyService.update(theCompany);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theCompany, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theCompany, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theCompany , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@DeleteMapping("/companies")
	public Entities deleteCompany(@RequestBody int companyId){
		Company company =  companyService.findById(companyId);
		companyService.deleteById(companyId);
		if (company == null) {
			Entities entities = new Entities(1,null, HttpStatus.NOT_FOUND.getReasonPhrase());
			return entities;
		}
		
		companyService.deleteById(companyId);
		Entities entities = new Entities(0,companyService.findAll(), HttpStatus.OK.getReasonPhrase());
		return entities;
	}	
	@GetMapping("/getNewCompanyCode")
	public Entities getNewCompanyCode(){
		
		//length code Company = 5
		String lastCode = "";
		String st_0 = "0";
		int lastId = -1;
		try {
			lastId = companyService.findNewCode();
		}catch(Exception e) {
			System.out.print(e.toString());
		}
		if (lastId == 0) {
			lastCode = "CP001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 =  new String(new char[3 - temp.length()]).replace("\0", "0");
			lastCode = "CP" + st_0 + temp;
		}
		Company company = new Company();
		company.setCode(lastCode);
		HashMap<String,String> mapCode =new HashMap();
		mapCode.put("companyCode",company.getCode() );
		Entities Entities = new Entities(0, mapCode , HttpStatus.OK.getReasonPhrase());
		return Entities;
		
	}

}
