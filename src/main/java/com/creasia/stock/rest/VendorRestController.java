package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Customer;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Vendor;
import com.creasia.stock.service.VendorService;


@RestController
@RequestMapping("api")
public class VendorRestController {

	private VendorService vendorService;
	
	@Autowired 
	public VendorRestController(VendorService theVendorService) {
		vendorService = theVendorService;
	}
	
	@GetMapping("/vendors")
	public Entities findAll(){
		
		List<Vendor> vendorList = vendorService.findAll();
		if (vendorList == null) {
			return new Entities(1, vendorList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, vendorList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/vendors/{vendorId}")
	public Entities getVendor(@PathVariable int vendorId){
		
		Vendor vendor = vendorService.findById(vendorId);
		if (vendor == null) {
			return new Entities(1, vendor, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, vendor, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/vendors")
	public Entities addVendor(@RequestBody Vendor theVendor) {
		System.out.println(" vendorrrrrrrr::: " + theVendor);
		if (theVendor.getName().equals("") || theVendor.getDistrict().getProvince().equals(null) 
				|| theVendor.getDistrict().equals(null)) {
			//name == null
			return new Entities(1,theVendor, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		theVendor.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		theVendor.setCreatedAt(dtf.format(now));
		try {
			vendorService.save(theVendor);
		}catch(OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1,theVendor, "Fail to server");
		}catch(StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1,theVendor, "Fail to server");
		}
		Entities entities = new Entities(0,theVendor , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/vendors")
	public Entities updateVendor(@RequestBody Vendor theVendor) {
//		vendorService.save(theVendor);
//		return theVendor;
		
		if (theVendor.getName().equals("") || theVendor.getDistrict().getProvince().equals(null) 
				|| theVendor.getDistrict().equals(null)) {
			//name == null
			return new Entities(1,theVendor, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			//theVendor.setAddress('N'+theVendor.getAddress().toString());
			vendorService.update(theVendor);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theVendor, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theVendor, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theVendor , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@DeleteMapping("/vendors/{vendorId}")
	public Entities deleteVendor(@PathVariable int vendorId) {
		
		Vendor vendor = vendorService.findById(vendorId);
		if (vendor == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + vendorId);
		}
		
		return findAll();
	}
	
	@GetMapping("/getNewVendorCode")
	public Entities getNewVendorCode(){
		
		//length code Company = 6 (EX: "CU0001")
		String lastCode = "";
		String st_0 = "0";
		int lastId =  vendorService.findNewCode();
		
		if (lastId == 0) {
			lastCode = "VD0001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 =  new String(new char[4 - temp.length()]).replace("\0", "0");
			lastCode = "VD" + st_0 + temp;
		}
		Vendor vendor = new Vendor();
		vendor.setCode(lastCode);
		HashMap<String,String> mapCode =new HashMap();
		mapCode.put("vendorCode",vendor.getCode() );
		Entities Entities = new Entities(0, mapCode , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	
}












