package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.service.ItemCategoryService;

@Controller
@RestController
@RequestMapping("api")
public class ItemCategoryRestController {

	
	private ItemCategoryService itemCategoryService;
	
	//quick and dirtry: inject itemcategory service
	@Autowired
	public ItemCategoryRestController(ItemCategoryService theItemCategoryService) {
		itemCategoryService = theItemCategoryService;
	}
	
	//expose "/itemcategories" and return list
	@GetMapping("/itemCategories")
	public Entities findAll(){
		List<ItemCategory> itemCategoryList = itemCategoryService.findAll();
		if (itemCategoryList == null) {
			return new Entities(1, itemCategoryList, HttpStatus.NOT_FOUND.getReasonPhrase());	
		}else {
			return new Entities(0, itemCategoryList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	//get itemcategory by id
	@GetMapping("/itemCategories/{itemCategoryId}")
	public Entities getItemCategory(@PathVariable int itemCategoryId) {
		
		ItemCategory theItemCategory = itemCategoryService.findById(itemCategoryId);
		if (theItemCategory == null) {
			return new Entities(1, theItemCategory, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, theItemCategory, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	//add new itemCategory
	@PostMapping("/itemCategories")
	public Entities addItemCategory(@RequestBody ItemCategory theItemCategory) {
		if (theItemCategory.getName().equals("") ) {
			//name == null
			return new Entities(1,theItemCategory, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		theItemCategory.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		theItemCategory.setCreateAt(dtf.format(now));
		itemCategoryService.save(theItemCategory);
		Entities Entities = new Entities(0,theItemCategory , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	@PutMapping("/itemCategories")
	public Entities updateItemcategory(@RequestBody ItemCategory theItemCategory) {
		if (theItemCategory.getName().equals("") ) {
			return new Entities(1,theItemCategory, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		itemCategoryService.update(theItemCategory);
		System.out.println(theItemCategory);
		Entities Entities = new Entities(0,theItemCategory , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	//add delete itemCategory
	@DeleteMapping("/itemCategories/{itemCategoryId}")
	public Entities deleteItemCategory(@PathVariable int itemCategoryId, Model model) {
		ItemCategory itemCategory = itemCategoryService.findById(itemCategoryId);
		
		if (itemCategory == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + itemCategoryId);
		}
		
		itemCategoryService.deleteById(itemCategoryId);
		Entities Entities = new Entities(0,itemCategoryService.findAll(), HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	@GetMapping("/getNewItemCategoryCode")
	public Entities getLastId(){
		
		//length code itemcategory = 5
		String lastCode = "";
		String st_0 = "0";
		int lastId =  itemCategoryService.findNewCode();
		
		if (lastId == 0) {
			lastCode = "IC001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 =  new String(new char[3 - temp.length()]).replace("\0", "0");
			lastCode = "IC" + st_0 + temp;
		}
		ItemCategory itemCategory = new ItemCategory();
		itemCategory.setCode(lastCode);
		HashMap<String,String> mapCode =new HashMap();
		mapCode.put("categoryCode",itemCategory.getCode() );
		Entities Entities = new Entities(0, mapCode , HttpStatus.OK.getReasonPhrase());
		return Entities;
		
	}

}
