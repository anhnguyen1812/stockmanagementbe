package com.creasia.stock.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Role;
import com.creasia.stock.service.RoleService;

@RestController
@RequestMapping("api")
public class RoleRestController {

	private RoleService roleService;

	@Autowired
	public RoleRestController(RoleService theRoleService) {
		roleService = theRoleService;
	}

	@GetMapping("/Roles")
	@PreAuthorize("hasRole('ADMIN')")
	public Entities findAll() {
		try {
		List<Role> roleList = roleService.findAll();
		if (roleList == null) {
			return new Entities(1, roleList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, roleList, HttpStatus.OK.getReasonPhrase());
		}
		} catch (Exception e) {
			// TODO: handle exception 
			System.out.println("error " + e.toString());
			return new Entities(1, null, "SERVER ERROR");
		}
	}
}
