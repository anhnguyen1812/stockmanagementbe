package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.ReceiptNote;
import com.creasia.stock.entity.ReceiptNoteLine;
import com.creasia.stock.entity.ReturnReceipt;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.service.ReceiptNoteService;
import com.creasia.stock.service.ReturnReceiptService;
import com.creasia.stock.service.TransactionService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ReceiptNoteRestController {

	private ReceiptNoteService receiptNoteService;
	private TransactionService transactionService;

	@Autowired
	public ReceiptNoteRestController(ReceiptNoteService theReceiptNoteService,
			TransactionService theTransactionService) {
		receiptNoteService = theReceiptNoteService;
		transactionService = theTransactionService;
	}

	@GetMapping("/receiptNotes")
	public Entities findAll() {

		List<ReceiptNote> receiptNoteList = receiptNoteService.findAll();
		if (receiptNoteList == null) {
			return new Entities(1, receiptNoteList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, receiptNoteList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/countReceiptNotes")
	public Entities countShipmentNotes() {
		long receiptNote = receiptNoteService.countReceiptNotes();
		System.out.println("item List:: " + receiptNote);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countReceiptNote", receiptNote);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}

	@GetMapping("/receiptNotes/{receiptNoteId}")
	public Entities getReceiptNote(@PathVariable int receiptNoteId) {

		ReceiptNote receiptNote = receiptNoteService.findById(receiptNoteId);
		if (receiptNote == null) {
			return new Entities(1, receiptNote, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, receiptNote, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/receiptNotes")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities addReceiptNote(@RequestBody ReceiptNote theReceiptNote) {
		if (theReceiptNote.getEmployee() == null || theReceiptNote.getProject() == null
				|| theReceiptNote.getStock() == null || theReceiptNote.getVendor() == null) {
			return new Entities(1, theReceiptNote, "Body Invalid");
		}
		for (int i = 0; i < theReceiptNote.getReceiptNoteLines().size(); i++) {
			ReceiptNoteLine rnl = theReceiptNote.getReceiptNoteLines().get(i);
			theReceiptNote.getReceiptNoteLines().get(i).setPostedQuantity(rnl.getQuantity());
			if (rnl.getExpDate().equals("")) {
				theReceiptNote.getReceiptNoteLines().get(i).setExpDate(null);
			}
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theReceiptNote.setCreatedAt(dtf.format(now));

		int totalQuantity = 0;
		for (ReceiptNoteLine rnLine : theReceiptNote.getReceiptNoteLines()) {
			totalQuantity += rnLine.getQuantity();
		}
		if (totalQuantity == 0) {
			return new Entities(1, theReceiptNote, "No item choosed");
		} else {
			theReceiptNote.setTotalQuantity(totalQuantity);
			theReceiptNote.setTotalPostedQuantity(totalQuantity);
			theReceiptNote.setPostedDate(theReceiptNote.getDocumentAt());
		}
		try {

			Transaction transaction = new Transaction(1, theReceiptNote.getCode(), "Receipt",
					theReceiptNote.getProject(), theReceiptNote.getEmployee(), theReceiptNote.getStock(), 1,
					theReceiptNote.getCreatedAt(), theReceiptNote.getDocumentAt(), theReceiptNote.getDocumentAt());
//			Transaction transaction = new Transaction( 1, theReceiptNote.getCode(),"Receipt", theReceiptNote.getProject(), theReceiptNote.getEmployee(), theReceiptNote.getStock(), true, theReceiptNote.getCreatedAt(), theReceiptNote.getDocumentAt());
			List<TransactionLine> transactionLines = new ArrayList<TransactionLine>();

			TransactionLine transactionLine;
			for (ReceiptNoteLine receiptNoteLine : theReceiptNote.getReceiptNoteLines()) {
				transactionLine = new TransactionLine();
				transactionLine.setItem(receiptNoteLine.getItem());
				transactionLine.setQuantity(receiptNoteLine.getQuantity());
				transactionLine.setPostedQuantity(receiptNoteLine.getQuantity());
				transactionLine.setUoM(receiptNoteLine.getUoM());
				transactionLine.setItemExtNo(receiptNoteLine.getItemExtNo());
				transactionLine.setExpDate(receiptNoteLine.getExpDate());
				transactionLines.add(transactionLine);
			}
			transaction.setTotalQuantity(totalQuantity);
			transaction.setTotalPostedQuantity(totalQuantity);
			transaction.setTransactionLines(transactionLines);
			transaction.setPostedBy(theReceiptNote.getEmployee());
			try {
				receiptNoteService.save(theReceiptNote);
				transactionService.save(transaction);
			} catch (Exception e) {
				return new Entities(1, theReceiptNote, "Fail to server");
			}
			System.out.println("transactionLines: " + transactionLines);

//			System.out.println("receiptNoteLines: " + theReceiptNote.getReceiptNoteLines());
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theReceiptNote, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theReceiptNote, "Fail to server");
		}
		Entities entities = new Entities(0, theReceiptNote, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/receiptNotes")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities updateReceiptNote(@RequestBody ReceiptNote theReceiptNote) {
		if (theReceiptNote.getEmployee() == null || theReceiptNote.getProject() == null
				|| theReceiptNote.getStock() == null || theReceiptNote.getVendor() == null) {
			return new Entities(1, theReceiptNote, "Body Invalid");
		}
		int totalQuantity = 0;
		for (ReceiptNoteLine rnLine : theReceiptNote.getReceiptNoteLines()) {
			totalQuantity += rnLine.getQuantity();
		}
		if (totalQuantity == 0) {
			return new Entities(1, theReceiptNote, "No item choosed");
		} else {
			theReceiptNote.setTotalQuantity(totalQuantity);
		}
		try {
			receiptNoteService.update(theReceiptNote);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theReceiptNote, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theReceiptNote, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theReceiptNote, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@GetMapping("/getNewReceiptNoteCode")
	public Entities getNewReceiptNoteCode() {
		String newCode;
		String lastCode = receiptNoteService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if (lastCode == "") {
			newCode = "RN/" + year + "/000001";
		} else {
			String headerCode = lastCode.substring(3, 5);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(6);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				int tempInt = newMainCode.length();
				if (tempInt == 1) {
					st_0 = "00000";
				}
				if (tempInt == 2) {
					st_0 = "0000";
				}
				if (tempInt == 3) {
					st_0 = "000";
				}
				if (tempInt == 4) {
					st_0 = "00";
				}
				if (tempInt == 5) {
					st_0 = "0";
				}
				if (tempInt == 6) {
					st_0 = "";
				}
				newCode = "RN/" + year + "/" + st_0 + newMainCode;
			} else {
				newCode = "RN/" + year + "/000001";
			}
			// System.out.println("newCode "+ newCode);
		}
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("receiptNoteCode", newCode);
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	@GetMapping("/findInventoryByReceiptNote")
	public Entities findInventoryByReceiptNote(@RequestParam("receiptId") int receiptId) {

		ReceiptNote receiptNote = receiptNoteService.findById(receiptId);
		if (receiptNote == null) {
			return new Entities(1, receiptNote, "No find to receipt Note");
		}
		Stock stock = receiptNote.getStock();
		Project project = receiptNote.getProject();
		if (project == null || stock == null) {
			return new Entities(1, receiptNote, "No find to receipt Note");
		}
		List<ReceiptNoteLine> rnLines = receiptNote.getReceiptNoteLines();
		if (rnLines == null || rnLines.size() == 0) {
			return new Entities(1, receiptNote, "No find to receipt Note");
		}
		try {
		ReceiptNote newReceiptNote = receiptNote;
		List<ReceiptNoteLine> newReceiptNoteLines = new ArrayList<ReceiptNoteLine>();
		ReceiptNoteLine newLine = new ReceiptNoteLine();
		int finalQuantity = 0;
		for (ReceiptNoteLine rnLine : rnLines) {
			newLine = rnLine;
			int inventoryQty = (int) transactionService.findInventoryByItemAndExpDate(project.getId(), stock.getId(),
					rnLine.getItem().getId(), rnLine.getUoM().getId(), rnLine.getExpDate());
			int postQquantity = rnLine.getPostedQuantity();
			if (inventoryQty < postQquantity) {
				newLine.setInventoryQty(inventoryQty);
			}else {
				newLine.setInventoryQty(postQquantity);
			}
			newReceiptNoteLines.add(newLine);
			finalQuantity += newLine.getInventoryQty();	
		}
		newReceiptNote.setTotalQuantity(finalQuantity);
		newReceiptNote.setReceiptNoteLines(newReceiptNoteLines);
		return new Entities(0, newReceiptNote, "No find to receipt Note");
		}catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, receiptNote, "No find to receipt Note");
		}
	}
}
