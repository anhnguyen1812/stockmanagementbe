package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Company;
import com.creasia.stock.entity.Customer;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.service.CustomerService;

@RestController
@RequestMapping("api")
public class CustomerRestController {

	private CustomerService customerService;
	
	@Autowired
	public CustomerRestController(CustomerService theCustomerService) {
		customerService = theCustomerService;
	}
	
	@GetMapping("/customers")
	public Entities findAll(){
		List<Customer> customer = customerService.findAll();
		if (customer == null) {
			return new Entities(1, customer, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, customer, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/customers/{customerId}")
	public Entities getCustomer(@PathVariable int customerId) {
		Customer customer = customerService.findById(customerId);
		if (customer == null) {
			return new Entities(1, customer, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, customer, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/customers")
	public Entities addCustomer(@RequestBody Customer theCustomer) {
		if (theCustomer.getName().equals("") || theCustomer.getDistrict().getProvince().equals(null) 
				|| theCustomer.getDistrict().equals(null) || theCustomer.getAddress().equals(null)) {
			//name == null
			return new Entities(1,theCustomer, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		theCustomer.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		theCustomer.setCreatedAt(dtf.format(now));
		try {
			System.out.println("theCustomer : " + theCustomer);
			customerService.save(theCustomer);
		}catch(OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1,theCustomer, "Fail to server");
		}catch(StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1,theCustomer, "Fail to server");
		}
		Entities entities = new Entities(0,theCustomer , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/customers")
	public Entities updateCustomer(@RequestBody Customer theCustomer) {
		if (theCustomer.getName().equals("") || theCustomer.getDistrict().getProvince().equals(null) 
				|| theCustomer.getDistrict().equals(null) || theCustomer.getAddress().equals("")) {
			//name == null
			return new Entities(1,theCustomer, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			customerService.update(theCustomer);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theCustomer, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theCustomer, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theCustomer , HttpStatus.OK.getReasonPhrase());
		return entities;
		
//		customerService.save(theCustomer);
//		return theCustomer;
	}
	
	@DeleteMapping("/customers")
	public Entities deleteCustomer(@PathVariable int customerId){
		
		Customer customer = customerService.findById(customerId);
		if (customer == null) {
			throw new RuntimeException("Customer is not found with id = " + customerId);
		}
		return findAll();
	}
	
	@GetMapping("/getNewCustomerCode")
	public Entities getNewCompanyCode(){
		
		//length code Company = 6 (EX: "CU0001")
		String lastCode = "";
		String st_0 = "0";
		int lastId =  customerService.findNewCode();
		
		if (lastId == 0) {
			lastCode = "CU0001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 =  new String(new char[4 - temp.length()]).replace("\0", "0");
			lastCode = "CU" + st_0 + temp;
		}
		Customer customer = new Customer();
		customer.setCode(lastCode);
		HashMap<String,String> mapCode =new HashMap();
		mapCode.put("customerCode",customer.getCode() );
		Entities Entities = new Entities(0, mapCode , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
}




















