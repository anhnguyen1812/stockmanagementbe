package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.ShipmentNote;
import com.creasia.stock.entity.ShipmentNoteLine;
import com.creasia.stock.entity.ShipmentNote;
import com.creasia.stock.entity.ShipmentNoteLine;
import com.creasia.stock.entity.ReturnShipment;
import com.creasia.stock.entity.ReturnShipmentLine;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.service.ShipmentNoteService;
import com.creasia.stock.service.ShipmentNoteService;
import com.creasia.stock.service.ReturnShipmentService;
import com.creasia.stock.service.TransactionService;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ReturnShipmentRestController {

	private ReturnShipmentService returnShipmentService;
	private TransactionService transactionService;
	private ShipmentNoteService shipmentNoteService;

	@Autowired
	public ReturnShipmentRestController(ReturnShipmentService theReturnShipmentService, TransactionService theTransactionService,
			ShipmentNoteService theShipmentNoteService) {
		returnShipmentService = theReturnShipmentService;
		transactionService = theTransactionService;
		shipmentNoteService = theShipmentNoteService;
	}

	@GetMapping("/returnShipments")
	public Entities findAll() {

		List<ReturnShipment> returnShipmentList = returnShipmentService.findAll();
		if (returnShipmentList == null) {
			return new Entities(1, returnShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, returnShipmentList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/returnShipments/{returnShipmentId}")
	public Entities getReturnShipment(@PathVariable int returnShipmentId) {

		ReturnShipment returnShipment = returnShipmentService.findById(returnShipmentId);
		if (returnShipment == null) {
			return new Entities(1, returnShipment, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, returnShipment, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/returnShipments")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities addReturnShipment(@RequestBody ReturnShipment theReturnShipment) {
		if (theReturnShipment.getShipmentNote().isReturned()) {
			return new Entities(1, theReturnShipment, "Shipment was returned");
		}
		if (theReturnShipment.getShipmentNote().getPostedBy() == null
				||theReturnShipment.getShipmentNote().getPostedDate() == null 
				|| theReturnShipment.getShipmentNote().getTotalQuantity() == theReturnShipment.getShipmentNote().getTotalPostedQuantity()) {
			return new Entities(1, theReturnShipment, "The shipment has not been issued");
		}
		if (theReturnShipment.getReturnBy() == null) {
			return new Entities(1, theReturnShipment, "Body Invalid");
		}
		if (theReturnShipment.getShipmentNote() == null ){
			return new Entities(1, theReturnShipment, "Body Invalid");
		}
		
		System.out.println("shipment not:  " + theReturnShipment.getShipmentNote().getId());
		int shipmentNoteId = theReturnShipment.getShipmentNote().getId();
		
		//check shipment note co line nao co quantity bang 0
		//for (int i = 0; i < theReturnShipment.get)
		List<ReturnShipmentLine> returnShipmentLines = theReturnShipment.getReturnShipmentLines();
		int i = 0;
		while (i <returnShipmentLines.size()) {
			if(returnShipmentLines.get(i).getQuantity() == 0) {
				returnShipmentLines.remove(i);
			}
			i ++;
		}
		 //all item has quatity = 0 then error
		if (i == 0) {
			return new Entities(1, theReturnShipment, "No item choosed");
		}
		int totalQuantity = 0;
		for (ReturnShipmentLine rnLine : theReturnShipment.getReturnShipmentLines()) {
			totalQuantity += rnLine.getQuantity();
		}
		// all item has quatity = 0 then error
		if (totalQuantity == 0) {
			return new Entities(1, theReturnShipment, "No item choosed");
		} else {
			theReturnShipment.setTotalDelivered(totalQuantity);
			theReturnShipment.setTotalPosted(totalQuantity);
		}
		
		theReturnShipment.setCode(getNewReturnShipmentCode());
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theReturnShipment.setCreatedAt(dtf.format(now));
		if (theReturnShipment.getPostedDate() == null || theReturnShipment.getPostedDate() == "") {
			theReturnShipment.setDocumentAt(theReturnShipment.getCreatedAt());
			theReturnShipment.setPostedDate(theReturnShipment.getCreatedAt());
		} else {
			theReturnShipment.setDocumentAt(theReturnShipment.getPostedDate());
		}
	
		try {
			System.out.println ("the return shipment" + theReturnShipment);
			
			//insert transaction and update request shipment
			ShipmentNote rs = shipmentNoteService.findById(shipmentNoteId);
			System.out.println ("the ShipmentNote " + rs);
			rs.setReturned(true);
			Transaction transaction = new Transaction( 4, theReturnShipment.getCode(),"ReturnShipment",
					theReturnShipment.getShipmentNote().getApprovedShipment().getRequestShipment().getProject(), theReturnShipment.getReturnBy(), 
					theReturnShipment.getShipmentNote().getApprovedShipment().getRequestShipment().getStock(), 1, theReturnShipment.getCreatedAt(),
					theReturnShipment.getDocumentAt(),theReturnShipment.getDocumentAt());			
			
			transaction.setTotalQuantity(theReturnShipment.getTotalDelivered());
			transaction.setTotalPostedQuantity(theReturnShipment.getTotalDelivered());
			transaction.setPostedBy(theReturnShipment.getReturnBy());
			List<TransactionLine> transactionLines = new ArrayList<TransactionLine>();
			TransactionLine transactionLine;
			for (ReturnShipmentLine returnShipmentLine : theReturnShipment.getReturnShipmentLines()) {
				transactionLine = new TransactionLine();
				transactionLine.setItem(returnShipmentLine.getItem());
				transactionLine.setExpDate(returnShipmentLine.getExpDate());
				transactionLine.setItemExtNo(returnShipmentLine.getItemExtNo());
				transactionLine.setQuantity(returnShipmentLine.getQuantity());
				transactionLine.setPostedQuantity(returnShipmentLine.getQuantity());
				transactionLine.setUoM(returnShipmentLine.getUoM());
				transactionLines.add(transactionLine);
			}
			transaction.setTransactionLines(transactionLines);
			try {
			returnShipmentService.save(theReturnShipment);
			transactionService.save(transaction);			
			shipmentNoteService.update(rs);
			} catch (Exception e) {
				// TODO: handle exception
				return new Entities(1, theReturnShipment, "save and update error");
			}
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theReturnShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theReturnShipment, "Fail to server");
		}
		Entities entities = new Entities(0, theReturnShipment, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/returnShipments")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities updateReturnShipment(@RequestBody ReturnShipment theReturnShipment) {
//		returnShipmentService.save(theReturnShipment);
//		return theReturnShipment;

		if (theReturnShipment.getReturnBy() == null) {
			// name == null
			return new Entities(1, theReturnShipment, "Body Invalid");
		}
		int totalQuantity = 0;
		for (ReturnShipmentLine rnLine : theReturnShipment.getReturnShipmentLines()) {
			totalQuantity += rnLine.getQuantity();
		}
		if (totalQuantity == 0) {
			return new Entities(1, theReturnShipment, "No item choosed");
		} else {
			theReturnShipment.setTotalDelivered(totalQuantity);
		}
		try {
			returnShipmentService.update(theReturnShipment);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theReturnShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theReturnShipment, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theReturnShipment, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/returnShipments/{returnShipmentId}")
	public Entities deleteReturnShipment(@PathVariable int returnShipmentId) {

		ReturnShipment returnShipment = returnShipmentService.findById(returnShipmentId);
		if (returnShipment == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + returnShipmentId);
		}

		return findAll();
	}

	@GetMapping("/getNewReturnShipmentCode")
	public Entities apiGetNewReturnShipmentCode() {

		// length code SHIPMENT_NOTE = 10 (EX: "SN00000001")
		String lastCode = getNewReturnShipmentCode();
		ReturnShipment returnShipment = new ReturnShipment();
		returnShipment.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("returnShipmentCode", returnShipment.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	public String getNewReturnShipmentCode() {
		String newCode;
		String lastCode = returnShipmentService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if(lastCode == "") {
			newCode = "RS/" + year + "/000001";
		}else  {
			String  headerCode = lastCode.substring(3, 5);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(6);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				st_0 =  new String(new char[6 - newMainCode.length()]).replace("\0", "0");
				newCode = "RS/" + year + st_0 + newMainCode;
			} else {
				newCode = "RS/" + year + "/000001";
			}
			//System.out.println("newCode "+ newCode);
		}
		return newCode;
	}
}
