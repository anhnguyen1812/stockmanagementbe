package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.service.EmployeeService;

@RestController
@RequestMapping("api")
public class EmployeeRestController {

	private EmployeeService employeeService;

	@Autowired
	public EmployeeRestController(EmployeeService theEmployeeService) {
		employeeService = theEmployeeService;
	}

	@GetMapping("/employees")
	public Entities findAll() {

		List<Employee> employeeList = employeeService.findAll();
		if (employeeList == null) {
			return new Entities(1, employeeList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, employeeList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/employees/{employeeId}")
	public Entities getEmployee(@PathVariable int employeeId) {

		Employee employee = employeeService.findById(employeeId);
		if (employee == null) {
			return new Entities(1, employee, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, employee, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/employees")
	public Entities addEmployee(@RequestBody Employee theEmployee) {
//		theEmployee.setId(0);
//		employeeService.save(theEmployee);
//		return theEmployee;
		System.out.println("theEmployee: " + theEmployee);

		if (theEmployee.getName().equals("") || theEmployee.getDistrict() == null) {
			// name == null
			return new Entities(1, theEmployee, "Body Invalid");
		}
		if (theEmployee.getDepartment() == null || theEmployee.getEmail() == null || theEmployee.getManager() == null) {
			return new Entities(1, theEmployee, "Body Invalid");
		}
		if (theEmployee.getManager().getId() == 0) {
			theEmployee.setManager(null);
		}
		if (theEmployee.getBirthday() == null) {
			theEmployee.setBirthday("");
		}
		if (theEmployee.getAddress() == null) {
			theEmployee.setAddress("");
		}
		if (theEmployee.getPosition() == null) {
			theEmployee.setPosition("");
		}
		theEmployee.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theEmployee.setCreatedAt(dtf.format(now));
		try {
			employeeService.save(theEmployee);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theEmployee, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theEmployee, "Fail to server");
		}
		Entities entities = new Entities(0, theEmployee, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/employees")
	public Entities updateEmployee(@RequestBody Employee theEmployee) {
//		employeeService.save(theEmployee);
//		return theEmployee;

		System.out.println("theEmployee: " + theEmployee);

		if (theEmployee.getName().equals("") || theEmployee.getDistrict() == null) {
			// name == null
			return new Entities(1, theEmployee, "Body Invalid");
		}
		
		if (theEmployee.getName().equals("") || theEmployee.getDistrict() == null || theEmployee.getManager() == null) {
			// name == null
			return new Entities(1, theEmployee, "Body Invalid");
		}
		if (theEmployee.getDepartment() == null || theEmployee.getEmail() == null
				|| theEmployee.getEmail().equals("")) {
			return new Entities(1, theEmployee, "Body Invalid");
		}
		if (theEmployee.getId() == theEmployee.getManager().getId()) {
			return new Entities(1, theEmployee, "Manager must different Employee");
		}
		System.out.println("theEmployee: manager:: " + theEmployee.getManager().getId());
		if (theEmployee.getManager().getId() == 0) {
			theEmployee.setManager(null);
		}
		if (theEmployee.getBirthday() == null) {
			theEmployee.setBirthday("");
		}
		if (theEmployee.getAddress() == null) {
			theEmployee.setAddress("");
		}
		if (theEmployee.getPosition() == null) {
			theEmployee.setPosition("");
		}
		
		try {
			System.out.println("theEmployee: update:: " + theEmployee);
			employeeService.update(theEmployee);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theEmployee, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theEmployee, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theEmployee, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/employees/{employeeId}")
	public Entities deleteEmployee(@PathVariable int employeeId) {

		Employee employee = employeeService.findById(employeeId);
		if (employee == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + employeeId);
		}

		return findAll();
	}

	@GetMapping("/getNewEmployeeCode")
	public Entities getNewEmployeeCode() {

		// length code Company = 6 (EX: "EM0001")
		String lastCode = "";
		String st_0 = "0";
		int lastId = employeeService.findNewCode();

		if (lastId == 0) {
			lastCode = "EM0001";
		} else {
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 = new String(new char[4 - temp.length()]).replace("\0", "0");
			lastCode = "EM" + st_0 + temp;
		}
		Employee employee = new Employee();
		employee.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("employeeCode", employee.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	@GetMapping("/employees/pms")
	public Entities findPM() {

		List<Employee> employeeList = employeeService.findAll();
		if (employeeList == null) {
			return new Entities(1, employeeList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			List<Employee> PMs = new ArrayList<Employee>();

			for (Employee e : employeeList) {
//				if (e.getPermission().getId() == 2) {
//					PMs.add(e);
//				}
			}
			return new Entities(0, PMs, HttpStatus.OK.getReasonPhrase());
		}
	}
}
