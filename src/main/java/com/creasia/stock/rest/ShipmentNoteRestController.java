package com.creasia.stock.rest;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.ApprovedShipment;
import com.creasia.stock.entity.ApprovedShipmentLine;
import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.RequestShipment;
import com.creasia.stock.entity.RequestShipmentLine;
import com.creasia.stock.entity.ShipmentNote;
import com.creasia.stock.entity.ShipmentNoteLine;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.export.PdfExporter;
import com.creasia.stock.service.ApprovedShipmentService;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.RequestShipmentService;
import com.creasia.stock.service.ShipmentNoteService;
import com.creasia.stock.service.TransactionService;
import com.creasia.stock.util.Email;
import com.lowagie.text.DocumentException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ShipmentNoteRestController {

	private ShipmentNoteService shipmentNoteService;
	private ApprovedShipmentService approvedShipmentService;
	private TransactionService transactionService;
	private RequestShipmentService requestShipmentService;
	private PermissionAssignmentService permissionAssignmentService; 
	private Email email = new Email();

	@Autowired
	public ShipmentNoteRestController(ShipmentNoteService theShipmentNoteService,
			TransactionService theTransactionService, RequestShipmentService theRequestShipmentService,
			ApprovedShipmentService theApprovedShipmentService, PermissionAssignmentService thePermissionAssignmentService) {
		shipmentNoteService = theShipmentNoteService;
		transactionService = theTransactionService;
		requestShipmentService = theRequestShipmentService;
		approvedShipmentService = theApprovedShipmentService;
		permissionAssignmentService =  thePermissionAssignmentService;
	}

	@GetMapping("/shipmentNotes")
	public Entities findAll() {

		List<ShipmentNote> shipmentNoteList = shipmentNoteService.findAll();
		if (shipmentNoteList == null) {
			return new Entities(1, shipmentNoteList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, shipmentNoteList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/countShipmentNotes")
	public Entities countShipmentNotes() {
		long shipmentNote = shipmentNoteService.countShipmentNotes();
		System.out.println("item List:: " + shipmentNote);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countShipmentNote", shipmentNote);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}

//	@GetMapping("/approvedShipments")
//	public Entities findApprovedShipment() {
//
//		List<ShipmentNote> shipmentNoteList = shipmentNoteService.findApprovedShipment();
//		if (shipmentNoteList == null) {
//			return new Entities(1, shipmentNoteList, HttpStatus.NOT_FOUND.getReasonPhrase());
//		} else {
//			return new Entities(0, shipmentNoteList, HttpStatus.OK.getReasonPhrase());
//		}
//	}

	@GetMapping("/postedShipments")
	public Entities findPostedShipment() {

		List<ShipmentNote> shipmentNoteList = shipmentNoteService.findPostedShipment();
		if (shipmentNoteList == null) {
			return new Entities(1, shipmentNoteList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, shipmentNoteList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/shipmentNotes/getByRequestId/{requestId}")
	public Entities findByRequestShipmentId(@RequestParam("requestId") int requestId) {

		List<ShipmentNote> shipmentNoteList = shipmentNoteService.findByRequestShipmentId(requestId);
		if (shipmentNoteList == null) {
			return new Entities(1, shipmentNoteList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, shipmentNoteList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/shipmentNotes/{shipmentNoteId}")
	public Entities getShipmentNote(@PathVariable int shipmentNoteId) {

		ShipmentNote shipmentNote = shipmentNoteService.findById(shipmentNoteId);
		if (shipmentNote == null) {
			return new Entities(1, shipmentNote, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, shipmentNote, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/findInventoryByItemAndExpDate")
	public int findInventoryByItemAndExpDate(@RequestParam("projectId") int projectId,
			@RequestParam("stockId") int stockId, @RequestParam("itemId") int itemId, @RequestParam("uomId") int uomId,
			@RequestParam("expDate") String expDate) {

		int inventoryQty = (int) transactionService.findInventoryByItemAndExpDate(projectId, stockId, itemId, uomId,
				expDate);
//		if (shipmentNote == null) {
//			return new Entities(1, shipmentNote, HttpStatus.NOT_FOUND.getReasonPhrase());
//
//		} else {
		return inventoryQty;
//		}
	}

	@PostMapping("/shipmentNotes")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities addShipmentNote(@RequestBody ShipmentNote theShipmentNote) {
		System.out.println("theShipmentNote::: " + theShipmentNote);
		if (theShipmentNote.getApproveBy() == null) {
			return new Entities(1, theShipmentNote, "Body Invalid");
		}
		if (theShipmentNote.getApprovedShipment().getRequestShipment().getCreateBy() == null) {
			return new Entities(1, theShipmentNote, "Body Invalid");
		}
		try {
			Stock stock = theShipmentNote.getApprovedShipment().getRequestShipment().getStock();
			Project project = theShipmentNote.getApprovedShipment().getRequestShipment().getProject();
			// check item inventory
			for (ShipmentNoteLine appLine : theShipmentNote.getShipmentNoteLines()) {
				int inventoryQty = (int) transactionService.findInventoryByItemAndExpDate(project.getId(),
						stock.getId(), appLine.getItem().getId(), appLine.getUoM().getId(), appLine.getExpDate());
				if (inventoryQty < appLine.getQuantity()) {
					return new Entities(1, theShipmentNote, "Have some item not inventory");
				}
			}

			// insert to shipment
			List<ShipmentNoteLine> shipmentNoteLines = theShipmentNote.getShipmentNoteLines();
			RequestShipment rs = theShipmentNote.getApprovedShipment().getRequestShipment();

			ApprovedShipment approvedShipment = approvedShipmentService
					.findById(theShipmentNote.getApprovedShipment().getId());
			approvedShipment.setPosted(true);
			theShipmentNote.setApprovedShipment(approvedShipment);
			int totalQuantity = 0;
			for (ShipmentNoteLine rnLine : theShipmentNote.getShipmentNoteLines()) {
				totalQuantity += rnLine.getQuantity();

			}
			for (int i = 0; i < theShipmentNote.getShipmentNoteLines().size(); i++) {
				theShipmentNote.getShipmentNoteLines().get(i)
						.setPostedQuantity(theShipmentNote.getShipmentNoteLines().get(i).getQuantity());
			}
			theShipmentNote.setCode(getNewShipmentNoteCode());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();

			if (theShipmentNote.getPostedDate() == null) {
				theShipmentNote.setPostedDate(dtf.format(now));
			} else {
				theShipmentNote.setPostedDate(theShipmentNote.getPostedDate());
			}
			theShipmentNote.setCreatedAt(dtf.format(now));
			theShipmentNote.setDocumentAt(dtf.format(now));
			// insert transaction and update request shipment
			int totalQuantityRequest = 0;
			for (RequestShipmentLine rsl : rs.getRequestShipmentLines()) {
				totalQuantityRequest = totalQuantityRequest + rsl.getQuantity();
			}
			rs.setTotalQuantity(totalQuantityRequest);
			Transaction transaction = new Transaction(2, theShipmentNote.getCode(), "Shipment",
					theShipmentNote.getApprovedShipment().getRequestShipment().getProject(),
					theShipmentNote.getApproveBy(),
					theShipmentNote.getApprovedShipment().getRequestShipment().getStock(), -1,
					theShipmentNote.getCreatedAt(), theShipmentNote.getDocumentAt(), theShipmentNote.getPostedDate());
			List<TransactionLine> transactionLines = new ArrayList<TransactionLine>();
			TransactionLine transactionLine;
			for (ShipmentNoteLine shipmentNoteLine : theShipmentNote.getShipmentNoteLines()) {
				transactionLine = new TransactionLine();
				transactionLine.setItem(shipmentNoteLine.getItem());
				transactionLine.setQuantity(shipmentNoteLine.getQuantity() * (-1));
				transactionLine.setPostedQuantity(shipmentNoteLine.getQuantity() * (-1));
				transactionLine.setUoM(shipmentNoteLine.getUoM());

				transactionLine.setItemExtNo(shipmentNoteLine.getItemExtNo());
				transactionLine.setExpDate(shipmentNoteLine.getExpDate());
				transactionLines.add(transactionLine);

				for (int j = 0; j < rs.getRequestShipmentLines().size(); j++) {
					if (shipmentNoteLine.getItem().getId() == rs.getRequestShipmentLines().get(j).getItem().getId()) {
						rs.getRequestShipmentLines().get(j).setShiptedQty(
								rs.getRequestShipmentLines().get(j).getShiptedQty() + shipmentNoteLine.getQuantity());
						rs.getRequestShipmentLines().get(j).setOutStdQty(
								rs.getRequestShipmentLines().get(j).getOutStdQty() - shipmentNoteLine.getQuantity());
					}
				}
			}
			transaction.setTotalQuantity(totalQuantity * (-1));
			transaction.setTotalPostedQuantity(totalQuantity * (-1));
			transaction.setTransactionLines(transactionLines);
			try {
				shipmentNoteService.save(theShipmentNote);
				transactionService.save(transaction);
				approvedShipmentService.update(approvedShipment);

			} catch (Exception e) {
				// TODO: handle exception
				return new Entities(1, theShipmentNote, "Fail to server");
			}
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theShipmentNote, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theShipmentNote, "Fail to server");
		}
		Entities entities = new Entities(0, theShipmentNote, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/shipmentNotes")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities updateShipmentNote(@RequestBody ShipmentNote theShipmentNote) {
		if (theShipmentNote.getId() == 0) {
			return new Entities(1, theShipmentNote, "body is null");
		}
		try {
			ShipmentNote shipment = shipmentNoteService.findById(theShipmentNote.getId());

			if (theShipmentNote.getPostedDate() == null) {
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				shipment.setPostedDate(dtf.format(now));
			} else {
				shipment.setPostedDate(theShipmentNote.getPostedDate());
			}
			shipment.setPostedBy(theShipmentNote.getPostedBy());
			shipment.setTotalPostedQuantity(shipment.getTotalQuantity());

			System.out.println("the shipment Note: " + theShipmentNote.getPostedDate());
			System.out.println(" shipment : " + shipment.getPostedDate());

			System.out.println("shipment code " + shipment.getCode());
			Transaction transaction = transactionService.findByNoteCode(shipment.getCode());
			transaction.setPostedDate(shipment.getPostedDate());
			transaction.setDocumentGroup(-1);
			transaction.setPostedBy(theShipmentNote.getPostedBy());
			transaction.setTotalPostedQuantity(transaction.getTotalQuantity());

			int postedQuantity = 0;
			for (int i = 0; i < shipment.getShipmentNoteLines().size(); i++) {
				postedQuantity = shipment.getShipmentNoteLines().get(i).getQuantity();
				shipment.getShipmentNoteLines().get(i).setPostedQuantity(postedQuantity);
			}
			for (int i = 0; i < transaction.getTransactionLines().size(); i++) {
				postedQuantity = transaction.getTransactionLines().get(i).getQuantity();
				transaction.getTransactionLines().get(i).setPostedQuantity(postedQuantity);
				transaction.getTransactionLines().get(i).setExpDate(null);
			}
			try {
				shipmentNoteService.update(shipment);
				transactionService.update(transaction);
				System.out.println(
						"shipment posted := " + shipment.getTotalPostedQuantity() + " " + shipment.getPostedDate());
				System.out.println("transaction := " + transaction);
				System.out.println("shipment := " + shipment);
			} catch (OptimisticLockingFailureException e) {
				return new Entities(1, theShipmentNote, "Fail to server");
			} catch (StaleObjectStateException e) {
				return new Entities(1, theShipmentNote, "Fail to server");
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, theShipmentNote, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theShipmentNote, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@GetMapping(value = "/sendEmailToRequestCreater")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities sendEmailToRequestCreater(@RequestParam("shipmentId") int shipmentId)
			throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		try {
			ShipmentNote shipmentNote = shipmentNoteService.findById(shipmentId);
			if (shipmentNote == null) {
				return new Entities(1, 0, "No Shipment");
			}
			RequestShipment request = shipmentNote.getApprovedShipment().getRequestShipment();
			String name = request.getCreateBy().getName();
			String requestCode = request.getCode();
			String shipmentCode = shipmentNoteService.findById(shipmentId).getCode();
			Employee empRequest = request.getCreateBy();
			Employee empManagerRequest = empRequest.getManager();
			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
			if (listReceive.indexOf(empRequest) != -1) {
				listReceive.remove(empRequest);
			}
			if (listReceive.indexOf(empRequest) == -1) {
				listReceive.add(empRequest);
			}
			// send email
			String subject = "Notification Shipment has been posted";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;' >Your request <a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/requestShipment\"> "
					+ requestCode + "</a> has been delivered complete! </p>"
					+ "<p style=' font-family: Arial;'> Please note with thanks! </p>"
					+ "<p style=' font-family: Arial;'> Your Shipment Note Code <a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/shipmentNote\">"
					+ shipmentCode + "</a> </p> <br>" 
					+ "<p style=' font-family: Arial;'>Thanks & regards!</p>" 
					+ "<p style=' font-family: Arial;'>Tech Teams</p>";
//			String content3 = "<p><a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/shipmentNote\">shipmentNote</a></p>";

			System.out.println("subject: " + subject + " emailPM " + empRequest);
			List<String> emails = new ArrayList<String>();
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}
			email.sendmail(subject, "<h4 style=' font-family: Arial;'>" + content1 + name + ", </h4>" + content2, emails, empRequest.getEmail());
			entities = new Entities(0, 0, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			return new Entities(1, 0, "Error send email");
		}

	}

	@DeleteMapping("/shipmentNotes/{shipmentNoteId}")
	public Entities deleteShipmentNote(@PathVariable int shipmentNoteId) {

		ShipmentNote shipmentNote = shipmentNoteService.findById(shipmentNoteId);
		if (shipmentNote == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + shipmentNoteId);
		}

		return findAll();
	}

	@GetMapping("/getNewShipmentNoteCode")
	public Entities apiGetNewShipmentNoteCode() {

		// length code SHIPMENT_NOTE = 10 (EX: "SN00000001")
		String lastCode = getNewShipmentNoteCode();
		ShipmentNote shipmentNote = new ShipmentNote();
		shipmentNote.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("shipmentNoteCode", shipmentNote.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	public String getNewShipmentNoteCode() {
		String newCode;
		String lastCode = shipmentNoteService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if (lastCode == "") {
			newCode = "SN/" + year + "/000001";
		} else {
			String headerCode = lastCode.substring(3, 5);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(6);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				st_0 = new String(new char[6 - newMainCode.length()]).replace("\0", "0");
				newCode = "SN/" + year + "/" + st_0 + newMainCode;
			} else {
				newCode = "SN/" + year + "/000001";
			}
			// System.out.println("newCode " + newCode);
		}
		return newCode;
	}

	@GetMapping("/processExportPdf")
	public void processExportPdf(HttpServletResponse response, @RequestParam("shipmentNoteId") int shipmentNoteId)
			throws DocumentException, IOException {
		ShipmentNote shipmentNote = shipmentNoteService.findById(shipmentNoteId);
		System.out.println("result shipmentNoteId: " + shipmentNote.getId());

		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		System.out.println("currentDateTime: " + currentDateTime);

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=ShipmentNote_" + currentDateTime + ".pdf";
		System.out.println("headerValue: " + headerValue);
		response.setHeader(headerKey, headerValue);
		//
		//
		System.out.println("response.getContentType: " + response.getContentType());
		PdfExporter exporter = new PdfExporter();
		System.out.println("exporter: " + exporter);
		exporter.export(response, shipmentNote);
		System.out.println("exporter: " + exporter.toString());
	}

}
