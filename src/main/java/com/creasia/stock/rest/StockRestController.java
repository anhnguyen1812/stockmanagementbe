package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.Vendor;
import com.creasia.stock.service.StockService;

@RestController
@RequestMapping("api")
public class StockRestController {

	private StockService stockService;

	@Autowired
	public StockRestController(StockService theStockService) {
		stockService = theStockService;
	}

	@GetMapping("/stocks")
	public Entities findAll() {
		List<Stock> stockList = stockService.findAll();
		if (stockList == null) {
			return new Entities(1, stockList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, stockList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/countStocks")
	public Entities countStocks() {
		long count = stockService.countStock();
		System.out.println("project List:: " + count);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countStock", count);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}
	
	@GetMapping("/stocks/{stockId}")
	public Entities getStock(@PathVariable int stockId) {
		Stock stock = stockService.findById(stockId);
		// Vendor stock = vendorService.findById(vendorId);
		if (stock == null) {
			return new Entities(1, stock, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, stock, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/stocks")
	public Entities addStock(@RequestBody Stock theStock) {
//		theStock.setId(0);
//		stockService.save(theStock);
//		return theStock;
		
		if (theStock.getName().equals("") || theStock.getDistrict().getProvince().equals(null)
				|| theStock.getDistrict().equals(null) || theStock.getAddress().equals(null)) {
			// name == null
			return new Entities(1, theStock, "Body Invalid");
		}
		if (theStock.getAddress() == null ) {
			theStock.setAddress("");
		}
		Stock stock = new Stock(getCodeStock(theStock), theStock.getName(), theStock.getAddress(), theStock.getCreatedAt(), false);
		stock.setId(0);
		stock.setDistrict(theStock.getDistrict());
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		stock.setCreatedAt(dtf.format(now));
		//System.out.println("theStock.getDistrict() : " + theStock.getDistrict());
		try {
			stockService.save(stock);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, stock, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, stock, "Fail to server");
		}
		Entities entities = new Entities(0, stock, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/stocks")
	public Entities updateStock(@RequestBody Stock theStock) {
//		stockService.save(theStock);
//		return theStock;
		if (theStock.getName().equals("") || theStock.getDistrict().getProvince().equals(null)
				|| theStock.getDistrict().equals(null) || theStock.getAddress().equals("")) {
			// name == null
			return new Entities(1, theStock, "Body Invalid");
		}
		try {
			stockService.update(theStock);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theStock, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theStock, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theStock, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/stocks")
	public Entities deleteStock(@PathVariable int stockId) {
		Stock stock = stockService.findById(stockId);
		if (stock == null) {
			throw new RuntimeException("Stock is not found with id = " + stockId);
		}
		return findAll();
	}

	@GetMapping("/getNewStockCode")
	public String getCodeStock(Stock theStock) {
		String code = "";
		String headerCode = theStock.getDistrict().getProvince().getNameSearch();
		int numberOfHearder = stockService.findNewCode(headerCode);
		if (numberOfHearder == 0) {
			code = headerCode + "01";
		}else {
			numberOfHearder += 1;
			if (numberOfHearder < 10) {
				code = headerCode + "0" + String.valueOf(numberOfHearder);
			} else {
				code = headerCode + "" + String.valueOf(numberOfHearder);
			}
		}
		return code;
	}
}
