package com.creasia.stock.rest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.ReceiptNote;
import com.creasia.stock.entity.ReceiptNoteLine;
import com.creasia.stock.entity.RequestShipment;
import com.creasia.stock.entity.ShipmentNote;
import com.creasia.stock.entity.ShipmentNoteLine;
import com.creasia.stock.entity.ReturnReceipt;
import com.creasia.stock.entity.ReturnReceiptLine;
import com.creasia.stock.entity.Transaction;
import com.creasia.stock.entity.TransactionLine;
import com.creasia.stock.service.ShipmentNoteService;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.ReceiptNoteService;
import com.creasia.stock.service.ReturnReceiptService;
import com.creasia.stock.service.TransactionService;
import com.creasia.stock.util.Email;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ReturnReceiptRestController {

	private PermissionAssignmentService permissionAssignmentService;
	private ReturnReceiptService returnReceiptService;
	private TransactionService transactionService;
	private ReceiptNoteService receiptNoteService;
	private Email email = new Email();

	@Autowired
	public ReturnReceiptRestController(ReturnReceiptService theReturnReceiptService,
			TransactionService theTransactionService, ReceiptNoteService theReceiptNoteService,
			PermissionAssignmentService thePermissionAssignmentService) {
		returnReceiptService = theReturnReceiptService;
		transactionService = theTransactionService;
		receiptNoteService = theReceiptNoteService;
		permissionAssignmentService = thePermissionAssignmentService;
	}

	@GetMapping("/returnReceipts")
	public Entities findAll() {

		List<ReturnReceipt> returnReceiptList = returnReceiptService.findAll();
		if (returnReceiptList == null) {
			return new Entities(1, returnReceiptList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, returnReceiptList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/returnReceipts/{returnReceiptId}")
	public Entities getReturnReceipt(@PathVariable int returnReceiptId) {

		ReturnReceipt returnReceipt = returnReceiptService.findById(returnReceiptId);
		if (returnReceipt == null) {
			return new Entities(1, returnReceipt, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, returnReceipt, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/returnReceipts")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities addReturnReceipt(@RequestBody ReturnReceipt theReturnReceipt) {
//		System.out.print("theReturn note: ", theReturnReceipt.getNote());
		if (theReturnReceipt.getNote().equals("null") || theReturnReceipt.getNote() == "") {
			return new Entities(1, theReturnReceipt, "Note is not NULL");
		}
		if (theReturnReceipt.getReceiptNote().isReturned()) {
			return new Entities(1, theReturnReceipt, "Receipt was returned");
		}
		if (theReturnReceipt.getReturnBy() == null) {
			return new Entities(1, theReturnReceipt, "Body Invalid");
		}
		if (theReturnReceipt.getReceiptNote() == null) {
			return new Entities(1, theReturnReceipt, "Body Invalid");
		}

		System.out.println("receipt not:  " + theReturnReceipt.getReceiptNote().getId());
		int receiptNoteId = theReturnReceipt.getReceiptNote().getId();

		// check shipment note co line nao co quantity bang 0
		// for (int i = 0; i < theReturnReceipt.get)
		List<ReturnReceiptLine> returnReceiptLines = theReturnReceipt.getReturnReceiptLines();
		int i = 0;
		while (i < returnReceiptLines.size()) {
			if (returnReceiptLines.get(i).getQuantity() == 0) {
				returnReceiptLines.remove(i);
			}
			i++;
		}
		// all item has quatity = 0 then error
		if (i == 0) {
			return new Entities(1, theReturnReceipt, "No item choosed");
		}
		int totalQuantity = 0;
		for (int x = 0; x < theReturnReceipt.getReturnReceiptLines().size(); x++) {
			totalQuantity += theReturnReceipt.getReturnReceiptLines().get(x).getQuantity();
			theReturnReceipt.getReturnReceiptLines().get(x).setPostedQuantity(0);
		}
		// all item has quatity = 0 then error
		if (totalQuantity == 0) {
			return new Entities(1, theReturnReceipt, "No item choosed");
		} else {
			if (theReturnReceipt.getTotalDelivered() != totalQuantity) {
				theReturnReceipt.setTotalDelivered(totalQuantity);
			}
			theReturnReceipt.setTotalPosted(0);
		}

		theReturnReceipt.setCode(getNewReturnReceiptCode());

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theReturnReceipt.setCreatedAt(dtf.format(now));
		theReturnReceipt.setDocumentAt(theReturnReceipt.getPostedDate());
		theReturnReceipt.setApprovedAt(null);
		theReturnReceipt.setApprovedBy(null);
		theReturnReceipt.setReturned(false);
		try {
			System.out.println("the return receipt" + theReturnReceipt);

			// insert transaction and update request shipment
			ReceiptNote rs = receiptNoteService.findById(receiptNoteId);
			System.out.println("the ReceiptNote " + rs);
			rs.setReturned(true);
			try {
				returnReceiptService.save(theReturnReceipt);
//			transactionService.save(transaction);			
				receiptNoteService.update(rs);
			} catch (Exception e) {
				// TODO: handle exception
				return new Entities(1, theReturnReceipt, "save and update error");
			}
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theReturnReceipt, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theReturnReceipt, "Fail to server");
		}
		Entities entities = new Entities(0, theReturnReceipt, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/returnReceipts")
	@PreAuthorize("hasRole('ADMIN')")
	public Entities updateReturnReceipt(@RequestBody ReturnReceipt theReturnReceipt) {
//		returnReceiptService.save(theReturnReceipt);
//		return theReturnReceipt;
		if (theReturnReceipt == null) {
			return new Entities(1, theReturnReceipt, "Not Return Receipt");
		}
		ReturnReceipt returnReceiptNote = returnReceiptService.findById(theReturnReceipt.getId());
		if (returnReceiptNote == null) {
			return new Entities(1, theReturnReceipt, "not find this return receipt");
		}
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			returnReceiptNote.setApprovedAt(dtf.format(now));
			returnReceiptNote.setApprovedBy(theReturnReceipt.getApprovedBy());
			returnReceiptNote.setReturned(true);
			returnReceiptNote.setTotalPosted(returnReceiptNote.getTotalDelivered());
			List<ReturnReceiptLine> rrlList = returnReceiptNote.getReturnReceiptLines();
			for (int i = 0; i < rrlList.size(); i++) {
				rrlList.get(i).setPostedQuantity(rrlList.get(i).getQuantity());
			}
			returnReceiptNote.setReturnReceiptLines(rrlList);
			returnReceiptService.update(returnReceiptNote);

			// ---------- add transaction -----------//
			Transaction transaction = new Transaction(3, returnReceiptNote.getCode(), "ReturnReceipt",
					returnReceiptNote.getReceiptNote().getProject(), returnReceiptNote.getReturnBy(),
					returnReceiptNote.getReceiptNote().getStock(), -1, returnReceiptNote.getApprovedAt(),
					returnReceiptNote.getDocumentAt(), returnReceiptNote.getApprovedAt());

			transaction.setTotalQuantity(returnReceiptNote.getTotalDelivered() * (-1));
			transaction.setTotalPostedQuantity(returnReceiptNote.getTotalDelivered() * (-1));
			transaction.setPostedBy(returnReceiptNote.getReturnBy());
			List<TransactionLine> transactionLines = new ArrayList<TransactionLine>();
			TransactionLine transactionLine;
			for (ReturnReceiptLine returnReceiptLine : returnReceiptNote.getReturnReceiptLines()) {
				transactionLine = new TransactionLine();
				transactionLine.setItem(returnReceiptLine.getItem());
				transactionLine.setExpDate(returnReceiptLine.getExpDate());
				transactionLine.setItemExtNo(returnReceiptLine.getItemExtNo());
				transactionLine.setQuantity(returnReceiptLine.getQuantity() * (-1));
				transactionLine.setPostedQuantity(returnReceiptLine.getQuantity() * (-1));
				transactionLine.setUoM(returnReceiptLine.getUoM());
				transactionLines.add(transactionLine);
			}
			transaction.setTransactionLines(transactionLines);

			transactionService.save(transaction);
			// ---------- end add transaction -----------//
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theReturnReceipt, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theReturnReceipt, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theReturnReceipt, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@GetMapping("/getNewReturnReceiptCode")
	public Entities apiGetNewReturnReceiptCode() {

		// length code SHIPMENT_NOTE = 10 (EX: "SN00000001")
		String lastCode = getNewReturnReceiptCode();
		ReturnReceipt returnReceipt = new ReturnReceipt();
		returnReceipt.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("returnReceiptCode", returnReceipt.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	public String getNewReturnReceiptCode() {
		String newCode;
		String lastCode = returnReceiptService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if (lastCode == "") {
			newCode = "RR/" + year + "/000001";
		} else {
			String headerCode = lastCode.substring(3, 5);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(6);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				st_0 = new String(new char[6 - newMainCode.length()]).replace("\0", "0");
				newCode = "RR/" + year + "/" + st_0 + newMainCode;
			} else {
				newCode = "RR/" + year + "/000001";
			}
			// System.out.println("newCode "+ newCode);
		}
		return newCode;
	}

	@GetMapping(value = "/sendemailToApproveReturnReceipt")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities sendemailToApproveReturnReceipt(@RequestParam("returnId") int returnId)
			throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		try {

			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
			System.out.println("listReceive: " + listReceive);
			ReturnReceipt rr = returnReceiptService.findById(returnId);
			System.out.println("RR: " + rr);
//			Employee manager = permissionAssignmentService.getUserManager(rs.getCreateBy().getId());
			Employee emp = rr.getReturnBy();
			if (emp == null) {
				return new Entities(1, 0, "No Create By");
			}
//			Employee manager = permissionAssignmentService.getUserManager(rs.getCreateBy().getId());
			Employee manager = emp.getManager();
			System.out.println("manager: " + manager);
			if (manager == null) {
				manager = emp;
			}
			if (listReceive.indexOf(manager) != -1) {
				listReceive.remove(manager);
			}
			String returnCode = rr.getCode();
			String name = manager.getName();
			// send email
			String subject = "Approve Request Return Receipt Note";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;'> Request Return Receipt <a href=\"http://inv.creasia.vn:3000/admin/stock/receipt/returnReceipt\"> "
					+ returnCode + "</a> has been created by " + emp.getName() + ".</p>"
					+ "<p style=' font-family: Arial;' > Please go to system to check and Approve it! </p> <br>"
					+ "<p style=' font-family: Arial;' >Thanks & regards!</p>"
					+ "<p style=' font-family: Arial;' >Tech Teams</p>";
			List<String> emails = new ArrayList<String>();
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}

			System.out.println("subject: " + subject + " emails " + emails);
			email.sendmail(subject, "<h4 style='font-family: Arial;'>" + content1 + name + ", </h4>" + content2, emails,
					manager.getEmail());
			entities = new Entities(0, emails, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			return new Entities(1, 0, "Error send email");
		}
	}
	
	@GetMapping(value = "/sendemailApprovedToCreateReturn")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities sendemailApprovedToCreateReturn(@RequestParam("returnId") int returnId)
			throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		try {
			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
			System.out.println("listReceive: " + listReceive);
			ReturnReceipt rr = returnReceiptService.findById(returnId);
			System.out.println("RR: " + rr);
			Employee emp = rr.getReturnBy();
			if (emp == null) {
				return new Entities(1, 0, "No Create By");
			}
			if (listReceive.indexOf(emp) != -1) {
				listReceive.remove(emp);
			}
			String returnCode = rr.getCode();
			String name = emp.getName();
			// send email
			String subject = "Notification Request Return Receipt has been Approved";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;'> Your Request Return Receipt <a href=\"http://inv.creasia.vn:3000/admin/stock/receipt/returnReceipt\"> "
					+ returnCode + "</a> has been approved.</p>"
					+ "<p style=' font-family: Arial;' > Please go to system to check and return it! </p> <br>"
					+ "<p style=' font-family: Arial;' >Thanks & regards!</p>"
					+ "<p style=' font-family: Arial;' >Tech Teams</p>";
			List<String> emails = new ArrayList<String>();
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}
			System.out.println("subject: " + subject + " emails " + emails);
			email.sendmail(subject, "<h4 style='font-family: Arial;'>" + content1 + name + ", </h4>" + content2, emails,
					emp.getEmail());
			entities = new Entities(0, emails, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			return new Entities(1, 0, "Error send email");
		}
	}
}
