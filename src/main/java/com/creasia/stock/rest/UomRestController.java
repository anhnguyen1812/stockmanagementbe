package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.UoM;
import com.creasia.stock.service.UomService;

@RestController
@RequestMapping("api")
public class UomRestController {

	private UomService uomService;
	
	@Autowired
	public UomRestController(UomService theUomService) {
		uomService = theUomService;
	}
	
	@GetMapping("UoMs")
	public Entities findAll(){
		
		List<UoM> uoMList = uomService.findAll();
		if(uoMList == null) {
			return new Entities(1, uoMList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}
		else {
			return new Entities(0, uoMList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/UoMs/{id}")
	public Entities findById(@RequestBody int uomId) {
		UoM theUoM = uomService.findById(uomId);
//		if (theUoM == null) {
//			throw new RuntimeException("ItemCategory is not found with id = "+uomId);
//		}
		if (theUoM == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, theUoM, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, theUoM, HttpStatus.OK.getReasonPhrase());
		}
		//return theUoM;
	}
	
	@PostMapping("/UoMs")
	public Entities addUoM(@RequestBody UoM theUom) {
		if (theUom.getName().equals("") ) {
			//name == null
			return new Entities(1,null, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		theUom.setCreateAt(dtf.format(now));
		theUom.setId(0);
		uomService.save(theUom);
		Entities Entities = new Entities(0,theUom , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	@PutMapping("/UoMs")
	public Entities updateUoM(@RequestBody UoM theUom) {
		if (theUom.getName().equals("") ) {
			//name == null
			return new Entities(1,null, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		uomService.save(theUom);
		Entities Entities = new Entities(0,theUom , HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
	@DeleteMapping("UoMs/{id}")
	public Entities deleteUoM(@RequestBody int uomId){
		UoM theUoM = uomService.findById(uomId);
		
		if(theUoM == null) {
			throw new RuntimeException("ItemCategory is not found with id = "+uomId);
		}
		
		Entities Entities = new Entities(0,uomService.findAll(), HttpStatus.OK.getReasonPhrase());
		return Entities;
	}
	
}











