package com.creasia.stock.rest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.ApprovedShipment;
import com.creasia.stock.entity.ApprovedShipmentLine;
import com.creasia.stock.entity.Employee;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Project;
import com.creasia.stock.entity.Report;
import com.creasia.stock.entity.RequestShipment;
import com.creasia.stock.entity.RequestShipmentLine;
import com.creasia.stock.entity.Role;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;
import com.creasia.stock.service.ApprovedShipmentService;
import com.creasia.stock.service.EmployeeService;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.RequestShipmentService;
import com.creasia.stock.service.TransactionService;
import com.creasia.stock.service.UserService;
import com.creasia.stock.util.Email;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ApprovedShipmentRestController {

	private ApprovedShipmentService approvedShipmentService;
	private TransactionService transactionService;
	private RequestShipmentService requestShipmentService;
	private UserService userService;
	private EmployeeService employeeService;
	private Email email = new Email();
	private PermissionAssignmentService permissionAssignmentService;

	@Autowired
	public ApprovedShipmentRestController(ApprovedShipmentService theApprovedShipmentService,
			TransactionService theTransactionService, RequestShipmentService theRequestShipmentService,
			UserService theUserService, EmployeeService theEmployeeService,
			PermissionAssignmentService thePermissionAssignmentService) {
		approvedShipmentService = theApprovedShipmentService;
		transactionService = theTransactionService;
		requestShipmentService = theRequestShipmentService;
		userService = theUserService;
		employeeService = theEmployeeService;
		permissionAssignmentService = thePermissionAssignmentService;
	}

	@GetMapping("/ApprovedShipments")
	public Entities findAll() {
		System.out.println("find :::");
		try {
			List<ApprovedShipment> approvedShipmentList = approvedShipmentService.findAll();
			if (approvedShipmentList == null) {
				return new Entities(1, approvedShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
			} else {
				return new Entities(0, approvedShipmentList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, e.toString());
		}

	}

	@GetMapping("/approvedShipments")
	public Entities findApprovedShipment() {

		List<ApprovedShipment> approvedShipmentList = approvedShipmentService.findApprovedShipment();
		if (approvedShipmentList == null) {
			return new Entities(1, approvedShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, approvedShipmentList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/approvedShipments/getByRequestId/{requestId}")
	public Entities findByRequestShipmentId(@RequestParam("requestId") int requestId) {

		List<ApprovedShipment> approvedShipmentList = approvedShipmentService.findByRequestShipmentId(requestId);
		if (approvedShipmentList == null) {
			return new Entities(1, approvedShipmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, approvedShipmentList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/approvedShipments/{approvedShipmentId}")
	public Entities getApprovedShipment(@PathVariable int approvedShipmentId) {

		ApprovedShipment approvedShipment = approvedShipmentService.findById(approvedShipmentId);
		if (approvedShipment == null) {
			return new Entities(1, approvedShipment, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, approvedShipment, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/getReportByProjectIdStockIdExpDateByApprovedShipmentId")
	public Entities getReportByProjectIdStockIdExpDateByApprovedShipmentId(
			@RequestParam("approvedShipmentId") int approvedShipmentId) {

		ApprovedShipment approvedShipment = approvedShipmentService.findById(approvedShipmentId);
		if (approvedShipment == null) {
			return new Entities(1, approvedShipment, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			Stock stock = approvedShipment.getRequestShipment().getStock();
			Project project = approvedShipment.getRequestShipment().getProject();
			// list approve line
			List<ApprovedShipmentLine> approvedShipmentLines = approvedShipment.getApprovedShipmentLines();
			System.out.println("approvedShipmentLines: " + approvedShipmentLines);
			List<Report> reportList = new ArrayList<Report>();
			ApprovedShipment theApprovedShipment = new ApprovedShipment(approvedShipment.getCode(),
					approvedShipment.getRequestShipment(), approvedShipment.getApproveBy(),
					approvedShipment.getCreatedAt(), approvedShipment.getDocumentAt(),
					approvedShipment.getTotalQuantity());
			List<ApprovedShipmentLine> newApprovedShipmentLines = new ArrayList<ApprovedShipmentLine>();
			ApprovedShipmentLine newApprovedShipmentLine = new ApprovedShipmentLine();
			int finalQuantity = 0;
//			// list item & inventory

			// check
			for (ApprovedShipmentLine appLine : approvedShipmentLines) {
				int inventoryQty = (int) transactionService.findInventoryByStockAndProject(stock.getId(),
						project.getId(), appLine.getItem().getId(), appLine.getUoM().getId());
				int quantity = appLine.getQuantity();
				if (inventoryQty < quantity) {
					newApprovedShipmentLine = new ApprovedShipmentLine();
					newApprovedShipmentLine.setItem(appLine.getItem());
					newApprovedShipmentLine.setItemExtNo(appLine.getItemExtNo());
					newApprovedShipmentLine.setExpDate(appLine.getExpDate());
					newApprovedShipmentLine.setUoM(appLine.getUoM());
					newApprovedShipmentLine.setQuantity(appLine.getQuantity());
					newApprovedShipmentLine.setInventoryQty(inventoryQty);
					newApprovedShipmentLines.add(newApprovedShipmentLine);
				} else {
					reportList = transactionService.getReportByProjectIdStockIdExpDate(stock.getId(), project.getId(),
							appLine.getItem().getId(), appLine.getUoM().getId());
					System.out.println("quantity: " + quantity);
					int totalQuantity = 0;
					int i = 0;
					while (i < reportList.size() && totalQuantity < quantity) {
						newApprovedShipmentLine = new ApprovedShipmentLine();
						newApprovedShipmentLine.setItem(reportList.get(i).getItem());
						newApprovedShipmentLine.setItemExtNo(appLine.getItemExtNo());
						newApprovedShipmentLine.setExpDate(reportList.get(i).getExpDate());
						newApprovedShipmentLine.setUoM(reportList.get(i).getUoM());
						if ((reportList.get(i).getQuantity() + totalQuantity) <= quantity) {
							newApprovedShipmentLine.setQuantity((int) reportList.get(i).getQuantity());
							newApprovedShipmentLine.setInventoryQty((int) reportList.get(i).getQuantity());
							totalQuantity += reportList.get(i).getQuantity();
						} else {
							System.out.println("ApprovedShipmentLine.setQuantity: " + (quantity - totalQuantity));
							newApprovedShipmentLine.setQuantity((int) quantity - totalQuantity);
							newApprovedShipmentLine.setInventoryQty((int) quantity - totalQuantity);
							totalQuantity = quantity;
						}
						System.out.println(
								"newApprovedShipmentLine.getQuantity(): " + newApprovedShipmentLine.getQuantity());
						newApprovedShipmentLines.add(newApprovedShipmentLine);
						System.out.println("newApprovedShipmentLines: " + newApprovedShipmentLines);
						i++;
					}
					finalQuantity += totalQuantity;
				}
			}
			if (finalQuantity < approvedShipment.getTotalQuantity()) {
				theApprovedShipment.setTotalQuantity(finalQuantity);
			}
			theApprovedShipment.setApprovedShipmentLines(newApprovedShipmentLines);
			return new Entities(0, theApprovedShipment, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/getNewApprovedShipmentCode")
	public Entities apiGetNewApprovedShipmentCode() {

		// length code SHIPMENT_NOTE = 10 (EX: "SN00000001")
		String lastCode = getNewApprovedShipmentCode();
		ApprovedShipment approvedShipment = new ApprovedShipment();
		approvedShipment.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("approvedShipmentCode", approvedShipment.getCode());
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	public String getNewApprovedShipmentCode() {
		String newCode;
		String lastCode = approvedShipmentService.findLastCode();
		LocalDateTime now = LocalDateTime.now();
		String year = String.valueOf(now.getYear());
		year = year.substring(2);
		if (lastCode == "") {
			newCode = "AS/" + year + "/000001";
		} else {
			String headerCode = lastCode.substring(3, 5);
			if (year.equals(headerCode)) {
				String mainCode = lastCode.substring(6);
				String newMainCode = String.valueOf(Integer.parseInt(mainCode) + 1);
				String st_0 = "";
				st_0 = new String(new char[6 - newMainCode.length()]).replace("\0", "0");
				newCode = "AS/" + year + "/" + st_0 + newMainCode;
			} else {
				newCode = "AS/" + year + "/000001";
			}
			// System.out.println("newCode " + newCode);
		}
		return newCode;
	}

	@PostMapping("/ApprovedShipments")
//	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVE')")
	public Entities addApprovedShipment(@RequestBody ApprovedShipment theApprovedShipment) {
		System.out.println("find :::" + theApprovedShipment);
		if (theApprovedShipment.getApproveBy() == null) {
			return new Entities(1, theApprovedShipment, "Body Invalid");
		}
		if (theApprovedShipment.getRequestShipment().getCreateBy() == null) {
			return new Entities(1, theApprovedShipment, "Body Invalid");
		}
		try {
			// check shipment note co line nao co quantity bang 0
			List<ApprovedShipmentLine> approvedShipmentLines = theApprovedShipment.getApprovedShipmentLines();
			RequestShipment rs = theApprovedShipment.getRequestShipment();
			int i = 0;
			while (i < approvedShipmentLines.size()) {
				if (approvedShipmentLines.get(i).getQuantity() == 0
						&& rs.getRequestShipmentLines().get(i).getOutStdQty() != 0) {
					approvedShipmentLines.remove(i);
				} else {
					i++;
				}
			}
			// all item has quantity = 0 then error
			if (i == 0) {
				return new Entities(1, theApprovedShipment, "Quantity in stock is not enough");
			}
			int totalQuantity = 0;
			for (ApprovedShipmentLine rnLine : theApprovedShipment.getApprovedShipmentLines()) {
				totalQuantity += rnLine.getQuantity();
			}
			// all item has quantity = 0 then error
			if (totalQuantity != 0) {
				theApprovedShipment.setTotalQuantity(totalQuantity);
				theApprovedShipment.setCode(getNewApprovedShipmentCode());
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				theApprovedShipment.setCreatedAt(dtf.format(now));

				int totalQuantityRequest = 0;
				for (RequestShipmentLine rsl : rs.getRequestShipmentLines()) {
					totalQuantityRequest = totalQuantityRequest + rsl.getQuantity();
				}
				rs.setTotalQuantity(totalQuantityRequest);
				for (ApprovedShipmentLine approvedShipmentLine : theApprovedShipment.getApprovedShipmentLines()) {
					for (int j = 0; j < rs.getRequestShipmentLines().size(); j++) {
						if (approvedShipmentLine.getItem().getId() == rs.getRequestShipmentLines().get(j).getItem()
								.getId()) {
							rs.getRequestShipmentLines().get(j)
									.setShiptedQty(rs.getRequestShipmentLines().get(j).getShiptedQty()
											+ approvedShipmentLine.getQuantity());
							rs.getRequestShipmentLines().get(j)
									.setOutStdQty(rs.getRequestShipmentLines().get(j).getOutStdQty()
											- approvedShipmentLine.getQuantity());
						}
					}
				}
				try {
					approvedShipmentService.save(theApprovedShipment);
				} catch (Exception e) {
					// TODO: handle exception
					return new Entities(1, theApprovedShipment, "Fail to server");
				}
			}
			int x = 0;
			while (x < rs.getRequestShipmentLines().size()
					&& (rs.getRequestShipmentLines().get(x).getOutStdQty() == 0)) {
				x++;
			}
			if (x == rs.getRequestShipmentLines().size()) {
				rs.setApproved(1);
			}
			requestShipmentService.update(rs);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theApprovedShipment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theApprovedShipment, "Fail to server");
		}

		Entities entities = new Entities(0, theApprovedShipment, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@GetMapping(value = "/sendemailPost")
	public Entities sendEmailPost(@RequestParam("approvedId") int approvedId)
			throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		// send email
//		String email = theApprovedShipment.get\
		try {
			ApprovedShipment approvedShipment = approvedShipmentService.findById(approvedId);
			
			if (approvedShipment == null) {
				System.out.println("no approve ");
				return new Entities(1, 0, "Error send email");
			}
			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
//			Employee manager = permissionAssignmentService.getUserManager(rs.getCreateBy().getId());
			Employee manager = permissionAssignmentService.getUserManager(approvedShipment.getApproveBy().getId());
			
			List<User> listUser = permissionAssignmentService
					.getUserByStockId(approvedShipment.getRequestShipment().getStock().getId());
			Employee ownerStock = null;
			if (listReceive.indexOf(manager) != -1) {
				listReceive.add(manager);
			}
			for (User u : listUser) {
				if (listReceive.indexOf(u.getEmployee()) != -1) {
					listReceive.add(u.getEmployee());
				}
				List<PermissionAssignment> PER = permissionAssignmentService.getPermissionByUser(u.getId());
				for (PermissionAssignment p :PER) {
					if (p.getStock() != null) {
						ownerStock = p.getUsers().getEmployee();
					}
				}
			}
			if (ownerStock!= null && listReceive.indexOf(ownerStock) != -1) {
				listReceive.remove(ownerStock);
			}
			System.out.println("listUser " + listReceive);

			String RequestCode = approvedShipmentService.findById(approvedId).getRequestShipment().getCode();

			String subject = "Post approved shipment in stock";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;'> The request shipment <a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/approvedShipment\">"
					+ RequestCode + "</a> has been approved. </p>"
					+ "<p style=' font-family: Arial;' > Please go to system to check and process it! </p> <br>" 
					+ "<p style=' font-family: Arial;' >Thanks & regards!</p>"
					+ "<p style=' font-family: Arial;' >Tech Teams</p>";
			String recipient = "";
			String name = "";
			String emailOwner = "";
			// anhnt@creasia.com.vn
			System.out.println("listUser.size():: " + listUser.size());
			System.out.println("listUser. :: " + listUser);
			List<String> emails = new ArrayList<String>();
			
			if (ownerStock == null) {
				name = "All";
				emailOwner = "";
			} else {
				name = ownerStock.getName();
				emailOwner = ownerStock.getEmail();
			}
			
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}
			email.sendmail(subject, "<h4 style=' font-family: Arial;'>" + content1 + name + ", </h4>" + content2, emails, emailOwner);
			entities = new Entities(0, emails, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, 0, "Error send email");
		}
	}

	@GetMapping(value = "/sendemailApprove")
	public Entities sendEmailApprove(@RequestParam("requestId") int requestId) throws AddressException, MessagingException, IOException {
		Entities entities = new Entities();
		try {
			List<String> emails = new ArrayList<String>();
			List<Employee> listReceive = permissionAssignmentService.getUserAdmin();
			for (Employee u : listReceive) {
				emails.add(u.getEmail());
			}
			System.out.println("emails :::" + emails);
			RequestShipment rq = requestShipmentService.findById(requestId);
			Employee PM = rq.getProject().getEmployee();
			Employee managerPM = null; 
			String name1 = "";
			if (PM == null) {
				name1 = "All";
			} else {
				if (emails.indexOf(PM.getEmail()) != -1) {
					emails.remove(PM.getEmail());
				}
				name1 = PM.getName();
				managerPM = PM.getManager();
				if (managerPM != null && emails.indexOf(managerPM.getEmail()) == -1) {
					emails.add(managerPM.getEmail());
				}
			}
			System.out.println("PM :::" + PM);
			String requestCode = requestShipmentService.findById(requestId).getCode();
			// send email
			String subject = "Approve shipment in stock";
			String content1 = "Dear ";
			String content2 = "<p style=' font-family: Arial;'> Request Shipment <a href=\"http://inv.creasia.vn:3000/admin/stock/shipment/requestShipment\"> "
					+ requestCode + "</a> has been verified.</p>"
					+ "<p style=' font-family: Arial;' > Please go to system to check and approve it! </p> <br>" 
					+ "<p style=' font-family: Arial;' >Thanks & regards!</p>"
					+ "<p style=' font-family: Arial;' >Tech Teams</p>";
			System.out.println("requestCode :::" + requestCode);
			email.sendmail(subject, "<h4 style=' font-family: Arial;'>" + content1 + name1 + ", </h4>" + content2, emails, PM.getEmail());
			System.out.println("find :::" + PM.getEmail());
			entities = new Entities(0, emails, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (Exception e) {
			return new Entities(1, 0, "Error send email");
		}

	}
}
