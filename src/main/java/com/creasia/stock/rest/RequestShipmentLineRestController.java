package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.RequestShipmentLine;
import com.creasia.stock.service.RequestShipmentLineService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class RequestShipmentLineRestController {

	private RequestShipmentLineService requestShipmentLineService;

	@Autowired
	public RequestShipmentLineRestController(RequestShipmentLineService theRequestShipmentLineService) {
		requestShipmentLineService = theRequestShipmentLineService;
	}

	@GetMapping("/requestShipmentLines")
	public Entities findAll() {
		List<RequestShipmentLine> requestShipmentLineList = requestShipmentLineService.findAll();
		if (requestShipmentLineList == null) {
			return new Entities(1, requestShipmentLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, requestShipmentLineList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/requestShipmentLines/{requestShipmentLineId}")
	public Entities getRequestShipmentLine(@PathVariable int requestShipmentLineId) {
		RequestShipmentLine requestShipmentLine = requestShipmentLineService.findById(requestShipmentLineId);
		if (requestShipmentLine == null) {
			return new Entities(1, requestShipmentLine, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, requestShipmentLine, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/requestShipmentLines")
	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVE')")
	public Entities addRequestShipmentLine(@RequestBody RequestShipmentLine theRequestShipmentLine) {
		if (theRequestShipmentLine.getItem() == null || theRequestShipmentLine.getQuantity() < 1) {
			return new Entities(1, theRequestShipmentLine, "Body Invalid");
		}
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
//		LocalDateTime now = LocalDateTime.now();
//		theRequestShipmentLine.setCreatedAt(dtf.format(now));
		try {
			requestShipmentLineService.save(theRequestShipmentLine);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theRequestShipmentLine, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theRequestShipmentLine, "Fail to server");
		}
		Entities entities = new Entities(0, theRequestShipmentLine, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@PutMapping("/requestShipmentLines")
	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVE')")
	public Entities updateRequestShipmentLine(@RequestBody RequestShipmentLine theRequestShipmentLine) {
		if (theRequestShipmentLine.getItem() == null || theRequestShipmentLine.getQuantity() < 1) {
			return new Entities(1, theRequestShipmentLine, "Body Invalid");
		}
		try {
			requestShipmentLineService.update(theRequestShipmentLine);
		} catch (OptimisticLockingFailureException e) {
			return new Entities(1, theRequestShipmentLine, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, theRequestShipmentLine, "Fail to server");
		}
		// System.out.println("update success");
		Entities entities = new Entities(0, theRequestShipmentLine, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/requestShipmentLines/{requestShipmentLineId}")
	public Entities deleteRequestShipmentLine(@PathVariable int requestShipmentLineId) {
		RequestShipmentLine requestShipmentLine = requestShipmentLineService.findById(requestShipmentLineId);
		if (requestShipmentLine == null) {
			return new Entities(1, requestShipmentLine, "Error Delete");
		}
		return findAll();
	}

	@GetMapping("/findLineByRequestShipmentId/{idRequestShipment}")
	public Entities findByRequestShipmentId(@PathVariable int idRequestShipment) {
		System.out.println("theRequestShipment : " + idRequestShipment);
		List<RequestShipmentLine> requestShipmentLineList = requestShipmentLineService.findByRequestShipmentId(idRequestShipment);
		if (requestShipmentLineList == null) {
			return new Entities(1, requestShipmentLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, requestShipmentLineList, HttpStatus.OK.getReasonPhrase());
		}
	}
}
