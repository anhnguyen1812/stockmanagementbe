package com.creasia.stock.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.District;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Province;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.service.DistrictService;

@RestController
@RequestMapping("api")
public class DistrictRestController {

	private DistrictService districtService;

	public DistrictRestController(DistrictService theDistrictService) {
		super();
		this.districtService = theDistrictService;
	}
	
	@GetMapping("/districts")
	public Entities findAll(){
		List<District> districtList = districtService.findAll();
		if (districtList == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, districtList, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else { 
			return new Entities(0, districtList, HttpStatus.OK.getReasonPhrase());
		}
		//return districtService.findAll();
	}
	
	
	@GetMapping("/districts/{districtId}")
	public District getStock(@PathVariable int districtId) {
		return districtService.findById(districtId);
	}
	
//	@GetMapping("/districts/{id}")
//	public Entities findById(@PathVariable  int districtId) {
//		District theDistrict = districtService.findById(districtId);
//		if (theDistrict == null) {
//			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
//			return new Entities(1, theDistrict, HttpStatus.NOT_FOUND.getReasonPhrase());
//			
//		}else {
//			return new Entities(0, theDistrict, HttpStatus.OK.getReasonPhrase());
//		}
//		//return districtService.findById(districtId);
//	}
	
	@PostMapping("/districts")
	public Entities addDistrict(@RequestBody District theDistrict) {
		//theDistrict.setId("");
		//ItemCategory itemCategory = theItem.getItemCategory();
			
		districtService.save(theDistrict);
		Entities entities = new Entities(0,theDistrict , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@GetMapping("/districtByProvinceId/{idProvince}")
	public Entities findDistrictByProvinceId(@PathVariable int idProvince) {
		
		//Province
		//District theDistrict = districtService.findById(provinceId);
		System.out.println("theProvince : " + idProvince);
		
		List<District> districtList = districtService.findDistrictByProvinceId(idProvince);
		//List<District> districtList = districtService.findDistrictByProvinceId(provinceId);
		if (districtList == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, districtList, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, districtList, HttpStatus.OK.getReasonPhrase());
		}
	}
}









