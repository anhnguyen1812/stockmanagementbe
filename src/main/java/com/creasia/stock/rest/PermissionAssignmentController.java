package com.creasia.stock.rest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.ERole;
import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.PermissionAssignment;
import com.creasia.stock.entity.Role;
import com.creasia.stock.entity.Stock;
import com.creasia.stock.entity.User;
import com.creasia.stock.service.PermissionAssignmentService;
import com.creasia.stock.service.RoleService;
import com.creasia.stock.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class PermissionAssignmentController {

	private PermissionAssignmentService permissionAssignmentService;
	private UserService userService;
	private RoleService roleService;

	@Autowired
	public PermissionAssignmentController(PermissionAssignmentService thePermissionAssignmentService,
			UserService theUserService, RoleService theRoleService) {
		permissionAssignmentService = thePermissionAssignmentService;
		userService = theUserService;
		roleService = theRoleService;
	}

	@GetMapping("/permissionAssignments")
	public Entities findAll() {
		try {
			List<PermissionAssignment> permissionAssignmentList = permissionAssignmentService.findAll();
			if (permissionAssignmentList == null) {
				return new Entities(1, permissionAssignmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
			} else {
				return new Entities(0, permissionAssignmentList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "server error");
		}
	}

	@PutMapping("/permissionAssignments")
	@PreAuthorize("hasRole('ADMIN')")
	public Entities updateUser(@RequestBody PermissionAssignment thePermissionAssignment) {
		System.out.println("user " + thePermissionAssignment);
		if (thePermissionAssignment == null) {
			return new Entities(1, thePermissionAssignment, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			PermissionAssignment per = permissionAssignmentService.findById(thePermissionAssignment.getId());
			if (per == null) {
				return new Entities(1, thePermissionAssignment, "No user update");
			}
			per.setBlock(thePermissionAssignment.isBlock());
			per.setEndingDate(thePermissionAssignment.getEndingDate());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			final DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDateTime now = LocalDateTime.now();

			if (per.getEndingDate() != null) {
				LocalDate dateTime = LocalDate.parse(thePermissionAssignment.getEndingDate(), dtf2);
				System.out.println("dtf.format(dateTime): : " + dtf.format(dateTime));
				per.setEndingDate(dtf.format(dateTime));
			}
			per.setBlock(thePermissionAssignment.isBlock());
			if (per.isBlock()) {
				per.setBlock(true);
				per.setEndingDate(dtf.format(now).toString());
				System.out.println("block: : " + per.isBlock());
			}
			System.out.println("getEndingDate: : " + per.getEndingDate());
			System.out.println("dtf.format(now): : " + dtf.format(now));
			if ((per.getEndingDate() != null && per.getEndingDate().equals(dtf.format(now)))) {
				per.setBlock(true);
				per.setEndingDate(dtf.format(now));
				System.out.println("getEndingDate: : " + per.getEndingDate());
				// check all per haave end <today-> user -> block
			}
			permissionAssignmentService.update(per);

			List<PermissionAssignment> perByUser = permissionAssignmentService
					.getPermissionByUser(per.getUsers().getId());

			User user = userService.findById(per.getUsers().getId());
			Set<Role> roles = user.getRoles();
			System.out.println("roles1::: " + roles);
			Set<Role> rolesPer = new HashSet<Role>();
			List<Role> rolesList = new ArrayList<>(roles);
			System.out.println("rolesList::: " + rolesList);
			for (PermissionAssignment p : perByUser) {
				if (p.isBlock() == false) {
					rolesPer.add(p.getRoles());
				}
			}
			List<Role> rolePersList = new ArrayList<>(rolesPer);
			System.out.println("rolePersList::: " + rolePersList);
			int index = 0;
			Role temp = null;
			for (Role r : rolesList) {
				index = 0;
				while (index < rolePersList.size() && (r.getId() != rolePersList.get(index).getId())) {
					index ++;
				}
				if (index == rolePersList.size()) {
					temp = r;
				}
				System.out.println("r::: " + r);
			}
			System.out.println("role remove ahaha::: " + temp);
			System.out.println("index ahaha::: " + index);
			roles.remove(temp);
			System.out.println("roles2::: " + roles);
			user.setRoles(roles);
			userService.update(user);

			Entities entities = new Entities(0, per, HttpStatus.OK.getReasonPhrase());
			return entities;
		} catch (OptimisticLockingFailureException e) {

			return new Entities(1, thePermissionAssignment, "Fail to server");
		} catch (StaleObjectStateException e) {
			return new Entities(1, thePermissionAssignment, "Fail to server");
		} catch (Exception e) {
			System.out.println("ERROR: : " + e.toString());
			// TODO: handle exception
			return new Entities(1, thePermissionAssignment, "Fail to server");
		}
	}

	@GetMapping("/getUserByStockId")
	public Entities getUserByStockId(@RequestParam("stockId") int stockId) {
		try {
			List<User> userList = permissionAssignmentService.getUserByStockId(stockId);
			if (userList == null) {
				return new Entities(1, userList, HttpStatus.NOT_FOUND.getReasonPhrase());
			} else {
				return new Entities(0, userList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "Error Server!");
		}

	}

	@PostMapping("/permissionAssignments")
	@PreAuthorize("hasRole('ADMIN')")
	public Entities addPermissionAssignments(@RequestBody PermissionAssignment thePermissionAssignment) {
		System.out.println("thePermissionAssignment0: : " + thePermissionAssignment);
		if (thePermissionAssignment.getUsers() == null) {
			return new Entities(1, null, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			System.out.println("thePermissionAssignment: : " + thePermissionAssignment);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			thePermissionAssignment.setBlock(false);
			thePermissionAssignment.setCreatedAt(dtf.format(now));
			if (thePermissionAssignment.getStock() != null) {
				if (thePermissionAssignment.getStock().getId() == 0) {
					System.out.println("stockId :::: ::: " + thePermissionAssignment.getStock().getId());
					thePermissionAssignment.setStock(null);
				}
			}
			System.out.println("thePermissionAssignment: : " + thePermissionAssignment);
			List<PermissionAssignment> permissionList = permissionAssignmentService
					.getPermissionByUser(thePermissionAssignment.getUsers().getId());
			System.out.println("permissionList: : " + permissionList);

			for (PermissionAssignment p : permissionList) {
				System.out.println("thePermissionAssign::: " + p.getRoles().getId());
				System.out
						.println("thePermissionAssignment.getRoles()::: " + thePermissionAssignment.getRoles().getId());
				if (p.getRoles().getId() == thePermissionAssignment.getRoles().getId()) {
					System.out.println("p.getRoles(): : " + p.getRoles().getId());
					if ((p.getRoles().getId() != 3)
							|| ((p.getRoles().getId() == 3) && (p.getStock() == thePermissionAssignment.getStock()))) {
						System.out.println("ROLE_ID::: " + p.getRoles().getId());
						return new Entities(1, null, "This user already has this permission");
					}
				}
			}
			System.out.println("thePermission end ::: " + thePermissionAssignment);
			permissionAssignmentService.save(thePermissionAssignment);
			User user = userService.findById(thePermissionAssignment.getUsers().getId());
			Set<Role> roles = user.getRoles();
			System.out.println("user user1::: " + user);
			if (!roles.contains(thePermissionAssignment.getRoles())) {
				roles.add(thePermissionAssignment.getRoles());
				user.setRoles(roles);
				System.out.println("user2::: " + user.getRoles());
				System.out.println("user user::: " + user);
				userService.update(user);
				user = userService.findById(thePermissionAssignment.getUsers().getId());
				System.out.println("user user result::: " + user);
			}
			Entities Entities = new Entities(0, permissionList, HttpStatus.OK.getReasonPhrase());
			return Entities;
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "Error Server!");
		}
	}

	@GetMapping("/getPermissionByUserId")
	public Entities getPermissionByUser(@RequestParam("userId") long userId) {
		try {
			List<PermissionAssignment> permissionList = permissionAssignmentService.getPermissionByUser(userId);
			if (permissionList == null) {
				return new Entities(1, permissionList, HttpStatus.NOT_FOUND.getReasonPhrase());
			} else {
				return new Entities(0, permissionList, HttpStatus.OK.getReasonPhrase());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new Entities(1, null, "Error Server!");
		}

	}
}
