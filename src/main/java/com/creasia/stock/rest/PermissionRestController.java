package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Permission;
import com.creasia.stock.service.PermissionService;


@RestController
@RequestMapping("api")
public class PermissionRestController {

	private PermissionService permissionService;
	
	@Autowired 
	public PermissionRestController(PermissionService thePermissionService) {
		permissionService = thePermissionService;
	}
	
	@GetMapping("/permissions")
	public Entities findAll(){
		
		List<Permission> permissionList = permissionService.findAll();
		if (permissionList == null) {
			return new Entities(1, permissionList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, permissionList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/permissions/{permissionId}")
	public Entities getPermission(@PathVariable int permissionId){
		
		Permission permission = permissionService.findById(permissionId);
		if (permission == null) {
			return new Entities(1, permission, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, permission, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/permissions")
	public Entities addPermission(@RequestBody Permission thePermission) {
//		thePermission.setId(0);
//		permissionService.save(thePermission);
//		return thePermission;
		
		if (thePermission.getName().equals("")) {
			//name == null
			return new Entities(1,thePermission, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		thePermission.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now(); 
		thePermission.setCreatedAt(dtf.format(now));
		try {
			permissionService.save(thePermission);
		}catch(OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1,thePermission, "Fail to server");
		}catch(StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1,thePermission, "Fail to server");
		}
		Entities entities = new Entities(0,thePermission , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/permissions")
	public Entities updatePermission(@RequestBody Permission thePermission) {
//		permissionService.save(thePermission);
//		return thePermission;
		
		if (thePermission.getName().equals("")) {
			//name == null
			return new Entities(1,thePermission, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			permissionService.update(thePermission);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,thePermission, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,thePermission, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,thePermission , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@DeleteMapping("/permissions/{permissionId}")
	public Entities deletePermission(@PathVariable int permissionId) {
		
		Permission permission = permissionService.findById(permissionId);
		if (permission == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + permissionId);
		}
		
		return findAll();
	}
}












