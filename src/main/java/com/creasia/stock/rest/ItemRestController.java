package com.creasia.stock.rest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Item;
import com.creasia.stock.entity.ItemCategory;
import com.creasia.stock.entity.UoM;
import com.creasia.stock.service.ItemCategoryService;
import com.creasia.stock.service.ItemService;
import com.creasia.stock.service.UomService;

@RestController
@RequestMapping("api")
public class ItemRestController {

	private ItemService itemService;
	private ItemCategoryService itemCategoryService;
	private UomService uomService;

	@Autowired
	public ItemRestController(ItemService theItemService, ItemCategoryService itemCategoryService,
			UomService uomService) {
		super();
		this.itemService = theItemService;
		this.itemCategoryService = itemCategoryService;
		this.uomService = uomService;
	}

	@GetMapping("/items")
	public Entities findAll() {
		List<Item> itemList = itemService.findAll();

		if (itemList == null) {
			// throw new RuntimeException("ItemCategory is not found with id =
			// "+itemCategoryId);
			return new Entities(1, itemList, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, itemList, HttpStatus.OK.getReasonPhrase());
		}
		// return itemService.findAll();
	}

	@GetMapping("/countItems")
	public Entities countItems() {
		long itemList = itemService.countItem();
		System.out.println("item List:: " + itemList);
		HashMap<String, Long> mapCode = new HashMap();
		mapCode.put("countItem", itemList);
		return new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		// return itemService.findAll();
	}

	@GetMapping("/items/{id}")
	public Entities findById(@RequestBody int itemId) {
		Item theItem = itemService.findById(itemId);

		if (theItem == null) {
			// throw new RuntimeException("ItemCategory is not found with id =
			// "+itemCategoryId);
			return new Entities(1, theItem, HttpStatus.NOT_FOUND.getReasonPhrase());

		} else {
			return new Entities(0, theItem, HttpStatus.OK.getReasonPhrase());
		}
		// return itemService.findById(itemId);
	}

	@PostMapping("/items")
	public Entities addItem(@RequestBody Item theItem) {
		if (theItem.getName().equals("")) {
			// name == null
			return new Entities(1, null, "Name null");
		}

		if (theItem.getItemCategory() == null) {
			// name == null
			return new Entities(1, theItem, "Category null");
		}

		if (theItem.getUoM() == null) {
			// name == null
			return new Entities(1, null, "UoM null");
		}
		theItem.setId(0);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		theItem.setCreateAt(dtf.format(now));
		itemService.save(theItem);
		Entities entities = new Entities(0, theItem, HttpStatus.OK.getReasonPhrase());
		return entities;
		// return findAll();
	}

	@PutMapping("/items")
	public Entities updateItem(@RequestBody Item theItem) {
		if (theItem.getName().equals("")) {
			// name == null
			return new Entities(1, null, "Name null");
		}

		if (theItem.getItemCategory() == null) {
			// name == null
			return new Entities(1, theItem, "Category null");
		}

		if (theItem.getUoM() == null) {
			// name == null
			return new Entities(1, null, "UoM null");
		}

		itemService.save(theItem);
		Entities entities = new Entities(0, theItem, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@DeleteMapping("/items")
	public Entities deleteItem(@RequestBody int itemId) {
		Item item = itemService.findById(itemId);
		itemService.deleteById(itemId);
		if (item == null) {
			Entities entities = new Entities(1, null, HttpStatus.NOT_FOUND.getReasonPhrase());
			return entities;
		}

		itemService.deleteById(itemId);
		Entities entities = new Entities(0, itemService.findAll(), HttpStatus.OK.getReasonPhrase());
		return entities;
	}

	@GetMapping("/getNewItemCode")
	public Entities findNewCode() {
		String lastCode = "";
		String st_0 = "0";
		int lastId = itemService.findNewCode();
		if (lastId == 0) {
			lastCode = "ITE00001";
		} else {
//			lastId += 1;
//			lastId += 10000000;
//			lastCode = String.valueOf(lastId);
			lastId += 1;
			String temp = String.valueOf(lastId);
			st_0 = new String(new char[5 - temp.length()]).replace("\0", "0");
			lastCode = "ITE" + st_0 + temp;
		}
		// System.out.println("lastID = " + lastId);
//		Item item = new Item();
//		item.setCode(lastCode);
		HashMap<String, String> mapCode = new HashMap();
		mapCode.put("itemCode", lastCode);
		Entities Entities = new Entities(0, mapCode, HttpStatus.OK.getReasonPhrase());
		return Entities;
	}

	@PostMapping("/import-item")
	public Entities importExcelFile(@RequestBody MultipartFile files) throws IOException {
		List<Item> item_list = new ArrayList<>();
		try {
		XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
		// Read student data form excel file sheet1.
		XSSFSheet worksheet = workbook.getSheetAt(0);
			if (worksheet.getPhysicalNumberOfRows() < 2) {
				Entities entities = new Entities(1, "", HttpStatus.NO_CONTENT.getReasonPhrase());
				return entities;
			} else {
				List<Integer> error_row = new ArrayList<>();
				List<Integer> exist_row = new ArrayList<>();
				int error = 0;
				for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
					if (index > 0) {
						XSSFRow row = worksheet.getRow(index);
						List<ItemCategory> theItemCategory = itemCategoryService.findByName(getCellValue(row, 0));
						List<UoM> theUoM = uomService.findByName(getCellValue(row, 2));
						if (theItemCategory.size() == 0 || theUoM.size() == 0) {
							error++;
							error_row.add(index + 1);
							break;
						}
						Item item = new Item();
						item.setItemCategory(theItemCategory.get(0));
						item.setName(getCellValue(row, 1));
						item.setUoM(theUoM.get(0));
						if (getCellValue(row, 3) != "") {
							item.setItemExtNo(getCellValue(row, 3));
						}
						item.setId(0);
						String lastCode = "";
						String st_0 = "0";
						int lastId = itemService.findNewCode();
						if (lastId == 0) {
							lastCode = "ITE00001";
						} else {
							lastId += 1;
							String temp = String.valueOf(lastId);
							st_0 = new String(new char[5 - temp.length()]).replace("\0", "0");
							lastCode = "ITE" + st_0 + temp;
						}
						item.setCode(lastCode);
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
						LocalDateTime now = LocalDateTime.now();
						item.setCreateAt(dtf.format(now));
						List<Item> list_item = itemService.findByNameAndUoM(item.getName(), item.getUoM().getId());

						if (list_item.size() > 0) {
							error++;
							exist_row.add(index + 1);
						} else {
							itemService.save(item);
						}
					}
				}
				if (error_row.size() == 0 && exist_row.size() == 0) {
					Entities entities = new Entities(0, 0 , HttpStatus.OK.getReasonPhrase());
					return entities;
				}
				else {
					HashMap<String, Object> mapCode = new HashMap();
					mapCode.put("rowsError", error_row);
					mapCode.put("rowsExist", exist_row);
					Entities entities = new Entities(2, mapCode, "Error some row");
					return entities;
				}
			}
		} catch (Exception e) {
			Entities entities = new Entities(1, 0, "File is incorrect");
			return entities;
		}
	}

	private String getCellValue(Row row, int cellNo) {
		DataFormatter formatter = new DataFormatter();
		Cell cell = row.getCell(cellNo);
		return formatter.formatCellValue(cell);
	}
}
