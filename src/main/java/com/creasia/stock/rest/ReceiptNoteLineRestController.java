package com.creasia.stock.rest;

import java.util.List;


import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.ReceiptNoteLine;
import com.creasia.stock.service.ReceiptNoteLineService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class ReceiptNoteLineRestController {

	private ReceiptNoteLineService receiptNoteLineService;

	@Autowired
	public ReceiptNoteLineRestController(ReceiptNoteLineService theReceiptNoteLineService) {
		receiptNoteLineService = theReceiptNoteLineService;
	}

	@GetMapping("/receiptNoteLines")
	public Entities findAll() {
		List<ReceiptNoteLine> receiptNoteLineList = receiptNoteLineService.findAll();
		if (receiptNoteLineList == null) {
			return new Entities(1, receiptNoteLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, receiptNoteLineList, HttpStatus.OK.getReasonPhrase());
		}
	}

	@GetMapping("/receiptNoteLines/{receiptNoteLineId}")
	public Entities getReceiptNoteLine(@PathVariable int receiptNoteLineId) {
		ReceiptNoteLine receiptNoteLine = receiptNoteLineService.findById(receiptNoteLineId);
		if (receiptNoteLine == null) {
			return new Entities(1, receiptNoteLine, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, receiptNoteLine, HttpStatus.OK.getReasonPhrase());
		}
	}

	@PostMapping("/receiptNoteLines")
	@PreAuthorize("hasRole('ADMIN') or hasRole('POST')")
	public Entities addReceiptNoteLine(@RequestBody ReceiptNoteLine theReceiptNoteLine) {
		if (theReceiptNoteLine.getItem() == null || theReceiptNoteLine.getQuantity() < 1) {
			return new Entities(1, theReceiptNoteLine, "Body Invalid");
		}
		try {
			receiptNoteLineService.save(theReceiptNoteLine);
		} catch (OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1, theReceiptNoteLine, "Fail to server");
		} catch (StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1, theReceiptNoteLine, "Fail to server");
		}
		Entities entities = new Entities(0, theReceiptNoteLine, HttpStatus.OK.getReasonPhrase());
		return entities;
	}

//	@PutMapping("/receiptNoteLines")
//	public Entities updateReceiptNoteLine(@RequestBody ReceiptNoteLine theReceiptNoteLine) {
//		if (theReceiptNoteLine.getItem() == null || theReceiptNoteLine.getQuantity() < 1) {
//			return new Entities(1, theReceiptNoteLine, "Body Invalid");
//		}
//		try {
//			receiptNoteLineService.update(theReceiptNoteLine);
//		} catch (OptimisticLockingFailureException e) {
//			return new Entities(1, theReceiptNoteLine, "Fail to server");
//		} catch (StaleObjectStateException e) {
//			return new Entities(1, theReceiptNoteLine, "Fail to server");
//		}
//		Entities entities = new Entities(0, theReceiptNoteLine, HttpStatus.OK.getReasonPhrase());
//		return entities;
//	}

	@GetMapping("/findLineByReceiptNoteId/{idReceiptNote}")
	public Entities findByReceiptNoteId(@PathVariable int idReceiptNote) {
		System.out.println("theReceiptNote : " + idReceiptNote);
		List<ReceiptNoteLine> receiptNoteLineList = receiptNoteLineService.findByReceiptNoteId(idReceiptNote);
		if (receiptNoteLineList == null) {
			return new Entities(1, receiptNoteLineList, HttpStatus.NOT_FOUND.getReasonPhrase());
		} else {
			return new Entities(0, receiptNoteLineList, HttpStatus.OK.getReasonPhrase());
		}
	}
}
