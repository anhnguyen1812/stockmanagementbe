package com.creasia.stock.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creasia.stock.entity.Entities;
import com.creasia.stock.entity.Department;
import com.creasia.stock.entity.District;
import com.creasia.stock.service.DepartmentService;


@RestController
@RequestMapping("api")
public class DepartmentRestController {

	private DepartmentService departmentService;
	
	@Autowired 
	public DepartmentRestController(DepartmentService theDepartmentService) {
		departmentService = theDepartmentService;
	}
	
	@GetMapping("/departments")
	public Entities findAll(){
		
		List<Department> departmentList = departmentService.findAll();
		if (departmentList == null) {
			return new Entities(1, departmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
		}else {
			return new Entities(0, departmentList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/departments/{departmentId}")
	public Entities getDepartment(@PathVariable int departmentId){
		
		Department department = departmentService.findById(departmentId);
		if (department == null) {
			return new Entities(1, department, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, department, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@GetMapping("/departmentByCompanyId/{companyId}")
	public Entities findDepartmentByCompanyId(@PathVariable int companyId) {
		
		//Province
		//District theDistrict = districtService.findById(provinceId);
		System.out.println("companyId : " + companyId);
		
		List<Department> departmentList = departmentService.findDepartmentByCompanyId(companyId);
		//List<District> districtList = districtService.findDistrictByProvinceId(provinceId);
		if (departmentList == null) {
			//throw new RuntimeException("ItemCategory is not found with id = "+itemCategoryId);
			return new Entities(1, departmentList, HttpStatus.NOT_FOUND.getReasonPhrase());
			
		}else {
			return new Entities(0, departmentList, HttpStatus.OK.getReasonPhrase());
		}
	}
	
	@PostMapping("/departments")
	public Entities addDepartment(@RequestBody Department theDepartment) {
//		theDepartment.setId(0);
//		departmentService.save(theDepartment);
//		return theDepartment;
		
		if (theDepartment.getName().equals("")) {
			//name == null
			return new Entities(1,theDepartment, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		theDepartment.setId(0);
		try {
			departmentService.save(theDepartment);
		}catch(OptimisticLockingFailureException e) {
			System.out.println("OptimisticLockingFailureException : " + e.toString());
			return new Entities(1,theDepartment, "Fail to server");
		}catch(StaleObjectStateException e) {
			System.out.println("StaleObjectStateException : " + e.toString());
			return new Entities(1,theDepartment, "Fail to server");
		}
		Entities entities = new Entities(0,theDepartment , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@PutMapping("/departments")
	public Entities updateDepartment(@RequestBody Department theDepartment) {
//		departmentService.save(theDepartment);
//		return theDepartment;
		
		if (theDepartment.getName().equals("")) {
			//name == null
			return new Entities(1,theDepartment, HttpStatus.NO_CONTENT.getReasonPhrase());
		}
		try {
			departmentService.update(theDepartment);
		}catch(OptimisticLockingFailureException e) {
			return new Entities(1,theDepartment, "Fail to server");
		}catch(StaleObjectStateException e) {
			return new Entities(1,theDepartment, "Fail to server");
		}
		//System.out.println("update success");
		Entities entities = new Entities(0,theDepartment , HttpStatus.OK.getReasonPhrase());
		return entities;
	}
	
	@DeleteMapping("/departments/{departmentId}")
	public Entities deleteDepartment(@PathVariable int departmentId) {
		
		Department department = departmentService.findById(departmentId);
		if (department == null) {
			throw new RuntimeException("ItemCategory is not found with id = " + departmentId);
		}
		
		return findAll();
	}
}












